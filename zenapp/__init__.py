# Import flask to define zenapp 
from flask import Flask, Blueprint, flash
# Import pymongo extension for flask
zenapp = Flask(__name__)
from zenapp import views
from zenapp import sessions
from zenapp.sessions import MongoSessionInterface
zenapp.session_interface = MongoSessionInterface(db='penna_db') 
from flask import render_template,redirect,url_for
# Load the Configuration file
zenapp.config.from_object('config')
# Import a module / component using its blueprint handler variable (mod_auth)
from zenapp.modules.user.views import zen_user
from zenapp.modules.leave.views import zen_leave
from zenapp.modules.sap.views import zen_sap
from zenapp.modules.payroll.views import zen_payroll
from zenapp.modules.pmpr.views import zen_pmpr
from zenapp.modules.sessions.views import zen_sessions
from zenapp.modules.backup.views import zen_backup
# Register blueprint(s)
zenapp.register_blueprint(zen_user)
zenapp.register_blueprint(zen_leave)
zenapp.register_blueprint(zen_sap)
zenapp.register_blueprint(zen_payroll)
zenapp.register_blueprint(zen_pmpr)
zenapp.register_blueprint(zen_sessions)
zenapp.register_blueprint(zen_backup)
from zenapp.modules.user.userfunctions import *
from flask.ext.login import LoginManager
login_manager = LoginManager()
login_manager.init_app(zenapp)
login_manager.session_protection = "strong"
login_manager.login_view = 'zenuser.signin'
login_manager.login_message = u""

@login_manager.user_loader
def load_user(userid):
	return find_by_id(userid)

@zenapp.errorhandler(404)
def page_not_found(error):
	flash("404 Error Page Not Found")
	return redirect(url_for('zenuser.index'))


@zenapp.errorhandler(500)
def page_not_found(error):
    return redirect(url_for('zenuser.index'))