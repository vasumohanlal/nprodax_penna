
/*
 * Editor client script for DB table hn
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		"ajax": "php/table.hn.php",
		"table": "#hn",
		"fields": [
			{
				"label": "first name",
				"name": "first_name",
				"type": "textarea"
			},
			{
				"label": "last name",
				"name": "last_name",
				"type": "textarea"
			},
			{
				"label": "age",
				"name": "age1",
				"type": "textarea"
			},
			{
				"label": "date of birth",
				"name": "date_of_birth",
				"type": "date",
				"dateFormat": "dd\/mm\/y"
			},
			{
				"label": "file",
				"name": "file1",
				"type": "hidden"
			},
			{
				"label": "delete",
				"name": "delete1",
				"type": "hidden"
			}
		]
	} );

	$('#hn').DataTable( {
		"dom": "Tfrtip",
		"ajax": "php/table.hn.php",
		"columns": [
			{
				"data": "first_name"
			},
			{
				"data": "last_name"
			},
			{
				"data": "age1"
			},
			{
				"data": "date_of_birth"
			},
			{
				"data": "file1"
			},
			{
				"data": "delete1"
			}
		],
		"tableTools": {
			"sRowSelect": "os",
			"aButtons": [
				{ "sExtends": "editor_create", "editor": editor },
				{ "sExtends": "editor_edit",   "editor": editor },
				{ "sExtends": "editor_remove", "editor": editor }
			]
		}
	} );
} );

}(jQuery));

