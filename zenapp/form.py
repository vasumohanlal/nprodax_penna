from wtforms import Form, TextField, validators, SelectField, StringField, PasswordField, DateField, FileField

class ApplyLeave(Form):
    leave_type = SelectField('Leave Type', choices=[('','Select Leave Type'),('1','Type1'),('2','Type2')])
    reason = SelectField('Reason', choices=[('','Select Reason'),('1','Reason1'),('2','Reason2')])
    start_date = DateField('Start Date')
    Total_days = StringField('Total Days', [validators.Required()])

class SigninForm(Form):
	username = StringField('user name', [validators.required(), validators.length(max=10)])
	password = PasswordField('password') 
	date = StringField('date')
	changepwd = PasswordField('password')
	confirmchangepwd = PasswordField('password')



















