from settings import *
import pymongo
from pymongo import MongoClient

# connect to database
def dbconnect() :
        
	client = MongoClient()
	db = client[databasename]
	return db

# To save the array into the collection 
def save_collection(collection,arr):

	db = dbconnect()
        coll = db[collection]
        data = coll.insert(arr)
        return data
      
# Updating a document in collection(old doc is replaced by new doc) and inserting if nothing there
def update_collection(collection,arr,ary):
      
	db = dbconnect()
        coll = db[collection]
        data = coll.update(arr,ary,upsert = True )
        return data


# To find the one array(first array from all the resulted arrays) from the collection
def find_one_in_collection(collection,arr):

	db = dbconnect()
        coll = db[collection]
        result = coll.find_one(arr)
        return result

# To Check Username or Email etc 
def checkfield(collection,field,value):
      
	db = dbconnect()
        coll = db[collection]
	checkarray = {field : value}
        checkresult = coll.find_one(checkarray)
        return checkresult

# find all the arrays in collection           
def find_all_in_collection(collection):
      
	db = dbconnect()
        coll = db[collection]
        cursor = coll.find()
        result = [item for item in cursor]
        return result


# find all distinct  arrays in collection           
def find_dist_in_collection(collection,field):
      
	db = dbconnect()
        coll = db[collection]
        cursor = coll.distinct(field)
        result = [item for item in cursor]
        return result

# find all the arrays in collection then return a particular field of all the docs
def find_all_and_filter(collection,arr):
     
	db = dbconnect()
        coll = db[collection]
        cursor = coll.find({},arr)
        result = [item for item in cursor]
        return result

# find all the arrays in collection then return arrays containing that particular arr
def find_in_collection(collection,arr):
      
	db = dbconnect()
        coll = db[collection]
        cursor = coll.find(arr)
        result = [item for item in cursor]
        return result


def find_in_collection_count(collection,arr):
      
        db = dbconnect()
        coll = db[collection]
        cursor = coll.find(arr).count()
        return cursor


# find all the arrays in collection then return arrays containing that particular arr and sort by asc or desc
# sort_arr should give like this sort_arr = [("field",1)] 
def find_in_collection_sort(collection,arr,sort_arr):
      
	db = dbconnect()
        coll = db[collection]
        cursor = coll.find(arr).sort(sort_arr)
        result = [item for item in cursor]
        return result
# To delete the array into the collection 
def del_doc(collection,arr):
        db = dbconnect()
        coll = db[collection]
        data = coll.remove(arr)
        return data

def update_coll(collection,arr,ary):
      
        db = dbconnect()
        coll = db[collection]
        data = coll.update(arr,ary)
        return data   

def find_and_filter(collection,arr,ary):
      
        db = dbconnect()
        coll = db[collection]
        cursor = coll.find(arr,ary)
        result = [item for item in cursor]
        return result

def find_in_collection_sort_limit(collection,arr,sort,limit):      
        db = dbconnect()
        coll = db[collection]
        cursor = coll.find(arr).sort(sort).limit(limit)
        result = [item for item in cursor]
        return result

def find_in_collection_sort(collection,arr,sort):      
        db = dbconnect()
        coll = db[collection]
        cursor = coll.find(arr).sort(sort)
        result = [item for item in cursor]
        return result

def get_sap_user_credentials():
        db = dbconnect()
        coll = db['userLicence']
        result=coll.find_and_modify({"flag":0},{'$set':{"flag":1}})
        return result
def close_sap_user_credentials(licence_id):
        print licence_id
        db = dbconnect()
        coll = db['userLicence']        
        data = coll.update({"_id":licence_id},{'$set':{"flag":0}})
        return data

def drop_col(collection):
        db = dbconnect()
        coll = db[collection]
        data = coll.drop()
        return data