from flask import Flask, Blueprint, render_template, redirect, url_for
from zenapp import * 
from flask.ext.login import login_required

@zenapp.route('/')
@login_required
def home():
	return redirect(url_for('zenuser.index'))

		
