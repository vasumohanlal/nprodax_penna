from flask import Flask, render_template, redirect, request, flash, url_for,Response
from zenapp import * 
from zenapp.modules.sap.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
#from pyrfc import *
from decimal import *
from zenapp.modules.leave.lfunctions import *
import logging
zen_sap = Blueprint('zensap', __name__, url_prefix='/sap')
logging.basicConfig(filename='portalerrors.log',level=logging.INFO)

@zen_sap.route('/loademployees/')
def loademployees():
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_LMS_PERSONAL_DATA')
	total_employees = len(result['IT_0002'])
	for i in range(0,total_employees):
		e = result['IT_0002'][i]
		dob = datetime.datetime.strptime(str(e['GBDAT']), '%Y-%m-%d')
		dob_new = dob.strftime('%d-%m-%Y')
		t =find_one_in_collection('TAddress',{"employee_id":e['PERNR'][3:]}) 
		if t :
			array3={"hno_street":t['hno_street'],"address_line2":t['address_line2'],"city":t['city'],
		"district":t['district'],"state":t['state'],"country":t['country'],"pincode":t['pincode'],
		"subtype":t['subtype']}
		else:
			array3={}
		p =find_one_in_collection('PAddress',{"employee_id":e['PERNR'][3:]})
		if p :
			array4={"hno_street":p['hno_street'],"address_line2":p['address_line2'],"city":p['city'],
		"district":p['district'],"state":p['state'],"country":p['country'],"pincode":p['pincode'],
		"subtype":p['subtype']}
		else:
			array4={}
		if e['EMPDOJ'] is None :
			doj_new='Not Available'
		else:
			doj = datetime.datetime.strptime(str(e['EMPDOJ']), '%Y-%m-%d')
			doj_new = doj.strftime('%d-%m-%Y')
		array2={"username":e['PERNR'][3:],"password":"","dob":dob_new,"f_name":e['VORNA'],"l_name": e['NACHN'],
				"m_status": e['FAMST'],"personal_contact1": e['CONTACT'],"personal_contact2": e['PCONTACT'],"official_contact": e['OCONTACT'],
				"personal_email": e['PEMAIL'],"official_email" :e['OEMAIL'],"department_id":e['ORGEH'],"department_name":e['ORGTX'],
				"gender":e['GESCH'],"designation_id":e['PLANS'],"designation":e['PLSTX'],"country":e['NATIO'],
				"created_time":datetime.datetime.now(),
				"present_address":array3,"permanent_address":array4,"pancard":e['ICNUM'],"calendar_code":e['MOFID'],
				"employee_status":"active","m1_pid":e['MANAGER1'],"m2_pid":e['MANAGER2'],"m3_pid":e['MANAGER3'],
				"m4_pid":e['MANAGER4'],"m5_pid":e['MANAGER5'],"m6_pid":e['MANAGER6'],"m7_pid":e['MANAGER7'],"m1_name":e['MAN_NAME1'],
				"m2_name":e['MAN_NAME2'],"m3_name":e['MAN_NAME3'],"m4_name":e['MAN_NAME4'],"m5_name":e['MAN_NAME5'],
				"m6_name":e['MAN_NAME6'],"m7_name":e['MAN_NAME7'],"plant_id":e['ABKRS'],"doj":doj_new,"ee_group":e['PERSG'],"birthday_flag":0}
		save_collection('Userinfo',array2)
		save_collection('Notificationtimestamp',{"username":e['PERNR'][3:],"created_time":datetime.datetime.now()})			
	return 'successful'

@zen_sap.route('/loademployeeaddress/')
def loademployeeaddress():
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_LMS_ADDRESS_DATA')
	total_employees_p_address = len(result['IT_0006P'])
	for i in range(0,total_employees_p_address):
		e = result['IT_0006P'][i]
		array={"hno_street":e['STRAS'],"address_line2":e['LOCAT'],"city":e['ORT01'],
		"district":e['ORT02'],"state":e['STATE'],"country":e['LAND1'],"pincode":e['PSTLZ'],
		"employee_id":e['PERNR'][3:],"subtype":e['SUBTY']}
		save_collection('PAddress',array)	
	total_employees_t_address = len(result['IT_0006T'])
	for i in range(0,total_employees_t_address):
		e = result['IT_0006T'][i]
		array={"hno_street":e['STRAS'],"address_line2":e['LOCAT'],"city":e['ORT01'],
		"district":e['ORT02'],"state":e['STATE'],"country":e['LAND1'],"pincode":e['PSTLZ'],
		"employee_id":e['PERNR'][3:],"subtype":e['SUBTY']}
		save_collection('TAddress',array)			
	return 'successful'



@zen_sap.route('/addstates/')
@login_required
def addstates():
	# conn = Connection(user='navmohan', passwd='guna1234', ashost='192.168.18.15', sysnr='Q01', client='900')
	# options=[{'TEXT' : "LAND1 = 'IN'"}]
	# fields1=[{'FIELDNAME': "BLAND"}]
	# fields2=[{'FIELDNAME': "BEZEI"}]
	# result1 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T005U',OPTIONS=options,FIELDS=fields1)
	# result2 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T005U',OPTIONS=options,FIELDS=fields2)
	# total_states=len(result1['DATA'])
	# a=[]
	# for i in range(0,total_states):
	# 	a = a + [{"state_key":result1['DATA'][i]['WA'], "state_name":result2['DATA'][i]['WA']}]
	# array={"states":a}
	# save_collection('Gendata',array)
	return 'success'

@zen_sap.route('/addcountries/')
@login_required
def addcountries():
	# conn = Connection(user='navmohan', passwd='guna1234', ashost='192.168.18.15', sysnr='Q01', client='900')
	# options=[{'TEXT' : "SPRAS = 'E'"}]
	# fields1=[{'FIELDNAME': "LAND1"}]
	# fields2=[{'FIELDNAME': "LANDX50"}]
	# result1 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T005T',OPTIONS=options,FIELDS=fields1)
	# result2 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T005T',OPTIONS=options,FIELDS=fields2)
	# total_countries=len(result1['DATA'])
	# a=[]
	# for i in range(0,total_countries):
	# 	a = a + [{"country_key":result1['DATA'][i]['WA'], "country_name":result2['DATA'][i]['WA']}]
	# array={"countries":a}
	# save_collection('Gendata',array)
	return 'success'	

@zen_sap.route('/addmaritalstatus/')
@login_required
def addmaritalstatus():
	# conn = Connection(user='navmohan', passwd='guna1234', ashost='192.168.18.15', sysnr='Q01', client='900')
	# options=[{'TEXT' : "SPRSL = 'E'"}]
	# fields1=[{'FIELDNAME': "FAMST"}]
	# fields2=[{'FIELDNAME': "FTEXT"}]
	# result1 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T502T',OPTIONS=options,FIELDS=fields1)
	# result2 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T502T',OPTIONS=options,FIELDS=fields2)
	# total=len(result1['DATA'])
	# a=[]
	# for i in range(0,total):
	# 	a = a + [{"m_key":result1['DATA'][i]['WA'], "m_status":result2['DATA'][i]['WA']}]
	# array={"maritalstatus":a}
	# save_collection('Gendata',array)
	return 'success'

def assignmanagerrole():
	db=dbconnect()
	coll = db['Userinfo']
	data=coll.aggregate([{"$match":{"m1_pid":{'$exists': True}}},
                        {"$project" :{"m1_pid":1,"m2_pid":1,"m3_pid":1,"m4_pid":1,
		"m5_pid":1,"m6_pid":1,"m7_pid":1,"_id":0,"designation_id":1} }
                        ])
	a=[]
	for i in range(0,len(data['result'])):
		b=[]
		if data['result'][i]['m1_pid'] != '':
			b+=[data['result'][i]['m1_pid']]
			if data['result'][i]['m2_pid'] != '':
				b+=[data['result'][i]['m2_pid']]
				if data['result'][i]['m3_pid'] != '':
					b+=[data['result'][i]['m3_pid']]
					if data['result'][i]['m4_pid'] != '':
						b+=[data['result'][i]['m4_pid']]
						if data['result'][i]['m5_pid'] != '':
							b+=[data['result'][i]['m5_pid']]
							if data['result'][i]['m6_pid'] != '':
								b+=[data['result'][i]['m6_pid']]
								if data['result'][i]['m7_pid'] != '':
									b+=[data['result'][i]['m7_pid']]
		array={"position_id":data['result'][i]['designation_id']}
		update_collection('Positions',array,{'$set' : {"levels" :b}})			
		a+=b
	c=list(set(a))
	for j in range(0,len(c)):
		array={"position_id":c[j]}
		update_collection('Positions',array,{'$addToSet' : {"roles" :"5487f2d857fbb310be5f9ab3"}})

@zen_sap.route('/addpositions/')
def addpositions():
	# sap_login=get_sap_user_credentials()
	# conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], sysnr=sap_login['sysnr'], client=sap_login['client'])
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	options=[{'TEXT' : "SPRSL = 'E'"}]
	fields1=[{'FIELDNAME': "PLANS"}]
	fields2=[{'FIELDNAME': "PLSTX"}]
	result1 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T528T',OPTIONS=options,FIELDS=fields1)
	result2 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T528T',OPTIONS=options,FIELDS=fields2)
	conn.close()
	# close_sap_user_credentials(sap_login['_id'])
	total_positions=len(result1['DATA'])
	for i in range(0,total_positions):
		array = {"position_id":result1['DATA'][i]['WA'], "position_name":result2['DATA'][i]['WA'],"roles":["5487f2a957fbb310be5f9ab1"]}
		save_collection('Positions',array)
	assignmanagerrole()
	return 'success'	

@zen_sap.route('/addmaterials/')
@login_required
def addmaterials():
	# conn = Connection(user='GREENBYTE', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	# result = conn.call('Z_MATNR_DISP')
	# array = {"materials": result['IT_FINAL']}
	# save_collection('Materials',array)
	return 'success'		

@zen_sap.route('/addcalendar/')
def addcalendar():
	c_codes=["HO","BP","TC","GC","TG","KA","AP","TN","TA","GP","MH","KE"]
	current_year=str(datetime.datetime.now().year)
	date_from=current_year+'0101'
	date_to=current_year+'1231'
	for i in range(0,len(c_codes)):
		conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')	
		result = conn.call('Z_LMS_HOLIDAY_GET',HOLIDAY_CALENDAR=c_codes[i],DATE_FROM=date_from,DATE_TO=date_to)
		calendar=[] 
		for r in result['HOLIDAYS']:
			a={}
			a['holiday'] = r['TXT_LONG'][:-6]
			a['date']=datetime.datetime.strptime(r['DATE'],'%d/%m/%Y')
			calendar+=[a]
		save_collection('Calendar',{"calendar_id":c_codes[i],"holidays":calendar,"calendar_year":current_year})
	return 'success'

@zen_sap.route('/loadplanthrs/')
def loadplanthrs():
	p_hrs=[{"plant_id":"HY","plant_current_hr":"50003308",'leave_time':0},
			{"plant_id":"BP","plant_current_hr":"50005631",'leave_time':0},
			{"plant_id":"TC","plant_current_hr":"50002449",'leave_time':0},
			{"plant_id":"GC","plant_current_hr":"50005540",'leave_time':0},
			{"plant_id":"TA","plant_current_hr":"50003447",'leave_time':0},
			{"plant_id":"GP","plant_current_hr":"50004532",'leave_time':0}]
	for i in range(0,len(p_hrs)):
			save_collection('Plants',p_hrs[i])	
	return 'successful'

@zen_sap.route('/loadcustomers/')
def loadcustomers():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16',sysnr='D01', client='150')
	result = conn.call('Z_BAPI_CUSTOMER_MASTER',IM_ERDAT_LOW='01.04.2012',IM_ERDAT_HIGH='02.04.2012')
	for i in range(0,len(result['IT_FINAL'])):
		save_collection('Customers',result['IT_FINAL'][i])
	return 'success'
@zen_sap.route('/loadmaterials/')
def loadmaterials():
	conn = Connection(user='satinos', passwd='sap@123', ashost='192.168.18.15', sysnr='Q01', client='900')
	result = conn.call('Z_BAPI_MATERIAL_MASTER',IM_ERSDA_LOW='01.01.2012',IM_ERSDA_HIGH='25.02.2015')
	for i in range(0,len(result['IT_FINAL'])):
		save_collection('Sdmaterials',result['IT_FINAL'][i])
	return 'success'
@zen_sap.route('/loadplants/')
def loadplants():
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_BAPI_PLANT_MASTER')
	for i in range(0,len(result['IT_FINAL'])):
		array={"WERKS":result['IT_FINAL'][i]['WERKS'],"NAME1":result['IT_FINAL'][i]['NAME1']}
		save_collection('Sdplants',array)
	return 'success'

@zen_sap.route('/loaddependentsdata/')
def loaddependentsdata():
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"_id":0})
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	for i in range(0,len(all_users)):	
		result = conn.call('Z_LMS_EMP_DEPENDENT_DATA',PERNR=all_users[i]['username'])
		b=[]
		if result['IT_0021']:
			for j in range(0,len(result['IT_0021'])):
				if result['IT_0021'][j]['DOB'] is not None:
					new_dob=result['IT_0021'][j]['DOB'].strftime("%d-%m-%Y")
				else:
					new_dob=''
				b+=[{"fname":result['IT_0021'][j]['FIRSTNAME'],"lname":result['IT_0021'][j]['LASTNAME'],"relation":result['IT_0021'][j]['SUBTY'],"dob":new_dob}]
		update_collection('Userinfo',{"username":all_users[i]['username']},{'$set' : {"dependents" :b}})
	return 'successful'

@zen_sap.route('/addrelations/')
@login_required
def addrelations():
	conn = Connection(user='NWGW001', passwd='lms@123', ashost='192.168.18.15', sysnr='Q01', client='899')
	options=[{'TEXT' : "SPRSL = 'E' AND INFTY = '0021'"}]
	fields1=[{'FIELDNAME': "SUBTY"}]
	fields2=[{'FIELDNAME': "STEXT"}]
	result1 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T591S',OPTIONS=options,FIELDS=fields1)
	result2 = conn.call('RFC_READ_TABLE',QUERY_TABLE='T591S',OPTIONS=options,FIELDS=fields2)
	total_relations=len(result1['DATA'])
	a=[]
	for i in range(0,total_relations):
		a = a + [{"relation_id":result1['DATA'][i]['WA'], "relation_type":result2['DATA'][i]['WA']}]
	array={"relations":a}
	save_collection('Gendata',array)
	return 'success'

@zen_sap.route('/workschedule/')
def workschedule():
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"_id":0})
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	current_year=datetime.datetime.now().year
	end_date=str(current_year)+'1231'
	start_date=str(current_year)+'0101'
	for i in range(0,len(all_users)):
		result = conn.call('Z_LMS_WORK_SCHEDULE',BEGDA=start_date,ENDDA=end_date,PERNR=all_users[i]['username'])
		total_count=len(result['IT_WORK_SCHDL'])
		d=[]	
		for j in range(0,total_count):
			new_date=result['IT_WORK_SCHDL'][j]['DATE'].strftime("%d/%m/%Y")
			n_date=datetime.datetime.strptime(new_date,"%d/%m/%Y")
			d+=[{"date":n_date,"worked_hours":str(result['IT_WORK_SCHDL'][j]['WORKED_HOURS'])}]
		save_collection('WorkSchedule',{"employee_id":all_users[i]['username'],"schedule":d,"year":current_year})
	return 'success'

@zen_sap.route('/leavequota/')
def leavequota():
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"_id":0})
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	for i in range(0,len(all_users)):		
		result = conn.call('Z_LMS_LEAVE_QUOTA',P_PERNR=all_users[i]['username'])
		a={}
		if result['IT_P006']:
			if len(result['IT_P006']) == 3:
				save_collection('LeaveQuota',{"employee_id":all_users[i]['username'],"available_cls":int(float(str(result['IT_P006'][1]['ANZHL']))),"leaves_taken":int(float(str(result['IT_P006'][1]['KVERB']))),"available_sls":int(float(str(result['IT_P006'][2]['ANZHL']))),"sls_taken":int(float(str(result['IT_P006'][2]['KVERB']))),"available_els":int(float(str(result['IT_P006'][0]['ANZHL']))),"els_taken":int(float(str(result['IT_P006'][0]['KVERB']))),"lwps_taken":0,"tours_taken":0,"onduty_taken":0})
			elif len(result['IT_P006']) == 2:
				for p in range(0,2):
					if result['IT_P006'][p]['SUBTY']=='10':
						a["available_els"]=float(result['IT_P006'][p]['ANZHL'])
						a["els_taken"]=float(result['IT_P006'][p]['KVERB'])
					elif result['IT_P006'][p]['SUBTY']=='20':
						a["available_cls"]=float(result['IT_P006'][p]['ANZHL'])
						a["leaves_taken"]=float(result['IT_P006'][p]['KVERB'])
					elif result['IT_P006'][p]['SUBTY']=='50':
						a["available_sls"]=float(result['IT_P006'][p]['ANZHL'])
						a["sls_taken"]=float(result['IT_P006'][p]['KVERB'])	
				try:
					print a["available_els"]
				except:
					a["available_els"]=0
					a["els_taken"]=0
				try:
					print a["available_cls"]
				except:
					a["available_cls"]=0
					a["leaves_taken"]=0
				try:
					print a["available_sls"]
				except:
					a["available_sls"]=0
					a["sls_taken"]=0
				a["employee_id"]=all_users[i]['username']
				a["lwps_taken"]=0
				a["tours_taken"]=0
				a["onduty_taken"]=0
				save_collection('LeaveQuota',a)	
			elif len(result['IT_P006']) == 1:
				if result['IT_P006'][0]['SUBTY']=='10':
					a["available_els"]=float(result['IT_P006'][0]['ANZHL'])
					a["els_taken"]=float(result['IT_P006'][0]['KVERB'])
				elif result['IT_P006'][0]['SUBTY']=='20':
					a["available_cls"]=float(result['IT_P006'][0]['ANZHL'])
					a["leaves_taken"]=float(result['IT_P006'][0]['KVERB'])
				elif result['IT_P006'][0]['SUBTY']=='50':
					a["available_sls"]=float(result['IT_P006'][0]['ANZHL'])
					a["sls_taken"]=float(result['IT_P006'][0]['KVERB'])
				a["employee_id"]=all_users[i]['username']
				a["lwps_taken"]=0
				a["tours_taken"]=0
				a["onduty_taken"]=0
				try:
					print a["available_els"]
				except:
					a["available_els"]=0
					a["els_taken"]=0
				try:
					print a["available_cls"]
				except:
					a["available_cls"]=0
					a["leaves_taken"]=0
				try:
					print a["available_sls"]
				except:
					a["available_sls"]=0
					a["sls_taken"]=0
				save_collection('LeaveQuota',a)
	return 'successful'
@zen_sap.route('/chartdata/')
def chartdata():
	chart=[]
	for i in range(1,13):
		chart+=[{"month":i,"CL":0,"SL":0,"LWP":0,"EL":0}]
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"_id":0})
	current_year=datetime.datetime.now().year
	for i in range(0,len(all_users)):
		save_collection('ChartData',{"employee_id":all_users[i]['username'],"year":current_year,"chartdata":chart})
	return 'successful'

@zen_sap.route('/updateholidaystoworkschedule/')
def updateholidaystoworkschedule():
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"calendar_code":1,"_id":0})
	for i in range(0,len(all_users)):
		list_of_holidays=find_one_in_collection('Calendar',{"calendar_id":all_users[i]['calendar_code']})		
		for h in list_of_holidays['holidays']:
			d=int(h['date'].strftime('%j'))-1
			arry4={"schedule."+str(d)+".worked_hours":"0.00"}
			update_coll('WorkSchedule',{"employee_id":all_users[i]['username']},{'$set': arry4})
	return 'successful'

@zen_sap.route('/upadateleavestosap/')
def upadate_leaves_to_sap():
	leaves=find_and_filter('Leaves',{"sap": "1","status":"approved","attribute":"leave"},{"start_date":1,"end_date":1,"halfdays":1,"leave_type":1,"employee_id":1})
	try:
		conn = Connection(user='NWGW001', passwd='Penna@123', ashost='192.168.18.52',gwserv='3301', sysnr='P01', client='900')
	except:
		logging.info('sap connection error')
	for leave in leaves:
		print leave
		sd=datetime.datetime.strptime(leave['start_date'],"%d/%m/%Y")
		newsd=sd.strftime("%Y%m%d")
		ed=datetime.datetime.strptime(leave['end_date'],"%d/%m/%Y")
		newed=ed.strftime("%Y%m%d")
		if len(leave['halfdays']) == 0 :
			try:
				result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=leave['employee_id'],SUBTY=leave['leave_type'],BEGDA=newsd,ENDDA=newed,GV_SIMULATION=' ')
				if result['RETURN']['TYPE'] == 'E':
					logging.info(result['RETURN']['MESSAGE'])
				else:
					update_coll('Leaves',{"_id":leave['_id']},{'$set':{"sap":"0"}})
			except:
				logging.info('error in using Z_LMS_LEAVE_APPLICATION function without halfdays')
		elif len(leave['halfdays']) == 1 :
			if leave['halfdays'][0] == "1":
				try:
					result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=leave['employee_id'],SUBTY=leave['leave_type'],BEGDA=newsd,ENDDA=newed,BFLAG='X',GV_SIMULATION=' ')
					if result['RETURN']['TYPE'] == 'E':
						logging.info(result['RETURN']['MESSAGE'])
					else:
						update_coll('Leaves',{"_id":leave['_id']},{'$set':{"sap":"0"}})
				except:
					logging.info('error in using Z_LMS_LEAVE_APPLICATION function with start date halfday')
			else:
				try:
					result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=leave['employee_id'],SUBTY=leave['leave_type'],BEGDA=newsd,ENDDA=newed,EFLAG='X',GV_SIMULATION=' ')
					if result['RETURN']['TYPE'] == 'E':
						logging.info(result['RETURN']['MESSAGE'])
					else:
						update_coll('Leaves',{"_id":leave['_id']},{'$set':{"sap":"0"}})
				except:
					logging.info('error in using Z_LMS_LEAVE_APPLICATION function with end date halfday')
		else:
			try:
				result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=leave['employee_id'],SUBTY=leave['leave_type'],BEGDA=newsd,ENDDA=newed,BFLAG='X',EFLAG='X',GV_SIMULATION=' ')
				if result['RETURN']['TYPE'] == 'E':
					logging.info(result['RETURN']['MESSAGE'])
				else:
					update_coll('Leaves',{"_id":leave['_id']},{'$set':{"sap":"0"}})
			except:
				logging.info('error in using Z_LMS_LEAVE_APPLICATION function with start date and end date halfdays')
	return 'successful'


# @zen_sap.route('/depupdatetosap/')
def dep_update_to_sap():
	data=find_in_collection('Userinfo',{'$or':[{"d_status":"1"}]})
	if data is not None:
		conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
		for e in data:	
			if e['d_status'] =='1':
				for d in e['dependents']:
					if d['dob'] is not None:
						dob_new = datetime.datetime.strptime(d['dob'],"%d-%m-%Y").strftime("%Y-%m-%d")
						dob=datetime.datetime.strptime(dob_new,"%Y-%m-%d")
					else:
						dob=""
					pennr='000'+e['username']
					value={"PERNR":pennr,"FIRSTNAME":d['fname'],"LASTNAME":d['lname'],"SUBTY":d['relation'],"DOB":dob,"FANAT":'IN',"FGBOT":'KADIRI'}
					result = conn.call('Z_LMS_EMP_DEPENDENT_CHANGE',PERNR=pennr,SUBTYPE=d['relation'],WA_P0021=value)
					print result
					if result['RETURN']['TYPE'] != 'E':
						update_coll('Userinfo',{"username":e['username']},{'$set':{"d_status":"0"}})
		

# @zen_sap.route('/addupdatetosap/')
def add_update_to_sap():
	data=find_in_collection('Userinfo',{'$or':[{"a_status":"1"}]})
	if data is not None:	
		conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
		for e in data:	
			if e['a_status'] =='1':
				if len(e['permanent_address']) != 0:
					value={"SUBTY":e['permanent_address']['subtype'],"STRAS":e['permanent_address']['hno_street'],"ORT01":e['permanent_address']['city'],"ORT02":e['permanent_address']['district'],"LOCAT":e['permanent_address']['address_line2'],"STATE":e['permanent_address']['state'],"PSTLZ":e['permanent_address']['pincode'],"LAND1":e['permanent_address']['country']}
					pennr='000'+e['username']
					result = conn.call('Z_LMS_EMP_ADDRESS_CHANGE',PERNR=pennr, ADR_TYPE='1',WA_P0006=value)
					print result
				if result['RETURN']['TYPE'] != 'E':
						update_coll('Userinfo',{"username":e['username']},{'$set':{"a_status":"0"}})
				if len(e['present_address']) != 0:
					value={"SUBTY":e['present_address']['subtype'],"STRAS":e['present_address']['hno_street'],"ORT01":e['present_address']['city'],"ORT02":e['present_address']['district'],"LOCAT":e['present_address']['address_line2'],"STATE":e['present_address']['state'],"PSTLZ":e['present_address']['pincode'],"LAND1":e['present_address']['country']}
					pennr='000'+e['username']
					result = conn.call('Z_LMS_EMP_ADDRESS_CHANGE',PERNR=pennr, ADR_TYPE='2',WA_P0006=value)
					print result
				if result['RETURN']['TYPE'] != 'E':
					update_coll('Userinfo',{"username":e['username']},{'$set':{"a_status":"0"}})


@zen_sap.route('/profileupdatetosap/')
def profile_update_to_sap():
	data=find_in_collection('Userinfo',{'$or':[{"p_status":"1"}]})
	if data is not None:
		conn = Connection(user='superid', passwd='initpass', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
		for e in data:	
			if e['p_status'] =='1':
				dob_new = datetime.datetime.strptime(e['dob'],"%d-%m-%Y").strftime("%Y-%m-%d")
				doj_new = datetime.datetime.strptime(e['doj'],"%d-%m-%Y").strftime("%Y-%m-%d")
				doj=datetime.datetime.strptime(doj_new,"%Y-%m-%d")
				dob=datetime.datetime.strptime(dob_new,"%Y-%m-%d")
				pennr='000'+e['username']
				value={"PERNR":pennr,"GBDAT":dob,"VORNA":e['f_name'],"NACHN":e['l_name'],"FAMST":e['m_status'],"CONTACT":e['personal_contact1'],"PCONTACT":e['personal_contact2'],"OCONTACT":e['official_contact'],"PEMAIL":e['personal_email'],"OEMAIL":e['official_email'],"ORGEH":e['department_id'],"ORGTX":e['department_name'],"GESCH":e['gender'],"PLANS":e['designation_id'],"PLSTX":e['designation'],"NATIO":e['country'],"ICNUM":e['pancard'],"MOFID":e['calendar_code'],"MANAGER1":e['m1_pid'],"MANAGER2":e['m2_pid'],"MANAGER3":e['m3_pid'],"MANAGER4":e['m4_pid'],"MANAGER5":e['m5_pid'],"MANAGER6":e['m6_pid'],"MANAGER7":e['m7_pid'],"MAN_NAME1":e['m1_name'],"MAN_NAME2":e['m2_name'],"MAN_NAME3":e['m3_name'],"MAN_NAME4":e['m4_name'],"MAN_NAME5":e['m5_name'],"MAN_NAME6":e['m6_name'],"MAN_NAME7":e['m7_name'],"ABKRS":e['plant_id'],"EMPDOJ":doj,"PERSG":e['ee_group']}
				print value
				result = conn.call('Z_LMS_EMPLOYEE_DATA_CHANGE',PERNR=pennr,WA_P0002=value)
				print result
				# if result['RETURN']['TYPE'] != 'E':
				# 	update_coll('Userinfo',{"username":e['username']},{'$set':{"p_status":"0"}})
	# dep_update_to_sap()
	# add_update_to_sap()
	return 'success'


		
	

@zen_sap.route('/sapworkschedule/')
def sap_workschedule():
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"_id":0})
	conn = Connection(user='NWGW001', passwd='Penna@123', ashost='192.168.18.52',gwserv='3301', sysnr='P01', client='900')
	current_year=datetime.datetime.now().year
	end_date=str(current_year)+'1231'
	for i in range(0,len(all_users)):
		result = conn.call('Z_LMS_WORK_SCHEDULE',ENDDA=end_date,PERNR=all_users[i]['username'])
		total_count=len(result['IT_WORK_SCHDL'])
		d=[]	
		for j in range(0,total_count):
			new_date=result['IT_WORK_SCHDL'][j]['DATE'].strftime("%d/%m/%Y")
			n_date=datetime.datetime.strptime(new_date,"%d/%m/%Y")
			d+=[{"date":n_date,"worked_hours":str(result['IT_WORK_SCHDL'][j]['WORKED_HOURS'])}]
		save_collection('WorkSchedule',{"employee_id":all_users[i]['username'],"schedule":d,"year":current_year})
	return 'success'

@zen_sap.route('/sapauto_approvals/')
def sap_auto_approvals():
	leaves=find_in_collection('Leaves',{"status":"applied",'hr_status':"unhold"})
	for leave in leaves:
		count_a_levels = len(leave['approval_levels'])
		for i in range(0,count_a_levels):
			if leave['approval_levels'][i]['a_status'] == 'current':
				j=i
		if leave['approval_levels'][j]['a_time'] != 'None':
			approval_time=leave['approval_levels'][j]['a_time']
			if len(leave['approval_levels'][j]) == 6 :
				approval_time=approval_time+leave['approval_levels'][j]['a_unhold_time']-leave['approval_levels'][j]['a_hold_time']
			current_time=datetime.datetime.now()
			if current_time > approval_time :

				if j == count_a_levels-1:

					arry3={"approval_levels."+str(j)+".a_status":"approved","status":"approved","sap":"1"}
					update_coll('Leaves',{'_id':leave['_id']},{'$set': arry3})
					#updated approved leaves to leave quota collection
					n_leaves=total_no_of_days_leave(leave['employee_id'],leave['start_date'],leave['end_date'],leave['halfdays'])
					update_approved_leaves_in_leave_quota(leave['employee_id'],leave['leave_type'],n_leaves)
					if leave['attribute'] == 'leave':
						get_leave_chart_data(leave['employee_id'],leave['start_date'],leave['end_date'],leave['halfdays'],leave['leave_type'])
					message1='Your '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto Approved'
					message= get_employee_name(leave['employee_id'])+' '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto approved by portal'
					notification(leave['employee_id'],message1)
					sms1='Your '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto approved by portal'
					get_employee_email_mobile(leave['employee_id'],'My Portal Leave Notification',message1,sms1)
					notification(get_hr_id(leave['plant_id']),message)
					sms2=leave['employee_id']+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto approved by portal'
					get_employee_email_mobile(get_hr_id(leave['plant_id']),'My Portal Leave Notification',message,sms2)
				else:
					leave_time=get_leave_time(get_plant_id(leave['employee_id']))
					if leave_time == 0:
						time='None'
					else:
						time=datetime.datetime.now()+datetime.timedelta(hours = leave_time)
					arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
						"approval_levels."+str(j+1)+".a_time":time}
					update_coll('Leaves',{'_id':leave['_id']},{'$set': arry4})
					message2='Your '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto approved by portal and escalated to'+get_employee_name(leave['approval_levels'][j+1]['a_id'])
					message= get_employee_name(leave['employee_id'])+' '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto approved by portal and escalated to'+get_employee_name(leave['approval_levels'][j+1]['a_id'])
					notification(leave['employee_id'],message2)
					sms1='Your '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto escalated to '+leave['approval_levels'][j+1]['a_id']
					get_employee_email_mobile(leave['employee_id'],'My Portal Leave Notification',message2,sms1)
					notification(get_hr_id(leave['plant_id']),message)
					sms2=leave['employee_id']+' '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto escalated to '+leave['approval_levels'][j+1]['a_id']
					get_employee_email_mobile(get_hr_id(leave['plant_id']),'My Portal Leave Notification',message,sms2)
					message1= get_employee_name(leave['employee_id'])+' '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto approved by portal and escalated to you'
					notification(leave['approval_levels'][j+1]['a_id'],message1)
					sms3=leave['employee_id']+' '+leave['leave_type']+' from '+leave['start_date']+' to '+leave['end_date']+' has been auto escalated to you'
					get_employee_email_mobile(leave['approval_levels'][j+1]['a_id'],'My Portal Leave Notification',message1,sms3)

@zen_sap.route('/sapleavequota/')
def sapleavequota():
	drop_col('LeaveQuota')
	all_users=find_and_filter('Userinfo',{"plant_id":{'$exists': True}},{"username":1,"_id":0})
	conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	for i in range(0,len(all_users)):		
		result = conn.call('Z_LMS_LEAVE_QUOTA',P_PERNR=all_users[i]['username'])
		a={}
		if result['IT_P006']:
			if len(result['IT_P006']) == 3:
				save_collection('LeaveQuota',{"employee_id":all_users[i]['username'],"available_cls":int(float(str(result['IT_P006'][1]['ANZHL']))),"leaves_taken":int(float(str(result['IT_P006'][1]['KVERB']))),"available_sls":int(float(str(result['IT_P006'][2]['ANZHL']))),"sls_taken":int(float(str(result['IT_P006'][2]['KVERB']))),"available_els":int(float(str(result['IT_P006'][0]['ANZHL']))),"els_taken":int(float(str(result['IT_P006'][0]['KVERB']))),"lwps_taken":0,"tours_taken":0,"onduty_taken":0})
			elif len(result['IT_P006']) == 2:
				for p in range(0,2):
					if result['IT_P006'][p]['SUBTY']=='10':
						a["available_els"]=float(result['IT_P006'][p]['ANZHL'])
						a["els_taken"]=float(result['IT_P006'][p]['KVERB'])
					elif result['IT_P006'][p]['SUBTY']=='20':
						a["available_cls"]=float(result['IT_P006'][p]['ANZHL'])
						a["leaves_taken"]=float(result['IT_P006'][p]['KVERB'])
					elif result['IT_P006'][p]['SUBTY']=='50':
						a["available_sls"]=float(result['IT_P006'][p]['ANZHL'])
						a["sls_taken"]=float(result['IT_P006'][p]['KVERB'])	
				try:
					print a["available_els"]
				except:
					a["available_els"]=0
					a["els_taken"]=0
				try:
					print a["available_cls"]
				except:
					a["available_cls"]=0
					a["leaves_taken"]=0
				try:
					print a["available_sls"]
				except:
					a["available_sls"]=0
					a["sls_taken"]=0
				a["employee_id"]=all_users[i]['username']
				a["lwps_taken"]=0
				a["tours_taken"]=0
				a["onduty_taken"]=0
				save_collection('LeaveQuota',a)	
			elif len(result['IT_P006']) == 1:
				if result['IT_P006'][0]['SUBTY']=='10':
					a["available_els"]=float(result['IT_P006'][0]['ANZHL'])
					a["els_taken"]=float(result['IT_P006'][0]['KVERB'])
				elif result['IT_P006'][0]['SUBTY']=='20':
					a["available_cls"]=float(result['IT_P006'][0]['ANZHL'])
					a["leaves_taken"]=float(result['IT_P006'][0]['KVERB'])
				elif result['IT_P006'][0]['SUBTY']=='50':
					a["available_sls"]=float(result['IT_P006'][0]['ANZHL'])
					a["sls_taken"]=float(result['IT_P006'][0]['KVERB'])
				a["employee_id"]=all_users[i]['username']
				a["lwps_taken"]=0
				a["tours_taken"]=0
				a["onduty_taken"]=0
				try:
					print a["available_els"]
				except:
					a["available_els"]=0
					a["els_taken"]=0
				try:
					print a["available_cls"]
				except:
					a["available_cls"]=0
					a["leaves_taken"]=0
				try:
					print a["available_sls"]
				except:
					a["available_sls"]=0
					a["sls_taken"]=0
				save_collection('LeaveQuota',a)
	return 'successful'

# @zen_sap.route('/sendmail/')
# def sendmail():
# 	data=find_in_collection('Email',{'$or':[{"flag":"1"}]})
# 	if data is not None:
# 		for e in data:	
# 			if e['flag'] =='1':
# 				sender = 'myportal@pennacement.com'
# 				msg = MIMEText('Dear '+e['fname']+',\n\n'+e['msg']+'\n\n\n\nThanks,\nTeam-HR.')
# 				msg['To'] = e['receiver']
# 				msg['From'] ='myportal@pennacement.com'
# 				msg['Subject'] = e['emailsubject']
# 				smtpObj = smtplib.SMTP('202.65.143.58',25)
# 				smtpObj.set_debuglevel(True) # show communication with the server

# 				try:
# 					smtpObj.sendmail(sender, receivers,msg.as_string())
# 					update_coll('Email',{"emailsubject":e['emailsubject'],"receiver":e['receiver'],"msg":e['msg'],"fname":e['fname']},{'$set':{"flag":"0"}})      
# 			   		print "Successfully sent email"
# 				except Exception,R:
# 			            print R











# def profile_update_to_sap():
# data=find_in_collection('Userinfo',{'$or':[{"p_status":"1"}]})
# 	try:
# 		conn = Connection(user='NWGW001', passwd='lms@123', ashost='192.168.18.15', sysnr='Q01', client='899')
# 	except:
# 		logging.info('sap connection error')
# 	for e in data:
# 		try:
# 			if e['p_status'] =='1':
# 				print "hello"
# 				print e
# 				try:
# 					dob = datetime.datetime.strptime(str(e['dob']), '%d-%m-%Y')
# 					dob_new = dob.strftime('%Y-%m-%d')
# 					doj = datetime.datetime.strptime(str(e['doj']),'%d-%m-%Y')
# 					doj_new = doj.strftime('%Y-%m-%d')
# 					print doj_new 
# 					print dob_new
# 					result = conn.call('Z_LMS_EMPLOYEE_DATA_CHANGE',PERNR=e['username'],GBDAT=dob_new,VORNA=e['f_name'],NACHN=e['l_name'],FAMST=e['m_status'],CONTACT=e['personal_contact1'],PCONTACT=e['personal_contact2'],OCONTACT=e['official_contact'],PEMAIL=e['personal_email'],OEMAIL=e['official_email'],ORGEH=e['department_id'],ORGTX=e['department_name'],GESCH=e['gender'],PLANS=e['designation_id'],PLSTX=e['designation'],NATIO=e['country'],ICNUM=e['pancard'],MOFID=e['calendar_code'],MANAGER1=e['m1_pid'],MANAGER2=e['m2_pid'],MANAGER3=e['m3_pid'],MANAGER4=e['m4_pid'],MANAGER5=e['m5_pid'],MANAGER6=e['m6_pid'],MANAGER7=e['m7_pid'],MAN_NAME1=e['m1_name'],MAN_NAME2=e['m2_name'],MAN_NAME3=e['m3_name'],MAN_NAME4=e['m4_name'],MAN_NAME5=e['m5_name'],MAN_NAME6=e['m6_name'],MAN_NAME7=e['m7_name'],ABKRS=['plant_id'],EMPDOJ=doj_new,PERSG=['ee_group'],GV_SIMULATION=' ')
# 					logging.info(result['RETURN']['MESSAGE'])
# 					# if result['RETURN']['TYPE'] == 'E':
# 					# 	logging.info(result['RETURN']['MESSAGE'])

# 					# else:
# 					# 	print "done"
# 					# 	update_coll('Userinfo',{"username":e['username']},{'$set':{"p_status":"0"}})
# 				except:
# 					logging.info('error in using Z_LMS_EMPLOYEE_DATA_CHANGE to update employee information')
# 		except:
# 			

# 	return 'success'