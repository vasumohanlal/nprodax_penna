from flask import Flask, render_template, redirect, request, flash, url_for,Response
from zenapp import * 
from zenapp.modules.pmpr.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
#from pyrfc import *
from zenapp.modules.leave.lfunctions import *
import logging
import json
import subprocess
import decimal
zen_pmpr = Blueprint('zenpmpr', __name__, url_prefix='/pmpr')


@zen_pmpr.route('/equipmentmaster/', methods = ['GET', 'POST'])
def equipmentmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_EQUIP_F4')
	for i in range(0,len(result['IT_EQUIP'])):
		e = result['IT_EQUIP'][i]
		array={"plant":e['WERKS'],"equipment_category":e['EQTYP'],"equipment_number":e['EQUNR'],"Desp_technical_object":e['SHTXT'],"cost_center":e['KOSTL'],"functional_loc":e['TPLNR'],"Desp_functional_loc":e['TXT_TPLNR'],"planner_group_id":e['INGRP'],"work_center":e['ARBPL'],"main_work_center":e['GEWRK']}
		save_collection('equipmentmaster',array)
	return "successfully"


# @zen_pmpr.route('/materialmaster/', methods = ['GET', 'POST'])
# def materialmaster():
# 	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
# 	result = conn.call('Z_MMS_MATERIAL_MASTER')
# 	print result
# 	for i in range(0,(len(result['IT_MAT'])-6)):
# 		e = result['IT_MAT'][i]
# 		# create= datetime.datetime.strptime(str(e['ERSDA']), '%Y-%m-%d')
# 		# create_new = create.strftime('%d-%m-%Y')
# 		array={"material_num":e['MATNR'],"material_desp":e['MAKTX']}
# 		# ,"old_material_num":e['BISMT'],"material_type":e['MTART'],"material_group":e['MATKL'],"product_hierarchy":e['PRODH'],"description":e['VTEXT'],"plant":e['WERKS'],"storage_loc":e['LGORT'],"valuated_unrestricted-use_stock":float(e['LABST']),"base_unit_measure":e['MEINS'],"periodic_unit_price":float(e['VERPR']),"storage_bin":e['LGPBE'],"created_on":create_new,"material_PO":e['MPOTXT']
# 		save_collection('materialmaster',array)
# 	return "successfully"

@zen_pmpr.route('/materialmaster/')
def materialmaster():
	c=os.path.abspath("zenapp/static/Mat_Master.csv")
	print c		
	with open(c) as csvfile:
		reader = csv.DictReader(csvfile)
		i=0
		for e in reader:
			if i>59171:
				array={u"plant_id":str(e['Pint']),u"material":str(e['Material']),u"material_desc":str(e['MaterialDescription']),u"matl_group":str(e['MatlGroup']),u"material_group_desc":str(e['MaterialGroupDesc']),u"mtype":str(e['MTyp']),u"material_type_desc":str(e['Materialtypedescription']),u"bun":str(e['BUn'])}
				print array
				save_collection('materialmaster',array)
			i=i+1
		return "successfully"
 	
# @zen_pmpr.route('/materialmaster/')
# def materialmaster():
# 	c=os.path.abspath("zenapp/static/Mat_Master.csv")
# 	print c		
# 	with open(c) as csvfile:
# 		reader = csv.DictReader(csvfile)
# 		i=0
# 		for e in reader:
# 			array={u"material":str(e['Material']),u"material_desc":str(e['MaterialDescription']),u"matl_group":str(e['MatlGroup']),u"material_group_desc":str(e['MaterialGroupDesc']),u"mtype":str(e['MTyp']),u"material_type_desc":str(e['Materialtypedescription']),u"bun":str(e['BUn'])}
# 			i=i+1
# 			if i%1000==0:
# 				subprocess.call("sudo service mongod restart",shell=True)
# 			print str(i)
# 			save_collection('materialmasterdemo',array)
# 		return "successfully"
 	

# other functions 

@zen_pmpr.route('/controlkeymaster/', methods = ['GET', 'POST'])
def controlkeymaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_CONTROLKEY_F4')
	for i in range(0,len(result['IT_T430T'])):
		e = result['IT_T430T'][i]
		array={"control_key":e['STEUS'],"control_key_desc":e['TXT']}
		save_collection('controlkeymaster',array)
	return "successfully"

@zen_pmpr.route('/costcentersmaster/', methods = ['GET', 'POST'])
def costcentersmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_COSTC_F4')
	for i in range(0,len(result['IT_CSKS'])):
		e = result['IT_CSKS'][i]
		array={"controlling_area":e['KOKRS'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER']}
		save_collection('costcentersmaster',array)
	return "successfully"

@zen_pmpr.route('/materialgroupmaster/', methods = ['GET', 'POST'])
def materialgroupmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_MAT_GROUP_F4')
	for i in range(0,len(result['IT_T023T'])):
		e = result['IT_T023T'][i]
		array={"material_group":e['MATKL'],"desc_material_group":e['WGBEZ60']}
		save_collection('materialgroupmaster',array)
	return "successfully"

@zen_pmpr.route('/materialtypemaster/', methods = ['GET', 'POST'])
def materialtypemaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_MAT_TYPE_F4')
	for i in range(0,len(result['IT_T134T'])):
		e = result['IT_T134T'][i]
		array={"material_type":e['MTART'],"desc_material_type":e['MTBEZ']}
		save_collection('materialtypemaster',array)
	return "successfully"

@zen_pmpr.route('/measureunitmaster/', methods = ['GET', 'POST'])
def measureunitmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_MATERIAL_UOM_F4')
	for i in range(0,len(result['IT_UOM'])):
		e = result['IT_UOM'][i]
		array={"measurement_unit":e['MSEHI'],"dimension_desc":e['TXDIM']}
		save_collection('measureunitmaster',array)
	return "successfully"


@zen_pmpr.route('/plantmaster/', methods = ['GET', 'POST'])
def plantmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_PLANT_F4')
	for i in range(0,len(result['IT_T001W'])):
		e = result['IT_T001W'][i]
		array={"plant":e['WERKS'],"name":e['NAME1']}
		save_collection('plantmaster',array)
	return "successfully"


@zen_pmpr.route('/prtypemaster/', methods = ['GET', 'POST'])
def prtypemaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_PR_TYPE_F4')
	for i in range(0,len(result['IT_T161T'])):
		e = result['IT_T161T'][i]
		array={"purchasing_doc_type":e['BSART'],"purchasing_doc_category":e['BSTYP'],"desc_purchasing_doc_type":e['BATXT']}
		save_collection('prtypemaster',array)
	return "successfully"


@zen_pmpr.route('/storagelocmaster/', methods = ['GET', 'POST'])
def storagelocmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_STORAGE_LOC_F4')
	for i in range(0,len(result['IT_T001L'])):
		e = result['IT_T001L'][i]
		array={"plant":e['WERKS'],"storage_loc":e['LGORT'],"desc_storage_loc":e['LGOBE']}
		save_collection('storagelocmaster',array)
	return "successfully"



@zen_pmpr.route('/functionlocmaster/', methods = ['GET', 'POST'])
def functionlocmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_FUNLOC_F4')
	for i in range(0,len(result['IT_FL'])):
		e = result['IT_FL'][i]
		array={"functional_loc":e['TPLNR'],"functional_loc_category":e['FLTYP'],"superior_functional_loc":e['TPLMA'],"techinical_object_authorization_group":e['BEGRU'],"maintanance_planning_plant":e['IWERK'],"data_origin_planning_plant_field":e['IWERKI'],"planner_group_customer_service_plant_maintanance":e['INGRP'],"data_origin_maintanance_planner_group_field":e['INGRPI'],"object_type_CIM_resources_work_center":e['PM_OBJTY'],"work_center_id":e['LGWID'],"data_origin_work_center":e['LGWIDI'],"object_number":e['OBJNR']}
		save_collection('functionlocmaster',array)
	return "successfully"

@zen_pmpr.route('/networkmaster/', methods = ['GET', 'POST'])
def networkmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_NETW_F4')
	for i in range(0,len(result['IT_AUFK'])):
		e = result['IT_AUFK'][i]
		array={"order_number":e['AUFNR'],"order_type":e['AUART'],"order_category":e['AUTYP'],"desc":e['KTEXT'],"long_text_exists":e['LTEXT'],"company_code":e['BUKRS'],"plant":e['WERKS'],"cost_collector_key":e['CCKEY'],"responsible_cost_center":e['KOSTV'],"loc_plant":e['SOWRK'],"object_id":e['OBJID'],"identifier_planning_line_items":e['PLGKZ'],"usage_condition_table":e['KVEWE'],"application":e['KAPPL'],"costing_sheet":e['KALSM'],"processing_group":e['ABKRS'],"settlement_cost_element":e['KSTAR'],"cost_center_basic_settlement":e['KOSTL'],"G/L_account_basic_settlement":e['SAKNR'],"sequence_number":e['SEQNR'],"person_responsible":e['USER2'],"department":e['USER6'],"identifier_work_permit_issued":e['USER9'],"object_number":e['OBJNR'],"profit_center":e['PRCTR'],"work_breakdown_structure_element":e['PSPEL'],"variance_key":e['AWSLS'],"functional_area":e['FUNC_AREA'],"object_class":e['SCOPE'],"user_responsible_co_internal_order":e['VERAA_USER'],"main_work_center_maintanance_tasks":e['VAPLZ'],"plant_associated_with_main_work_center":e['WAWRK']}
		save_collection('networkmaster',array)
	return "successfully"

@zen_pmpr.route('/wbselementmaster/', methods = ['GET', 'POST'])
def wbselementmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_WBS_F4')
	for i in range(0,len(result['IT_PRPS'])):
		e = result['IT_PRPS'][i]
		array={"WBS_element":e['PSPNR'],"short_desc":e['POST1'],"object_number":e['OBJNR'],"current_number_appropriate_project":e['PSPHI'],"WBS_element_short_identification":e['POSKI'],"profit_center":e['PRCTR'],"project_type":e['PRART'],"level_project_hierarchy":e['STUFE'],"network_assignment":e['ZUORD'],"usage_condition_table":e['KVEWE'],"costing_sheet":e['KALSM'],"overhead_key":e['ZSCHL'],"results_analysis_key":e['ABGSL'],"object_class":e['SCOPE'],"loc":e['STORT'],"functional_area":e['FUNC_AREA'],}
		save_collection('wbselementmaster',array)
	return "successfully"

@zen_pmpr.route('/assetmaster/', methods = ['GET', 'POST'])
def assetmaster():
	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
	result = conn.call('Z_MMS_ASSET_F4')
	for i in range(0,len(result['IT_ASSET'])):
		e = result['IT_ASSET'][i]
		create= datetime.datetime.strptime(str(e['ERDAT']), '%Y-%m-%d')
		create_new = create.strftime('%d-%m-%Y')
		array={"main_asset_number":e['ANLN1'],"asset_subnumber":e['ANLN2'],"asset_class":e['ANLKL'],"technical_asset_number":e['GEGST'],"asset_types":e['ANLAR'],"object_creator":e['ERNAM'],"created_on":create_new,"asset_desc":e['TXT50'] }
		save_collection('assetmaster',array)
	return "successfully"

# @zen_pmpr.route('/departmentmaster/', methods = ['GET', 'POST'])
# def departmentmaster():
# 	conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
# 	result = conn.call('Z_MMS_DEPARTMENT_F4')
# 	for i in range(0,len(result['IT_DEPT'])):
# 		e = result['IT_DEPT'][i]
# 		array={"department_id":e['OBJID'],"department_name":e['SHORT'],"department_desc":e['STEXT']}
# 		save_collection('departmentmaster',array)
# 	return "successfully"


@zen_pmpr.route('/departmentmaster/')
def departmentmaster():
	c=os.path.abspath("zenapp/static/dept.csv")
	print c		
	with open(c) as csvfile:
		reader = csv.DictReader(csvfile)
		i=0
		for e in reader:
			array={u"plant":str(e['plant']),u"dept":str(e['dept']),u"dept_pid":str(e['dept_pid']),u"dept_chief_pid":str(e['dept_chief_pid'])}
			i=i+1
			print str(i)
			save_collection('departmentmaster',array)
		return "successfully"


# till  master upload in mongodb from sap


#
@zen_pmpr.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    imgname= find_one_in_collection('fs.files',{"_id" :ObjectId(filename)})
    thing = fs.get_last_version(filename=imgname['filename'])
    return Response(thing, mimetype='application/pdf')

#saving pdf in databse
def file_save_gridfs(file_data,ctype,ctag):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    #check is there any previous docs 
    #if it is there remove it
    #save the latest doc
    # userpic = find_one_in_collection('fs.files',{"uid" :uid ,"tag" :ctag})
    # if userpic:
    # 	del_doc('fs.chunks',{"files_id":userpic['_id']})
    # 	del_doc('fs.files',{"_id":userpic['_id']})
    file_ext = file_data.filename.rsplit('.', 1)[1]
    a=fs.put(file_data.read(), content_type=ctype, filename = str(uuid.uuid4())+'.'+file_ext, uid = uid,
        tag = ctag)
    return a 


@zen_pmpr.route('/prcreation/', methods=['GET', 'POST'])
@login_required
def purchasecreation():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	mtype=find_dist_in_collection("materialmaster","mtype")
	if request.method == 'POST':
		form=request.form
		print form
		top=[]
		top=form['topval'].split(',')
		print "F"
		print get_employee_name(current_user.username)
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		if top[1] == "ZRGS":
			description1=form.getlist("description1")
			material1=form.getlist("material1")
			quantity1=form.getlist('quantity1')
			uom1=form.getlist('uom1')
			unit_value1=form.getlist('unit_value1')
			date1=form.getlist('date1')
			just1=form.getlist('just1')
			poheader1=form.getlist('poheader1')
			specify1=form.getlist('specify1')
			procured1=form.getlist('procured1')
			image=request.files.getlist("file1[]")
			print image
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","com_pmorder")
				else:
					a=""
				save_image.append(a)
			# print description1
			# print unit_value1

			# print len(description1)
			# print len(material1)
			# print len(quantity1)
			# print len(uom1) 
			# print len(unit_value1)
			# print len(date1)
			# print len(just1)
			# print len(poheader1)
			# print len(specify1)
			# print len(procured1)
			for i in range(0,len(description1)):
				array={}
				create_new=datetime.datetime.strptime(date1[i],"%d.%m.%Y")
				array={"MATNR":"000000"+material1[i],"MAKTX":"","MENGE":decimal.Decimal(quantity1[i]),"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":create_new,"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i]}
				array1={"MAKTX":description1[i],"MATNR":material1[i],"MENGE":float(quantity1[i]),"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":date1[i],"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i],"file":save_image[i]}			
				ZRGS.append(array)
				ZRGS1.append(array1)
			print ZRGS
		else:
			description2=form.getlist("description2")
			quantity2=form.getlist('quantity2')
			uom2=form.getlist('uom2')
			unit_value2=form.getlist('unit_value2')
			date2=form.getlist('date2')
			grprice=form.getlist('grprice')
			image=request.files.getlist("file2[]")
			print image
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_pmorder")
				else:
					a=""
				save_image.append(a)

			print len(description2)
		
			print len(quantity2)
			print len(uom2) 
			print len(unit_value2)
			print len(date2)
			print len(grprice)

			for j in range(0,len(description2)):
				array={}
				create_new=datetime.datetime.strptime(date2[j],"%d.%m.%Y")
				array={"SER_TXT":description2[j],"MENGE":quantity2[j],"MEINS":uom2[j],"VERPR":unit_value2[j],"EEIND":create_new}
				array1={"SER_TXT":description2[j],"MENGE":float(quantity2[j]),"uom":uom2[j],"unit_value":unit_value2[j],"date":date2[j],"file":save_image[j]}
				ZRSV.append(array)
				ZRSV1.append(array1)
			print ZRSV

		print user_details["plant_id"]
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"_id":0})
		print dept_chief_pid[0]['dept_chief_pid']
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		print dept_store_pid[0]['dept_chief_pid']
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
		print plant_pid[0]['chief_pid']
		print plant_pid[0]['plant_id1']

		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})



		prname=get_employee_name(current_user.username)
		print len(ZRGS)
		print top[0]
		print top[1]
		print top[2]
		print top[3]
		print top[4]
		print top[5]

		if len(ZRGS)!=0:
			conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
			result = conn.call('Z_MMS_PR_CREATION_NEW_NEW',P_ACAT="F",P_TPLNR=top[3],P_EQUNR=top[4],P_KOSTL=top[5],P_AUART="PM04",P_WERKS=plant_pid[0]['plant_id1'],P_OBJID=top[0],IT_PRCRT=ZRGS,P_SIMULATION=" ")
			print result
			if result['IT_PMCRET'][0]["TYPE"]=='E':			
				flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
			else:
				flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-success')

			prarray={"status":"applied","Assignment_category":"F","functionlocation":top[3],"equipment":top[4],"costcenter":top[5],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels}
			save_collection("prcreation",prarray)

		else:
			conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
			result = conn.call('Z_MMS_PR_CREATION_NEW',P_ACAT="F",P_TPLNR=top[3],P_EQUNR=top[4],P_KOSTL=top[5],P_AUART="PM04",P_WERKS=plant_pid[0]['plant_id1'],P_OBJID=top[0],IT_PRSER=ZRSV,P_SIMULATION=" ")
			print result
			if result['IT_PMCRET'][0]["TYPE"]=='E':			
				flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
			else:
				flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-success')
			prarray={"status":"applied","Assignment_category":"F","functionlocation":top[3],"equipment":top[4],"costcenter":top[5],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":"" ,"service": ZRSV1,"approval_levels":levels}
			save_collection("prcreation",prarray)






		# print form['dept']
		# print form['selct']
		# print form['function_location']
		# print form['equipment']
		# print form['cost_center']
		# print form['tabl']
		# print form['description1']

		# print form.getlist("description1")
		# print form.getlist("material1")
		# print form.getlist('quantity1')
		# print form.getlist('uom1')
		# print form.getlist('unit_value1')
		# print form.getlist('date1')
		# print form.getlist('just1')
		# print form.getlist('poheader1')
		# print form.getlist('specify1')
		# print form.getlist('procured1')
		# print request.files.getlist("file1[]")
		return render_template('purchasecreation.html',user_details=user_details,dept=dept,mtype=mtype)
	# uploaded_files = request.files.getlist("file[]")
	return render_template('purchasecreation.html',user_details=user_details,dept=dept,mtype=mtype)

@zen_pmpr.route('/prcostcenter/', methods=['GET', 'POST'])
@login_required
def prcostcenter():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	mtype=find_dist_in_collection("materialmaster","mtype")
	# print "hello"
	# print mtype
	if request.method == 'POST':
		form=request.form
		print form
		top=[]
		top=form['topval'].split(',')
		print "K"
		print get_employee_name(current_user.username)
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		if top[1] == "ZRGS":
			description3=form.getlist("description3")
			material3=form.getlist("material3")
			quantity3=form.getlist('quantity3')
			uom3=form.getlist('uom3')
			unit_value3=form.getlist('unit_value3')
			date3=form.getlist('date3')
			image=request.files.getlist("file3[]")
			print image
			save_image=[]
			for img in image:
				if img.filename !="":
					print "hello34"
					print img
					a=file_save_gridfs(img,"application/pdf","com_costcenter")
				else:
					a=""
				save_image.append(a)
		
			# print description3
			# print unit_value3
			print "hello"
			print len(description3)
			print len(material3)
			print len(quantity3)
			print len(uom3) 
			print len(unit_value3)
			print len(date3)
			for i in range(0,len(description3)):
				array={}
				create_new=datetime.datetime.strptime(date3[i],"%d.%m.%Y")
				array={"MATNR":"000000"+material3[i],"MAKTX":"","MENGE":decimal.Decimal(quantity3[i]),"MEINS":uom3[i],"VERPR":unit_value3[i],"EEIND":create_new}
				array1={"MAKTX":description3[i],"MATNR":material3[i],"MENGE":float(quantity3[i]),"MEINS":uom3[i],"VERPR":unit_value3[i],"EEIND":date3[i],"file":save_image[i]}			
				ZRGS.append(array)
				ZRGS1.append(array1)
			print ZRGS
		else:
			# print "2"
			print "4"
			description4=form.getlist("description4")
			quantity4=form.getlist('quantity4')
			uom4=form.getlist('uom4')
			unit_value4=form.getlist('unit_value4')
			date4=form.getlist('date4')
			grprice=form.getlist('grprice')
			image=request.files.getlist("file4[]")
			print image
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_costcenter")
				else:
					a=""
				save_image.append(a)

			print len(description4)
			print len(quantity4)
			print len(uom4) 
			print len(unit_value4)
			print len(date4)
			print len(grprice)
			# print len(just4)
			# print len(poheader4)
			# print len(specify4)
			# print len(procured4)

			for j in range(0,len(description4)):
				array={}
				create_new=datetime.datetime.strptime(date4[j],"%d.%m.%Y")
				array={"SER_TXT":description4[j],"MENGE":quantity4[j],"MEINS":uom4[j],"VERPR":unit_value4[j],"EEIND":create_new}
				array1={"SER_TXT":description4[j],"MENGE":float(quantity4[j]),"uom":uom4[j],"unit_value":unit_value4[j],"date":date4[j],"file":save_image[j]}
				ZRSV.append(array)
				ZRSV1.append(array1)
			print ZRSV

		print user_details["plant_id"]
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"_id":0})
		print dept_chief_pid[0]['dept_chief_pid']
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		print dept_store_pid[0]['dept_chief_pid']
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
		print plant_pid[0]['chief_pid']
		print plant_pid[0]['plant_id1']

		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})



		prname=get_employee_name(current_user.username)
		print len(ZRGS)
		print top[0]
		print top[1]
		print top[2]
	

		if len(ZRGS)!=0:
			conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
			result = conn.call('Z_MMS_PR_CREATION_NEW',P_ACAT="K",P_KOSTL=top[2],P_AUART=top[1],P_WERKS=plant_pid[0]['plant_id1'],P_PRNAME=prname,P_OBJID=top[0],IT_PRCRT=ZRGS,P_SIMULATION=" ")
			print result
			if result['IT_MMRET'][0]["TYPE"]=='E':			
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-danger')
			else:
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-success')
			prarray={"status":"applied","Assignment_category":"k","functionlocation":"","equipment":"","costcenter":top[2],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels}
			save_collection("prcreation",prarray)

		else:
			conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
			result = conn.call('Z_MMS_PR_CREATION_NEW',P_ACAT="K",P_KOSTL=top[2],P_AUART=top[1],P_WERKS=plant_pid[0]['plant_id1'],P_PRNAME=prname,P_OBJID=top[0],IT_PRSER=ZRSV,P_SIMULATION=" ")
			print result
			if result['IT_MMRET'][0]["TYPE"]=='E':			
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-danger')
			else:
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-success')
			prarray={"status":"applied","Assignment_category":"k","functionlocation":"","equipment":"","costcenter":top[2],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":"" ,"service": ZRSV1,"approval_levels":levels}
			save_collection("prcreation",prarray)






		# print form['dept']
		# print form['selct']
		# print form['function_location']
		# print form['equipment']
		# print form['cost_center']
		# print form['tabl']
		# print form['description1']

		# print form.getlist("description1")
		# print form.getlist("material1")
		# print form.getlist('quantity1')
		# print form.getlist('uom1')
		# print form.getlist('unit_value1')
		# print form.getlist('date1')
		# print form.getlist('just1')
		# print form.getlist('poheader1')
		# print form.getlist('specify1')
		# print form.getlist('procured1')
		# print request.files.getlist("file1[]")
		return render_template('costcenter.html',user_details=user_details,dept=dept,mtype=mtype)
	return render_template('costcenter.html',user_details=user_details,dept=dept,mtype=mtype)

@zen_pmpr.route('/prproject/', methods=['GET', 'POST'])
@login_required
def prproject():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	mtype=find_dist_in_collection("materialmaster","mtype")
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	# print "hello"
	# print mtype
	if request.method == 'POST':
		form=request.form
		print form
		top=[]
		top=form['topval'].split(',')
		print "n/p"
		print get_employee_name(current_user.username)
		print top 
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		if top[1] == "ZRGS":
			description5=form.getlist("description5")
			material5=form.getlist("material5")
			quantity5=form.getlist('quantity5')
			uom5=form.getlist('uom5')
			unit_value5=form.getlist('unit_value5')
			date5=form.getlist('date5')
			# location5=form.getlist('location5')
			image=request.files.getlist("file5[]")
			print image
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","com_p/n")
				else:
					a=""
				save_image.append(a)
		
			# print description5
			# print unit_value5

			# print len(description5)
			# print len(material5)
			# print len(quantity5)
			# print len(uom5) 
			# print len(unit_value5)
			# print len(date5)
			# print len(just5)
			# print len(poheader5)
			# print len(specify5)
			# print len(procured5)
			for i in range(0,len(description5)):
				array={}
				create_new=datetime.datetime.strptime(date5[i],"%d.%m.%Y")
				array={"MAKTX":description5[i],"MATNR":"000000"+material5[i],"MENGE":decimal.Decimal(quantity5[i]),"MEINS":uom5[i],"VERPR":unit_value5[i],"EEIND":create_new}
				array1={"MAKTX":description5[i],"MATNR":material5[i],"MENGE":float(quantity5[i]),"MEINS":uom5[i],"VERPR":unit_value5[i],"EEIND":date5[i],"file":save_image[i]}			
				ZRGS.append(array)
				ZRGS1.append(array1)
			print ZRGS
		else:
			# print "2"
			print "6"
			description6=form.getlist("description6")
			quantity6=form.getlist('quantity6')
			uom6=form.getlist('uom6')
			unit_value6=form.getlist('unit_value6')
			date6=form.getlist('date6')
			just6=form.getlist('just6')
			poheader6=form.getlist('poheader6')
			specify6=form.getlist('specify6')
			procured6=form.getlist('procured6')
			grprice=form.getlist('grprice')
			image=request.files.getlist("file6[]")
			print image
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_n/p")
				else:
					a=""
				save_image.append(a)


			# print len(description6)
		
			# print len(quantity6)
			# print len(uom6) 
			# print len(unit_value6)
			# print len(date6)
			# print len(just6)
			# print len(poheader6)
			# print len(specify6)
			# print len(procured6)

			for j in range(0,len(description6)):
				array={}
				create_new=datetime.datetime.strptime(date6[j],"%d.%m.%Y")
				array={"SER_TXT":description6[j],"MENGE":quantity6[j],"MEINS":uom6[j],"VERPR":unit_value6[j],"EEIND":create_new}
				array1={"SER_TXT":description6[j],"MENGE":float(quantity6[j]),"uom":uom6[j],"unit_value":unit_value6[j],"date":date6[j],"file":save_image[j]}
				ZRSV.append(array)
				ZRSV1.append(array1)
			print ZRSV

		print user_details["plant_id"]
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"_id":0})
		print dept_chief_pid[0]['dept_chief_pid']
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		print dept_store_pid[0]['dept_chief_pid']
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
		print plant_pid[0]['chief_pid']
		print plant_pid[0]['plant_id1']

		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})



		prname=get_employee_name(current_user.username)
		print len(ZRGS)
		print top[0]
		print top[1]
		print top[2]
		if top[2]=="wbs":
			category="P"
		else:
			category="N"

		if len(ZRGS)!=0:
			conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
			result = conn.call('Z_MMS_PR_CREATION_NEW',P_ACAT=category,P_WBS=top[3],P_NTW=top[4],P_AUART=top[1],P_WERKS=plant_pid[0]['plant_id1'],P_OBJID=top[0],IT_PRCRT=ZRGS,P_SIMULATION=" ")
			print result
			if result['IT_MMRET'][0]["TYPE"]=='E':			
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-danger')
			else:
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-success')
			# prarray={"status":"applied","Assignment_category":category,"functionlocation":"","equipment":"","costcenter":"","wbs_element":top[3],"network":top[4],"assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels}
			# save_collection("prcreation",prarray)

		else:
			conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
			result = conn.call('Z_MMS_PR_CREATION_NEW',P_ACAT=category,P_WBS=top[3],P_NTW=top[4],P_AUART=top[1],P_WERKS=plant_pid[0]['plant_id1'],P_OBJID=top[0],IT_PRSER=ZRSV,P_SIMULATION=" ")
			print result
			if result['IT_MMRET'][0]["TYPE"]=='E':			
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-danger')
			else:
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-success')
			# prarray={"status":"applied","Assignment_category":category,"functionlocation":"","equipment":"","costcenter":top[2],"wbs_element":top[3],"network":top[4],"assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":"" ,"service": ZRSV1,"approval_levels":levels}
			# save_collection("prcreation",prarray)






		# print form['dept']
		# print form['selct']
		# print form['function_location']
		# print form['equipment']
		# print form['cost_center']
		# print form['tabl']
		# print form['description1']

		# print form.getlist("description1")
		# print form.getlist("material1")
		# print form.getlist('quantity1')
		# print form.getlist('uom1')
		# print form.getlist('unit_value1')
		# print form.getlist('date1')
		# print form.getlist('just1')
		# print form.getlist('poheader1')
		# print form.getlist('specify1')
		# print form.getlist('procured1')
		# print request.files.getlist("file1[]")
		return render_template('costcenter.html',user_details=user_details,dept=dept,mtype=mtype)
	



	return render_template('project.html',user_details=user_details,dept=dept,mtype=mtype)

@zen_pmpr.route('/prasset/', methods=['GET', 'POST'])
@login_required
def prasset():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	mtype=find_dist_in_collection("materialmaster","mtype")
	# print "hello"
	# print mtype
	if request.method == 'POST':
		form=request.form
		print form
		top=[]
		top=form['topval'].split(',')
		print "n/p"
		print get_employee_name(current_user.username)

		
		ZRGS=[]
		ZRSV=[]
		quantity7=form.getlist('quantity7')
		uom7=form.getlist('uom7')
		unit_value7=form.getlist('unit_value7')
		date7=form.getlist('date7')
		# location7=form.getlist('location7')
		image=request.files.getlist("file7[]")
		print image
		save_image=[]
		for img in image:
			if img.filename !="":
				a=file_save_gridfs(img,"application/pdf","com_prasset")
			else:
				a=""
			save_image.append(a)
		# print description7
		# print unit_value7

		# print len(description7)
		# print len(material7)
		# print len(quantity7)
		# print len(uom7) 
		# print len(unit_value7)
		# print len(date7)
		# print len(just7)
		# print len(poheader7)
		# print len(specify7)
		# print len(procured7)
		for i in range(0,len(description7)):
			array={}
			create_new=datetime.datetime.strptime(date7[i],"%d.%m.%Y")
			array={"SHORT_TEXT":"test","MENGE":decimal.Decimal(quantity7[i]),"MEINS":uom7[i],"VERPR":unit_value7[i],"EEIND":create_new}
			array1={"SHORT_TEXT":"test","MENGE":float(quantity7[i]),"MEINS":uom7[i],"VERPR":unit_value7[i],"EEIND":date7[i],"file":save_image[i]}
			ZRGS1.append(array1)
		print ZRGS
	

		print user_details["plant_id"]
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"_id":0})
		print dept_chief_pid[0]['dept_chief_pid']
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		print dept_store_pid[0]['dept_chief_pid']
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
		print plant_pid[0]['chief_pid']
		print plant_pid[0]['plant_id1']

		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})



		prname=get_employee_name(current_user.username)
		print len(ZRGS)
		print top[0]
		print top[1]
		print top[2]
		
		conn = Connection(user='GBTPRP', passwd='gbt@abap', ashost='192.168.18.16', sysnr='D01', client='120')
		result = conn.call('Z_MMS_PR_CREATION_NEW',P_ACAT="A",P_ASSET=top[1],P_AUART="ZRGS",P_WERKS=plant_pid[0]['plant_id1'],P_OBJID=top[0],IT_PRCRT=ZRGS,P_SIMULATION=" ")
		print result
		if result['IT_MMRET'][0]["TYPE"]=='E':			
				flash(result['IT_MMRET'][0]["MESSAGE"],'alert-danger')
		else:
			flash(result['IT_MMRET'][0]["MESSAGE"],'alert-success')
		# prarray={"status":"applied","Assignment_category":"A","functionlocation":"","equipment":"","costcenter":"","wbs_element":"","network":"","assetcode":top[1],"PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels}
		# save_collection("prcreation",prarray)






		# print form['dept']
		# print form['selct']
		# print form['function_location']
		# print form['equipment']
		# print form['cost_center']
		# print form['tabl']
		# print form['description1']

		# print form.getlist("description1")
		# print form.getlist("material1")
		# print form.getlist('quantity1')
		# print form.getlist('uom1')
		# print form.getlist('unit_value1')
		# print form.getlist('date1')
		# print form.getlist('just1')
		# print form.getlist('poheader1')
		# print form.getlist('specify1')
		# print form.getlist('procured1')
		# print request.files.getlist("file1[]")
		return render_template('asset.html',user_details=user_details,dept=dept,mtype=mtype)
	return render_template('asset.html',user_details=user_details,dept=dept,mtype=mtype)


@zen_pmpr.route('/prapproval/', methods=['GET', 'POST'])
@login_required
def prapproval():
	# p =check_user_role_permissions('550fde51850d2d3861bd7adf2')
	# if p:
	user_details=base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	uid = current_user.username
	db=dbconnect()
	employee_att = db.prcreation.aggregate([{'$match' : {'status': "applied"}},
	{ '$unwind': '$approval_levels' },
            {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])
	
	if request.method == 'POST':
			form=request.form
			c_pr =find_and_filter('prcreation',{"_id":ObjectId(form['prid'])},{"_id":0,"approval_levels":1,"employee_id":1})
			count_a_levels = len(c_pr[0]['approval_levels'])
			for i in range(0,count_a_levels):
				if c_pr[0]['approval_levels'][i]['a_status'] == 'current':
					j=i
					employee_name =	get_employee_name(c_pr[0]['employee_id'])

			if form['submit']== 'Approve':
				if j == count_a_levels-1:
					arry3={"approval_levels."+str(j)+".a_status":"approved","status":"approved","sap":"1"}
					update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry3})
				else:
					arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
						"approval_levels."+str(j+1)+".a_time":"None"}
					update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry4})
				msg="You have approved it."
			elif form['submit'] == 'Reject':
				arry2={"approval_levels."+str(j)+".a_status":"rejected","status":"rejected"}
				update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry2})
				msg="You have rejected it."
			flash(msg,'alert-success')
			return redirect(url_for('zenpmpr.prapproval'))



	return render_template('prapproval.html',user_details=user_details,pr=employee_att['result'])
	# else:
	# 	return redirect(url_for('zenuser.index'))

	

@zen_pmpr.route('/mivcreation/', methods=['GET', 'POST'])
@login_required
def mivcreation():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if request.method == 'POST':
		form=request.form
		print form
		description=form.getlist('description')
		material=form.get('material')
		st_loc=form.getlist('st_loc')
		quantity=form.getlist('quantity')
		myquantity=form.getlist('myquantity')
		specify=form.getlist('specify')

		print description
		print material
		print st_loc
		print quantity
		print myquantity
		print specify
		return redirect(url_for('zenpmpr.mivcreation'))
	return render_template('mivcreation.html',user_details=user_details)



@zen_pmpr.route('/prlevels/', methods = ['GET', 'POST'])
@login_required
def prlevels():
	p =check_user_role_permissions('550fdf44850d2d3861bd7ae4')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid= current_user.username
		plant_id=get_plant_id(uid)
		p_employees = find_and_filter('Userinfo',{"plant_id":plant_id},
			{"_id":0,"username":1,"designation_id":1,"f_name":1,"l_name":1,"m1_pid":1,"m2_pid":1,"m3_pid":1,
				"m4_pid":1,"m5_pid":1,"m6_pid":1,"m7_pid":1,"m1_name":1,
				"m2_name":1,"m3_name":1,"m4_name":1,"m5_name":1,
				"m6_name":1,"m7_name":1})
		a=[]
		for p in p_employees:
			q=find_one_in_collection('Positions',{"position_id":p['designation_id']})
			if 'prlevels' in q:
				p['levels']= q['prlevels']
			a=a+[p]	
		if request.method == 'POST':
			form=request.form
			for i in range(0,len(p_employees)):
				e_id = p_employees[i]['username']
				if e_id in form:
					p_id = form[e_id]
					if p_id in form:
						k={}
						array={}
						for manager in form.getlist(p_id):
							k[manager]=1
							k['_id']=0
						g=find_and_filter('Userinfo',{"username":e_id},k)
						print g[0]
						for p in g[0]:
							array={"position_id":g[0][p]}
							update_collection('Positions',array,{'$addToSet' : {"roles" :"550fe437850d2d3861bd7aeb"}})
						update_collection('Positions',{"position_id":p_id},{'$set' : {"prlevels" : form.getlist(p_id)}})
					else:
						update_collection('Positions',{"position_id":p_id},{'$set' : {"prlevels" : []}})
			flash('Save changes successfully','alert-success')
			return redirect(url_for('zenpmpr.prlevels'))
		return render_template('pr_approval_levels.html', user_details=user_details, p_employees=a)
	else:
		return redirect(url_for('zenuser.index'))



@zen_pmpr.route('/mivlevels/', methods = ['GET', 'POST'])
@login_required
def mivlevels():
	p =check_user_role_permissions('550fdf7a850d2d3861bd7ae5')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid= current_user.username
		plant_id=get_plant_id(uid)
		p_employees = find_and_filter('Userinfo',{"plant_id":plant_id},
			{"_id":0,"username":1,"designation_id":1,"f_name":1,"l_name":1,"m1_pid":1,"m2_pid":1,"m3_pid":1,
				"m4_pid":1,"m5_pid":1,"m6_pid":1,"m7_pid":1,"m1_name":1,
				"m2_name":1,"m3_name":1,"m4_name":1,"m5_name":1,
				"m6_name":1,"m7_name":1})
		a=[]
		for p in p_employees:
			q=find_one_in_collection('Positions',{"position_id":p['designation_id']})
			if 'mivlevels' in q:
				p['levels']= q['mivlevels']
			a=a+[p]	
		if request.method == 'POST':
			form=request.form
			for i in range(0,len(p_employees)):
				e_id = p_employees[i]['username']
				if e_id in form:
					p_id = form[e_id]
					if p_id in form:
						lo={}
						array={}		
						for man in form.getlist(p_id):
							lo[man]=1
							lo['_id']=0
						print lo			
						a=find_and_filter('Userinfo',{"username":e_id},lo)
						print a[0]
						for y in a[0]:
							array={"position_id":a[0][y]}
							print array		
							update_collection('Positions',array,{'$addToSet' : {"roles" :"550fe456850d2d3861bd7aec"}})
						update_collection('Positions',{"position_id":p_id},{'$set' : {"mivlevels" : form.getlist(p_id)}})
					else:
						update_collection('Positions',{"position_id":p_id},{'$set' : {"mivlevels" : []}})
			flash('Save changes successfully','alert-success')
			return redirect(url_for('zenpmpr.mivlevels'))
		return render_template('miv_approval_levels.html', user_details=user_details, p_employees=a)
	else:
		return redirect(url_for('zenuser.index'))

#ajax functions 

@zen_pmpr.route('/functionlocation', methods = ['GET', 'POST'])
def functionlocation():
	q = request.args.get('term')
	db=dbconnect()
	funcloc = db.functionlocmaster.aggregate([
	    {'$match' : { '$or':[{'functional_loc' : {'$regex': str(q),'$options': 'i' }}] }}, 
	    {'$project' : {'functional_loc':1,"_id":0}}])
	return json.dumps({"results":funcloc['result']})


@zen_pmpr.route('/equipment', methods = ['GET', 'POST'])
def equipment():
	q = request.args.get('term')
	db=dbconnect()
	equip = db.equipmentmaster.aggregate([
                {'$match' : { '$or':[{"Desp_technical_object" : {'$regex': str(q),'$options': 'i' }},{'equipment_number' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'Desp_technical_object':1,'equipment_number':1, "_id":0}}])
	return json.dumps({"results":equip['result']})

@zen_pmpr.route('/costcenter', methods = ['GET', 'POST'])
def costcenter():
	q = request.args.get('term')
	db=dbconnect()
	cm= db.costcentersmaster.aggregate([
	    {'$match' : { '$or':[{'cost_center' : {'$regex': str(q),'$options': 'i' }}] }}, 
	    {'$project' : {'cost_center':1,"_id":0}}])
	return json.dumps({"results":cm['result']})




@zen_pmpr.route('/wbs_element', methods = ['GET', 'POST'])
def wbs_element():
	q = request.args.get('term')
	db=dbconnect()
	wbselement = db.wbselementmaster.aggregate([
                {'$match' : { '$or':[{"short_desc" : {'$regex': str(q),'$options': 'i' }},{'WBS_element' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'short_desc':1,'WBS_element':1, "_id":0}}])
	return json.dumps({"results":wbselement['result']})


@zen_pmpr.route('/network', methods = ['GET', 'POST'])
def network():
	q = request.args.get('term')
	db=dbconnect()
	net = db.networkmaster.aggregate([
	    {'$match' : { '$or':[{'object_number' : {'$regex': str(q),'$options': 'i' }}] }}, 
	    {'$project' : {'object_number':1,"_id":0}}])
	return json.dumps({"results":net['result']})

@zen_pmpr.route('/asset', methods = ['GET', 'POST'])
def asset():
	q = request.args.get('term')
	db=dbconnect()
	asset = db.assetmaster.aggregate([
                {'$match' : { '$or':[{"asset_desc" : {'$regex': str(q),'$options': 'i' }},{'main_asset_number' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'asset_desc':1,'main_asset_number':1, "_id":0}}])
	return json.dumps({"results":asset['result']})


@zen_pmpr.route('/department', methods = ['GET', 'POST'])
def department():
	q = request.args.get('term')
	db=dbconnect()
	dpt = db.departmentmaster.aggregate([
                {'$match' : { '$or':[{"department_desc" : {'$regex': str(q),'$options': 'i' }},{'department_id' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'department_desc':1,'department_id':1, "_id":0}}])
	return json.dumps({"results":dpt['result']})


@zen_pmpr.route('/material', methods = ['GET', 'POST'])
def material():
	# print request.args.get('term')
	# print request.args.get('mtype')
	q = request.args.get('term')
	m=request.args.get('mtype')
	db=dbconnect()
	mat = db.materialmaster.aggregate([
                {'$match' : { "mtype":m,'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
                {'$project' : {'material_desc':1,'material':1,'bun':1 ,"_id":0}}])
	return json.dumps({"results":mat['result']})

# @zen_pmpr.route('/functionlocation', methods = ['GET', 'POST'])
# def functionlocation():
# 	q = request.args.get('term')
# 	db=dbconnect()
# 	u_plant = db.Sdplants.aggregate([
#                 {'$match' : { '$or':[{'NAME1' : {'$regex': str(q),'$options': 'i' }},{'WERKS' : {'$regex': str(q),'$options': 'i' }}] }}, 
#                 {'$project' : {'NAME1':1,'WERKS':1, "_id":0}}])
# 	return json.dumps({"results":u_plant['result']})



# @zen_pmpr.route('/functionlocation', methods = ['GET', 'POST'])
# def functionlocation():
# 	q = request.args.get('term')
# 	db=dbconnect()
# 	u_plant = db.Sdplants.aggregate([
#                 {'$match' : { '$or':[{'NAME1' : {'$regex': str(q),'$options': 'i' }},{'WERKS' : {'$regex': str(q),'$options': 'i' }}] }}, 
#                 {'$project' : {'NAME1':1,'WERKS':1, "_id":0}}])
# 	return json.dumps({"results":u_plant['result']})


