from wtforms import Form, TextField, validators, SelectField, StringField, PasswordField, DateField, FileField, SelectMultipleField,widgets
from zenapp.dbfunctions import *

class back_up(Form):
	host = StringField('Host:')
	username = StringField('Username:')
	password = PasswordField('Password:')
	foldername = StringField('Foldername:')