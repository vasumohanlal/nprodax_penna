from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.backup.form import *
from zenapp.dbfunctions import *
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
import os
import csv
import zipfile
import StringIO
from calendar import monthrange
from datetime import datetime as dt
#from pyrfc import *
from zenapp.modules.leave.lfunctions import *
import pysftp
import paramiko
import os,sys
import datetime

zen_backup = Blueprint('zenbackup', __name__, url_prefix='/backup')

@zen_backup.route('/backup/', methods=['GET', 'POST'])
@login_required
def backup():
	p =check_user_role_permissions('55b0c0ccd1c282e4074d2082')
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	form = back_up(request.values)
	host_name = str(form.host.data)
	u_name = str(form.username.data)
	password = "satinospc03"
	folder_name = str(form.foldername.data)
	if host_name != None:
		backup = backup_form("192.168.0.114","satinos-pc-03","satinospc03","dumb_new")
	if p:
		if request.method == 'POST':
			return redirect(url_for('zenbackup.backup'))
		return render_template('backup.html', form=form, user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))

def backup_form(host,uname,passw,data_backup):
	#os.mkdir(data_backup)
	# print "directory '"+ data_backup +"' created sucessfully...."
	client = paramiko.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(host, username=uname, password=passw)
	sftp = pysftp.Connection(host, username=uname, password=passw)
	stdin, stdout, stderr = client.exec_command('mongodump')
	print stdout.readlines() #  for mongodump it is not needed .for mongo restore it is needed
	print "dumped sucessfully......."
	fol_name = str(datetime.datetime.now().ctime())
	sftp.rename('dump',fol_name)
	sftp.get_r(fol_name,data_backup)
	print "sucess....."


# host="192.168.0.113"
# uname="satinos-pc-01"
# passw="satinospc01"
# data_backup="test_penna"
# backup(host,uname,passw,data_backup)


# def restore(res_fname):
# 	client = paramiko.SSHClient()
# 	client.load_system_host_keys()
# 	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# 	client.connect(hostr, username=unamer, password=passwr)
# 	sftp = pysftp.Connection(hostr, username=unamer, password=passwr)
# 	sftp.rename(res_fname,'dump') #res_fname taken from frontend
# 	stdin, stdout, stderr = client.exec_command('mongorestore dump')
# 	print sdtdout.readlines()
# 	print "sucessfully restored........"

# hostr="192.168.0.120"
# unamer="satinos-pc-01"
# passwr="satinospc01"
# res_fname="test_final"
# # backup(hostr,unamer,passwr,res_fname)