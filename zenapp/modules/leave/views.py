from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
import os
import csv
import zipfile
import StringIO
from calendar import monthrange
#from pyrfc import *
from zenapp.modules.leave.lfunctions import *
zen_leave = Blueprint('zenleave', __name__, url_prefix='/leave')

#get pdf from database and display it in browser
@zen_leave.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
    return Response(thing, mimetype='application/pdf')

#saving pdf in databse
def file_save_gridfs(file_data,ctype,ctag):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    #check is there any previous docs 
    #if it is there remove it
    #save the latest doc
    userpic = find_one_in_collection('fs.files',{"uid" :uid ,"tag" :ctag})
    if userpic:
    	del_doc('fs.chunks',{"files_id":userpic['_id']})
    	del_doc('fs.files',{"_id":userpic['_id']})
    file_ext = file_data.filename.rsplit('.', 1)[1]
    fs.put(file_data.read(), content_type=ctype, filename = str(uuid.uuid4())+'.'+file_ext, uid = uid,
        tag = ctag)
# apply leave or attendance
@zen_leave.route('/', methods = ['GET', 'POST'])
@zen_leave.route('/apply/', methods = ['GET', 'POST'])
@login_required
def apply_leave():
	p =check_user_role_permissions('546f2038850d2d1b00023b21')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p: 
		user_details = base()  
		uid= current_user.get_id() # employee mongodb id
		uname=current_user.username # employee id
		#get required details from database
		user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid)},{"calendar_code":1,"designation_id":1,"_id":0,"plant_id":1})
		#get data to show holidays and weeekly halfs in calendar
		calendar=get_calendar_data(uname)
		#leave quota of an employee
		leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":uname})
		leave_quota['sls_balance']=leave_quota['available_sls']-leave_quota['sls_taken']
		leave_quota['els_balance']=leave_quota['available_els']-leave_quota['els_taken']
		leave_quota['cls_balance']=leave_quota['available_cls']-leave_quota['leaves_taken']

		if request.method == 'POST':
			form=request.form
			leave_att = form['leave']
			#check is there any halfdays
			if 'halfday' in form:
				list1= form.getlist('halfcheck')
			else:
				list1 = []
			#if it is a leave check these conditions
			if(leave_att == '1'):
				previous_leaves=business_rule_7(uname,form['start_date'],form['end_date'])
				if previous_leaves > 0 :
					flash('Previous leave dates are colliding with applied leave','alert-danger')
					return redirect(url_for('zenleave.apply_leave'))
				# employee cannot apply a leave if same type of previous leave is under process
				br1=business_rule_1(uname,form['leave_type'])
				if br1 != '0':
					flash(br1,'alert-danger')
					return redirect(url_for('zenleave.apply_leave'))
				# Check CL business rule
				if form['leave_type'] == 'CL':
					employee_group=find_and_filter('Userinfo',{"_id":ObjectId(uid)},{"ee_group":1,'_id':0})
					#no of CLs available to apply now 
					leaves_available=business_rule_3(uname,employee_group[0]['ee_group'])
					#total no of days in a applied leave excluding holidays and weekly halfs
					total_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
					if total_days > leaves_available:
						flash('Please check your quota and retry','alert-danger')
						return redirect(url_for('zenleave.apply_leave'))		
					if employee_group[0]['ee_group'] == "T":
						if total_days > 1.5:
							flash('Casual Leave cannot be applied for more than 1.5 Days','alert-danger')
							return redirect(url_for('zenleave.apply_leave'))
					elif employee_group[0]['ee_group'] == "A" or employee_group[0]['ee_group'] == "S":
						if total_days > 3:
							flash('Casual Leave cannot be applied for more than 3 Days','alert-danger')
							return redirect(url_for('zenleave.apply_leave'))
						no_cls_before=business_rule_8(uname,form['start_date'])
						if no_cls_before+total_days > 3 :
							flash('Casual Leave cannot be applied for more than 3 Days','alert-danger')
							return redirect(url_for('zenleave.apply_leave'))
						no_cls_after=business_rule_10(uname,form['end_date'])
						if no_cls_after+total_days > 3 :
							flash('Casual Leave cannot be applied for more than 3 Days','alert-danger')
							return redirect(url_for('zenleave.apply_leave'))				
					#CL should not followed by SL or EL
					allow_applied_leave=business_rule_5(uname,form['start_date'],form['end_date']) 
					if allow_applied_leave > 0 :
						flash('Casual leave should not be followed by Sick leave or Earned leave','alert-danger')
						return redirect(url_for('zenleave.apply_leave'))
				#check EL business rule
				if form['leave_type'] == 'EL':
					total_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
					# employee cannot apply less than 3 ELs continously 
					if total_days < 3:
						flash('Minimum of 3 Earned Leaves should apply','alert-danger')
						return redirect(url_for('zenleave.apply_leave'))
					if 'lta' in form:
						if total_days < 4:
							flash('Minimum of 4 Earned Leaves should be applied for LTA','alert-danger')
							return redirect(url_for('zenleave.apply_leave'))
					# the maximum of 4 spells can apply in a year
					if business_rule_4(uname) > 4:
						flash('You have already applied Earned Leaves 4 times','alert-danger')
						return redirect(url_for('zenleave.apply_leave'))
					#EL should not followed by CL 
					allow_applied_leave=business_rule_6(uname,form['start_date'],form['end_date']) 
					if allow_applied_leave > 0 :
						flash('Earned leave should not be followed by Casual leave','alert-danger')
						return redirect(url_for('zenleave.apply_leave'))
				if form['leave_type'] == 'SL' :		
					#EL or SL should not followed by CL 
					allow_applied_leave=business_rule_6(uname,form['start_date'],form['end_date']) 
					if allow_applied_leave > 0 :
						flash('Sick leave should not be followed by Casual leave','alert-danger')
						return redirect(url_for('zenleave.apply_leave'))
				br2=business_rule_2(uname,form['start_date'],form['end_date'],list1,form['leave_type'])
				if br2 != '0':
					flash(br2,'alert-danger')
					return redirect(url_for('zenleave.apply_leave'))
				leave_type=get_leave_type(form['leave_type']) #get leave type from database
				attribute="leave"
				msg= 'Leave applied successfully'
			else:
				leave_type=get_attendance_type(form['leave_type']) #get attendance type from database
				attribute="attendance"
				msg=leave_type+'applied successfully'
			# get employee approval levels
			b=find_and_filter('Positions',{"position_id":user_data[0]['designation_id']},{"_id":0,"levels":1})
			d=[]
			k={}
			s=[]
			#get approval time of each level
			leave_time=get_leave_time(user_data[0]['plant_id'])
			if leave_time == 0:
				time='None'
			else:
				time=datetime.datetime.now()+datetime.timedelta(hours = leave_time)
				# get rm ids of this employee
			if b[0]['levels']:
				count_levels=len(b[0]['levels'])
				for j in range(0,count_levels):
					k[b[0]['levels'][j]]=1
					k['_id']=0
				g=find_and_filter('Userinfo',{"username":current_user.username},k)
				for p in g[0]:
					s.append(g[0][p])
				count_levels2=len(s)
				for i in range(0,count_levels2):
					c=find_and_filter('Userinfo',{"designation_id":s[i]},{"username":1,"_id":0})
					if i == 0:
						d= d+[{"a_status":"current","a_id":c[0]['username'],"a_discuss":'0',"a_time":time}]
					else:
						d= d+[{"a_status":"waiting","a_id":c[0]['username'],"a_discuss":'0'}]			
				array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
					"start_date":form['start_date'],"end_date":form['end_date'],"applied_time":datetime.datetime.now(),
					"halfdays":list1, "employee_id":current_user.username,"status":"applied","approval_levels":d,
					"hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0"}
				#save the applied leave or attendance
				save_collection('Leaves',array)
				if request.files['doc']:
					input_file = request.files['doc']
					#save doctor certificate
					file_save_gridfs(input_file,"application/pdf","doc") 
				#get rm1 id 
				d=find_and_filter('Userinfo',{"designation_id":s[0]},{"username":1,"_id":0})
				message= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']+'it is pending for your approval.\n You can approve the leave by clicking on this link : http://myportal.pennacement.com/leave/approvals/'

				message1= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				#send notification to employee
				notification(uname,message1)
				sms1='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				#send email/sms to employee
				#get_employee_email_mobile(uname,'My Portal Leave Notification',message1,sms1)
				#send notification to rm1
				notification(d[0]['username'],message)
				sms2=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				#send email/sms to rm1
				#get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms2)
				#send notification to hr
				notification(get_hr_id(user_data[0]['plant_id']),message)
				#send email/sms to hr
				#get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message,sms2)
			else:
				#auto approval of leave/attendance if there are no approval levels
				array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
					"start_date":form['start_date'],"end_date":form['end_date'],"applied_time":datetime.datetime.now(),
					"halfdays":list1, "employee_id":current_user.username,"status":"approved","approval_levels":d,
					"hr_id":user_data[0]['plant_id']}
				# save leave or attendance
				save_collection('Leaves',array)
				message1= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				message2= get_employee_name(uname)+' '+leave_type+' approved '+' from '+form['start_date']+' to '+form['end_date']
				message3= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				message4= 'Your '+leave_type+' approved '+' from '+form['start_date']+' to '+form['end_date']
				#notification to employee
				notification(uname,message3)
				sms3='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				#email/sms to employee
				#get_employee_email_mobile(uname,'My Portal Leave Notification',message3,sms3)
				notification(get_hr_id(user_data[0]['plant_id']),message1)
				sms1=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
				#get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message1,sms1)
				notification(uname,message4)
				sms4='Your '+leave_type+' from '+form['start_date']+' to '+form['end_date']+'has been approved'
				#get_employee_email_mobile(uname,'My Portal Leave Notification',message4,sms4)
				notification(get_hr_id(user_data[0]['plant_id']),message2)
				#get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message2,sms4)
			flash(msg,'alert-success')
			return redirect(url_for('zenleave.apply_leave'))
		return render_template('apply_leave.html', user_details=user_details,calendar=calendar,leave_quota=leave_quota)
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/approvals/', methods = ['GET', 'POST'])
@login_required
def leaveapprovals():
	p =check_user_role_permissions('546f204b850d2d1b00023b22')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid = current_user.username
		db=dbconnect()
		employee_att = db.Leaves.aggregate([{'$match' : {'status': "applied",'hr_status':"unhold"}},
		{ '$unwind': '$approval_levels' },
                {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])
		a=[]
		for e in employee_att['result']:
			e['fullname']=get_employee_name(e['employee_id'])
			e['reason']=e['leave_reason']
			if e['attribute'] == 'attendance':
				e['leave']=get_attendance_type(e['leave_type'])
			else:
				e['leave']=get_leave_type(e['leave_type'])
				if e['leave_type'] == 'SL':
					leave_days=total_no_of_days_leave(e['employee_id'],e['start_date'],e['end_date'],e['halfdays'])
					if leave_days > 3:
						doc_cert = find_one_in_collection('fs.files',{"uid" :e['employee_id'],"tag":"doc"})
						e['doc_url']='/leave/static/pdf/gridfs/'+doc_cert['filename']
			a=a+[e]
		if request.method == 'POST':
			form=request.form
			c_leave =find_and_filter('Leaves',{"_id":ObjectId(form['leaveid'])},{"_id":0,"approval_levels":1,"employee_id":1,"attribute":1,"plant_id":1,"leave_type":1,"start_date":1,"end_date":1,"halfdays":1})
			count_a_levels = len(c_leave[0]['approval_levels'])
			for i in range(0,count_a_levels):
				if c_leave[0]['approval_levels'][i]['a_status'] == 'current':
					j=i
					employee_name =	get_employee_name(c_leave[0]['employee_id'])
					e_attribute = c_leave[0]['attribute']
			if e_attribute == 'attendance':
				leave_type=get_attendance_type(c_leave[0]['leave_type'])
			else:
				leave_type=get_leave_type(c_leave[0]['leave_type'])	
			if form['submit']== 'Approve':
				if j == count_a_levels-1:
					arry3={"approval_levels."+str(j)+".a_status":"approved","status":"approved","sap":"1"}
					update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry3})
					#updated approved leaves to leave quota collection
					if c_leave[0]['leave_type']== 'SL':
						n_leaves=total_no_of_days_el(c_leave[0]['employee_id'],c_leave[0]['start_date'],c_leave[0]['end_date'],c_leave[0]['halfdays'])
					else:
						n_leaves=total_no_of_days_leave(c_leave[0]['employee_id'],c_leave[0]['start_date'],c_leave[0]['end_date'],c_leave[0]['halfdays'])
					update_approved_leaves_in_leave_quota(c_leave[0]['employee_id'],c_leave[0]['leave_type'],n_leaves)
					if e_attribute == 'leave':
						get_leave_chart_data(c_leave[0]['employee_id'],c_leave[0]['start_date'],c_leave[0]['end_date'],c_leave[0]['halfdays'],c_leave[0]['leave_type'])
					msg=employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been Approved'
					message1='Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been Approved'
					message= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved by '+get_employee_name(uid)
					notification(c_leave[0]['employee_id'],message1)
					sms1='Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved by '+get_employee_name(uid)
					#get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message1,sms1)
					notification(get_hr_id(c_leave[0]['plant_id']),message)
					sms2=get_employee_name(c_leave[0]['employee_id'])+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved by '+get_employee_name(uid)
					#get_employee_email_mobile(get_hr_id(c_leave[0]['plant_id']),'My Portal Leave Notification',message,sms2)
				else:
					leave_time=get_leave_time(get_plant_id(c_leave[0]['employee_id']))
					if leave_time == 0:
						time='None'
					else:
						time=datetime.datetime.now()+datetime.timedelta(hours = leave_time)
					arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
						"approval_levels."+str(j+1)+".a_time":time}
					update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry4})
					msg=employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved and is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
					message2='Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved by '+get_employee_name(uid)+' and is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
					message= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved by '+get_employee_name(uid)+'and is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
					notification(c_leave[0]['employee_id'],message2)
					sms1='Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
					#get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message2,sms1)
					notification(get_hr_id(c_leave[0]['plant_id']),message)
					sms2=get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
					#get_employee_email_mobile(get_hr_id(c_leave[0]['plant_id']),'My Portal Leave Notification',message,sms2)
					message1= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been approved by '+get_employee_name(uid)+'and it is pending for your approval.\n You can approve the leave by clicking on this link : http://myportal.pennacement.com/leave/approvals/'
					notification(c_leave[0]['approval_levels'][j+1]['a_id'],message1)
					sms3=get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' is pending for approval by you'
					#get_employee_email_mobile(c_leave[0]['approval_levels'][j+1]['a_id'],'My Portal Leave Notification',message1,sms3)
			elif form['submit'] == 'Reject':
				arry2={"approval_levels."+str(j)+".a_status":"rejected","status":"rejected"}
				update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry2})
				msg=employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been rejected'
				message1='Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been rejected by '+get_employee_name(uid)
				message= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been rejected by '+get_employee_name(uid)
				notification(c_leave[0]['employee_id'],message1)
				sms1='Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been rejected by '+get_employee_name(uid)
				#get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message1,sms1)
				notification(get_hr_id(c_leave[0]['plant_id']),message)
				sms2=get_employee_name(c_leave[0]['employee_id']) +' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been rejected by '+get_employee_name(uid)
				#get_employee_email_mobile(get_hr_id(c_leave[0]['plant_id']),'My Portal Leave Notification',message,sms2)
			elif form['submit'] == 'Discuss':
				arry1={"approval_levels."+str(j)+".a_discuss":"1"}
				update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry1})
				msg= 'You have been successfully raised a request to DISCUSS with '+ employee_name
				message= 'Meet '+get_employee_name(uid)+' to discuss about your '+leave_type
				notification(c_leave[0]['employee_id'],message)
				sms='Meet '+get_employee_name(uid)+' to discuss about your '+leave_type
				#get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,sms)
				notification(uid,msg)
			flash(msg,'alert-success')
			return redirect(url_for('zenleave.leaveapprovals'))
		return render_template('leave_approvals.html', user_details=user_details, employee_att=a)
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/holdleaves/', methods = ['GET', 'POST'])
@login_required
def holdleaves():
	p =check_user_role_permissions('547c7e0157fbb30ed690336e')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid = current_user.username
		employee_att=find_in_collection('Leaves',{"status": "applied","plant_id":get_plant_id(uid),"hold_status":"0"})
		a=[]
		for e in employee_att:
			e['fullname']=get_employee_name(e['employee_id'])
			e['reason']=e['leave_reason']
			if e['attribute'] == 'attendance':
				e['leave']=get_attendance_type(e['leave_type'])
			else:
				e['leave']=get_leave_type(e['leave_type'])			
			a=a+[e]
		if request.method == 'POST':
			form=request.form
			c_leave =find_and_filter('Leaves',{"_id":ObjectId(form['leaveid'])},{"_id":0,"employee_id":1,"attribute":1,"approval_levels":1,"leave_type":1,"start_date":1,"end_date":1})
			employee_name =	get_employee_name(c_leave[0]['employee_id'])
			e_attribute = c_leave[0]['attribute']
			if e_attribute == 'attendance':
				leave_type=get_attendance_type(c_leave[0]['leave_type'])
			else:
				leave_type=get_leave_type(c_leave[0]['leave_type'])	
			count_a_levels = len(c_leave[0]['approval_levels'])
			for i in range(0,count_a_levels):
				if c_leave[0]['approval_levels'][i]['a_status'] == 'current':
					j=i
			if form['submit']== 'Hold':
				update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"hr_status":"hold","approval_levels."+str(j)+".a_hold_time":datetime.datetime.now()}})
				msg= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been hold successfully'
				message= 'Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been hold by HR'
				message1= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been hold by HR'
				notification(c_leave[0]['employee_id'],message)
				#get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,message)
				notification(uid,message1)
				#get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
			elif form['submit']== 'Unhold':
				update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"hr_status":"unhold","hold_status":"1","approval_levels."+str(j)+".a_unhold_time":datetime.datetime.now()}})
				msg= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been unhold successfully'
				message= 'Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been unhold by HR'
				message1= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been unhold by HR'
				notification(c_leave[0]['employee_id'],message)
				#get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,message)
				notification(uid,message1)
				#get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
			flash(msg,'alert-success')	
			return redirect(url_for('zenleave.holdleaves'))
		return render_template('hold_leaves.html', user_details=user_details, employee_att=a)
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/history/')
@login_required
def leave_history():
	p =check_user_role_permissions('546f2071850d2d1b00023b24')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid = current_user.username
		current_year=datetime.datetime.now().year
		lh=find_in_collection('Leaves',{"employee_id":uid,'$or':[{"status":"approved"},{"status":"rejected"}]})
		a=[]
		for e in lh:
			e['reason']=e['leave_reason']
			if e['attribute'] == 'attendance':
				e['leave']=get_attendance_type(e['leave_type'])
			else:
				e['leave']=get_leave_type(e['leave_type'])			
			a=a+[e]
		chartdata=find_and_filter('ChartData',{"employee_id":uid,"year":current_year},{"_id":0,"chartdata":1})
		return render_template('leave_history.html', user_details=user_details,lh=a,chartdata=chartdata[0]['chartdata'])
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/reports')
@login_required
def reports():
	p =check_user_role_permissions('5487ed0757fbb30f702709c0')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid = current_user.username
		lh=find_in_collection('Leaves',{"plant_id":get_plant_id(uid),'$or':[{"status":"approved"},{"status":"rejected"}]})
		a=[]
		for e in lh:
			e['fullname']=get_employee_name(e['employee_id'])
			e['reason']=e['leave_reason']
			if e['attribute'] == 'attendance':
				e['leave']=get_attendance_type(e['leave_type'])
			else:
				e['leave']=get_leave_type(e['leave_type'])			
			a=a+[e]
		return render_template('reports.html', user_details=user_details,lh=a)
	else:
		return redirect(url_for('zenuser.index'))


@zen_leave.route('/levels/', methods = ['GET', 'POST'])
@login_required
def levels():
	p =check_user_role_permissions('546f2097850d2d1b00023b26')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		uid= current_user.username
		plant_id=get_plant_id(uid)
		p_employees = find_and_filter('Userinfo',{"plant_id":plant_id},
			{"_id":0,"username":1,"designation_id":1,"f_name":1,"l_name":1,"m1_pid":1,"m2_pid":1,"m3_pid":1,
				"m4_pid":1,"m5_pid":1,"m6_pid":1,"m7_pid":1,"m1_name":1,
				"m2_name":1,"m3_name":1,"m4_name":1,"m5_name":1,
				"m6_name":1,"m7_name":1})
		a=[]
		for p in p_employees:
			q=find_one_in_collection('Positions',{"position_id":p['designation_id']})
			if 'levels' in q:
				p['levels']= q['levels']
			a=a+[p]	
		if request.method == 'POST':
			form=request.form
			for i in range(0,len(p_employees)):
				e_id = p_employees[i]['username']
				if e_id in form:
					p_id = form[e_id]
					if p_id in form:
						update_collection('Positions',{"position_id":p_id},{'$set' : {"levels" : form.getlist(p_id)}})
					else:
						update_collection('Positions',{"position_id":p_id},{'$set' : {"levels" : []}})
			flash('Save changes successfully','alert-success')
			return redirect(url_for('zenleave.levels'))
		return render_template('approval_levels.html', user_details=user_details, p_employees=a)
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/mylog/', methods = ['GET', 'POST'])
@login_required
def mylog():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	uid= current_user.username
	mlog=find_and_filter('Notifications',{"e_id":uid},{"msg":1,"_id":0,"created_time":1})
	return render_template('mylog.html', user_details=user_details,mlog=mlog)
        
@zen_leave.route('/time/', methods = ['GET', 'POST'])
@login_required
def time():
	p =check_user_role_permissions('54ae3abd507c00c54ba22299')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details = base()
		uid= current_user.username
		plant_id=get_plant_id(uid)
		plant_details=find_one_in_collection('Plants',{"plant_id":plant_id})
		if request.method == 'POST':
			form=request.form		
			update_collection('Plants',{"plant_id":plant_id},{"$set":{"leave_time":int(form['time'])}})
			flash('Leave time updated successfully','alert-success')
			return redirect(url_for('zenleave.time'))
		return render_template('time.html', user_details=user_details,plant_details=plant_details)
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/ehistory/')
@login_required
def employee_leave_history():
	p =check_user_role_permissions('54b378d4507c00c54ba2229b')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		employees_list=get_employees_under_manager()
		return render_template('employee_leave_history.html', user_details=user_details,el=employees_list)
	else:
		return redirect(url_for('zenuser.index'))

@zen_leave.route('/getleaves', methods = ['GET', 'POST'])
@login_required
def getleaves():
	deg = str(request.form.get('deg'))
	lh=find_in_collection('Leaves',{"employee_id":deg,'$or':[{"status":"approved"},{"status":"rejected"}]})
	a=[]
	if lh:
		for e in lh:
			e['_id']=str(e['_id'])
			e['reason']=e['leave_reason']
			if e['attribute'] == 'attendance':
				e['leave']=get_attendance_type(e['leave_type'])
			else:
				e['leave']=get_leave_type(e['leave_type'])			
			a=a+[e]
	leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":deg})
	leave_quota['_id']=str(leave_quota['_id'])
	leave_quota['sls_balance']=leave_quota['available_sls']-leave_quota['sls_taken']
	leave_quota['els_balance']=leave_quota['available_els']-leave_quota['els_taken']
	leave_quota['cls_balance']=leave_quota['available_cls']-leave_quota['leaves_taken']
	return jsonify({"leaves":a,"leave_quota":leave_quota})

@zen_leave.route('/checkappliedleave', methods = ['GET', 'POST'])
@login_required
def checkappliedleave():
	form=request.form
	if 'employee' in form:
		uid=form['employee']
	else:
		uid= current_user.username	
	if 'halfday' in form:
		list1= form.getlist('halfcheck')
	else:
		list1 = []
	data=list_of_holidays_total_days(uid,form['start_date'],form['end_date'],list1,form['leave_type'])
	return jsonify(data)

@zen_leave.route('/eapply/', methods = ['GET', 'POST'])
@login_required
def eapply_leave():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	uid= current_user.get_id()
	user_data1 = find_and_filter('Userinfo',{"_id":ObjectId(uid)},{"_id":0,"plant_id":1})
	employee_data=find_and_filter('Userinfo',{"plant_id":user_data1[0]['plant_id']},{"_id":0,"username":1,"f_name":1,"l_name":1})
	if request.method == 'POST':
		form=request.form
		uname=form['employee']
		user_data = find_and_filter('Userinfo',{"username":uname},{"calendar_code":1,"designation_id":1,"_id":0,"plant_id":1})
		leave_att = form['leave']
		if 'halfday' in form:
			list1= form.getlist('halfcheck')
		else:
			list1 = []
		if(leave_att == '1'):
			br1=business_rule_1(uname,form['leave_type'])
			if br1 != '0':
				flash(br1,'alert-danger')
				return redirect(url_for('zenleave.eapply_leave'))
			total_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
			if form['leave_type'] == 'CL':
				employee_group=find_and_filter('Userinfo',{"username":uname},{"ee_group":1,'_id':0})
				leaves_available=business_rule_3(uname,employee_group[0]['ee_group'])
				if total_days > leaves_available:
					flash('No of leaves applied is greater than available leaves','alert-danger')
					return redirect(url_for('zenleave.eapply_leave'))
			if form['leave_type'] == 'EL':
				if business_rule_4(uname) > 4:
					flash('You have already applied Earned Leaves 4 times','alert-danger')
					return redirect(url_for('zenleave.eapply_leave'))
			br2=business_rule_2(uname,form['start_date'],form['end_date'],list1,form['leave_type'])
			if br2 != '0':
				flash(br2,'alert-danger')
				return redirect(url_for('zenleave.eapply_leave'))
			attribute="leave"
			leave_type=get_leave_type(form['leave_type'])
			msg= 'Leave applied successfully'
		else:
			attribute="attendance"
			leave_type=get_attendance_type(form['leave_type'])
			msg= 'Attendance applied successfully'
		b=find_and_filter('Positions',{"position_id":user_data[0]['designation_id']},{"_id":0,"levels":1})
		d=[]
		k={}
		s=[]
		leave_time=get_leave_time(user_data[0]['plant_id'])
		if leave_time == 0:
			time='None'
		else:
			time=datetime.datetime.now()+datetime.timedelta(hours = leave_time)
		if b[0]['levels']:
			count_levels=len(b[0]['levels'])
			for j in range(0,count_levels):
				k[b[0]['levels'][j]]=1
				k['_id']=0
			g=find_and_filter('Userinfo',{"username":uname},k)
			for p in g[0]:
				s.append(g[0][p])
			print s
			count_levels2=len(s)
			for i in range(0,count_levels2):
				c=find_and_filter('Userinfo',{"designation_id":s[i]},{"username":1,"_id":0})
				if i == 0:
					d= d+[{"a_status":"current","a_id":c[0]['username'],"a_discuss":'0',"a_time":time}]
				else:
					d= d+[{"a_status":"waiting","a_id":c[0]['username'],"a_discuss":'0'}]			
			array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
				"start_date":form['start_date'],"end_date":form['end_date'],"applied_time":datetime.datetime.now(),
				"halfdays":list1, "employee_id":uname,"status":"applied","approval_levels":d,
				"hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0"}
			save_collection('Leaves',array)
			if request.files['doc']:
				input_file = request.files['doc']
				file_save_gridfs(input_file,"application/pdf","doc") 
			d=find_and_filter('Userinfo',{"designation_id":s[0]},{"username":1,"_id":0})
			message= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			message1= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			notification(uname,message1)
			sms1='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			#get_employee_email_mobile(uname,'My Portal Leave Notification',message1,sms1)
			notification(d[0]['username'],message)
			sms2=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			#get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms2)
			notification(get_hr_id(user_data[0]['plant_id']),message)
			#get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message,sms2)
		else:
			array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
				"start_date":form['start_date'],"end_date":form['end_date'],"applied_time":datetime.datetime.now(),
				"halfdays":list1, "employee_id":uname,"status":"approved","approval_levels":d,
				"hr_id":user_data[0]['plant_id']}
			save_collection('Leaves',array)
			message1= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			message2= get_employee_name(uname)+' '+leave_type+' approved '+' from '+form['start_date']+' to '+form['end_date']
			message3= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			message4= 'Your '+leave_type+' approved '+' from '+form['start_date']+' to '+form['end_date']
			notification(uname,message3)
			sms3='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			#get_employee_email_mobile(uname,'My Portal Leave Notification',message3,sms3)
			notification(get_hr_id(user_data[0]['plant_id']),message1)
			sms1=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
			#get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message1,sms1)
			notification(uname,message4)
			sms4='Your '+leave_type+' from '+form['start_date']+' to '+form['end_date']+'has been approved'
			#get_employee_email_mobile(uname,'My Portal Leave Notification',message4,sms4)
			notification(get_hr_id(user_data[0]['plant_id']),message2)
			#get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message2,sms4)
		flash(msg,'alert-success')
		return redirect(url_for('zenleave.eapply_leave'))
	return render_template('e_apply_leave.html', user_details=user_details,employee_data=employee_data)

# @zen_leave.route('/eplant/', methods = ['GET', 'POST'])
# @login_required
# def eapply_leave():
# 	user_details = base()

# 		#return redirect(url_for('zenleave.eplant'))
# 	return render_template('plant.html', user_details=user_details)

@zen_leave.route('/get_calendar_and_quota', methods = ['GET', 'POST'])
@login_required
def get_calendar_and_quota():
	deg = str(request.form.get('deg'))
	leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":deg})
	leave_quota['_id']=str(leave_quota['_id'])
	leave_quota['sls_balance']=leave_quota['available_sls']-leave_quota['sls_taken']
	leave_quota['els_balance']=leave_quota['available_els']-leave_quota['els_taken']
	leave_quota['cls_balance']=leave_quota['available_cls']-leave_quota['leaves_taken']
	calendar=get_calendar_data(deg)
	return jsonify({"leave_quota":leave_quota,"calendar":calendar})


@zen_leave.route('/uploadleave/')
def uploadleave():
	c=os.path.abspath("zenapp/static/leave.csv")		
	with open(c) as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			halfdays=[]	
			approval_levels=[]
			edate=datetime.datetime.strptime(row['TOLEAVEDATE'],"%Y-%m-%d")
			end_date=edate.strftime("%d/%m/%Y")
			sdate=datetime.datetime.strptime(row['FROMLEAVEDATE'],"%Y-%m-%d")
			start_date=sdate.strftime("%d/%m/%Y")
			if row['FROMLEAVETYPE']=="fhalf" and row['TOLEAVETYPE']=="fhalf":
				halfdays.append("1")
			elif row['FROMLEAVETYPE']=="shalf" and row['TOLEAVETYPE']=="shalf":
				halfdays.append("2")
			elif row['FROMLEAVETYPE']=="shalf" and row['TOLEAVETYPE']=="fhalf":
				halfdays.append("1")
				halfdays.append("2")
			array={"status" : "approved","approval_levels":approval_levels,"attribute" : "leave","hold_status" : "0","leave_reason" :row['REASON'],"hr_status" :"unhold", "employee_id":row['EMPID'],"end_date" :end_date,"halfdays" : halfdays,"applied_time":row['LEV_APPLY_DATE'] ,"leave_type" :row['LTID'],"plant_id" : "HY","start_date" :start_date, "sap":"upload"}
			print array
			save_collection('Leaves',array)
	return 'success'
