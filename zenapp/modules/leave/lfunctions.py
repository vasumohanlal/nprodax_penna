from zenapp.dbfunctions import *
from bson.objectid import ObjectId
from zenapp.modules.user.userfunctions import *
from flask.ext.login import current_user
import datetime
import calendar
#from pyrfc import *
def get_employee_name(e_id):
	data=find_and_filter('Userinfo',{"username":e_id},{"_id":0,"f_name":1,"l_name":1})
	fullname=data[0]['f_name']+' '+data[0]['l_name']
	return fullname

def get_attendance_type(a_id):
	data=find_one_in_collection('AttendanceTypes',{"att_id":a_id})
	return data['att_type']

def get_leave_type(l_id):
	data=find_one_in_collection('LeaveTypes',{"leave_id":l_id})
	return data['leave_type']

def get_leave_reason(r_id):
	data=find_one_in_collection('LeaveReasons',{"reason_id":r_id})
	return data['reason']

def get_employee_id(e_pid):
	data=find_and_filter('Userinfo',{"designation_id":e_pid},{"_id":0,"username":1})
	return data[0]['username']

def get_hr_id(plant_id):
	data=find_and_filter('Plants',{"plant_id":plant_id},{"_id":0,"plant_current_hr":1})
	data1=find_and_filter('Userinfo',{"designation_id":data[0]['plant_current_hr']},{"_id":0,"username":1})
	return data1[0]['username']

def get_plant_id(uid):
	if current_user.username !="00SHR":
		data=find_and_filter('Userinfo',{"username":uid},{"_id":0,"plant_id":1})
		return data[0]['plant_id']

def get_employee_name_given_pid(p_id):
	data=find_and_filter('Userinfo',{"designation_id":p_id},{"_id":0,"f_name":1,"l_name":1})
	fullname=data[0]['f_name']+' '+data[0]['l_name']
	return fullname
#send email or sms or notification to employee
def get_employee_email_mobile(uid,emailsubject,emailmsg,smsg):
	data=find_and_filter('Userinfo',{"username":uid},{"_id":0,"official_email":1,"official_contact":1,"personal_contact1":1})
	fname=get_employee_name(uid)
	if data[0]['official_email'] != '':
		save_collection('Email',{"emailsubject":emailsubject,"receiver":data[0]['official_email'],"msg":emailmsg,"fname":fname,"flag":"1"})
	elif data[0]['official_contact'] != '':
		SendSms(smsg,data[0]['official_contact'],fname)
	elif data[0]['personal_contact1'] != '':
		SendSms(smsg,data[0]['personal_contact1'],fname)
	else:
		notification(uid,'Please update your official email or official mobile number or personal mobile number')

def get_leave_time(plant_id):
	data=find_and_filter('Plants',{"plant_id":plant_id},{"_id":0,"leave_time":1})
	return data[0]['leave_time']

def get_employee_designation_id(e_id):
	data=find_and_filter('Userinfo',{"username":e_id},{"_id":0,"designation_id":1})
	return data[0]['designation_id']

def get_employees_list():
	uid = current_user.username
	employees_list=[{'PERNR':uid}]
	d_id=get_employee_designation_id(uid)
	p_id=get_plant_id(uid)
	data=find_and_filter('Userinfo', {'$or': [{"m1_pid":d_id},{"m2_pid":d_id},{"m3_pid":d_id},{"m4_pid":d_id},{"m5_pid":d_id},{"m6_pid":d_id},{"m7_pid":d_id}]},{"_id":0,"username":1})
	if data:
		for i in range(0,len(data)):
			employees_list+=[{'PERNR':data[i]['username']}]
	return employees_list

def get_employees_under_manager():
	uid = current_user.username
	employees_list=[{'PERNR':uid}]
	d_id=get_employee_designation_id(uid)
	p_id=get_plant_id(uid)
	data=find_and_filter('Userinfo', {'$or': [{"m1_pid":d_id},{"m2_pid":d_id},{"m3_pid":d_id},{"m4_pid":d_id},{"m5_pid":d_id},{"m6_pid":d_id},{"m7_pid":d_id}]},{"_id":0,"username":1,"f_name":1,"l_name":1})
	return data

def get_calendar_data(e_id):
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":e_id}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00"}},
		{'$project':{'_id':0,'date':'$schedule.date'}}])
	k=[]
	if data['result']:		
		for i in range(0,len(data['result'])):
			k+=[{"date":str(data['result'][i]['date'].month)+'-'+str(data['result'][i]['date'].day)+'-'+str(data['result'][i]['date'].year)}]
	return k
# employee cannot apply a leave if same type of previous leave is under process
def business_rule_1(uname,leave_type):
	leave_ex=find_one_in_collection('Leaves',{"employee_id":uname,"attribute":"leave","leave_type":leave_type,"status":"applied"})
	if leave_ex:
		m='Your previous '+leave_type+' is in process. please apply '+leave_type+' once its approved.'
	else:
		m='0'
	return m
# sap validations
def business_rule_2(uname,start_date,end_date,list1,leave_type):
	sd=datetime.datetime.strptime(start_date,"%d/%m/%Y")
	newsd=sd.strftime("%Y%m%d")
	ed=datetime.datetime.strptime(end_date,"%d/%m/%Y")
	newed=ed.strftime("%Y%m%d")
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if len(list1) == 0 :
			result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=uname,SUBTY=leave_type,BEGDA=newsd,ENDDA=newed,GV_SIMULATION='X')
		elif len(list1) == 1 :
			if list1[0] == "1":
				result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=uname,SUBTY=leave_type,BEGDA=newsd,ENDDA=newed,BFLAG='X',GV_SIMULATION='X')
			else:
				result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=uname,SUBTY=leave_type,BEGDA=newsd,ENDDA=newed,EFLAG='X',GV_SIMULATION='X')
		else:
			result = conn.call('Z_LMS_LEAVE_APPLICATION',PERNR=uname,SUBTY=leave_type,BEGDA=newsd,ENDDA=newed,BFLAG='X',EFLAG='X',GV_SIMULATION='X')
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		if result['RETURN']['TYPE'] == 'E':
			m=result['RETURN']['MESSAGE']
		else:
			m='0'
	except:
		m='Unable to connect Sap user Credential'
		close_sap_user_credentials(sap_login['_id'])
	return m
# total no of leaves in a applied casual leave excluding holidays and weekly halfs
def total_no_of_days_leave(uname,start_date,end_date,list1):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	total_days=(edate-sdate).days+1
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':{'$gt':sdate,'$lt':edate}}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	total_days-=len(data['result'])
	if len(list1) == 1 :
		total_days-=0.5
	elif len(list1) == 2:
		total_days-=1
	return total_days

# total no of leaves in a applied earned/sick leave excluding holidays and weekly halfs
def total_no_of_days_el(uname,start_date,end_date,list1):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	total_days=(edate-sdate).days+1
	if len(list1) == 1 :
		total_days-=0.5
	elif len(list1) == 2:
		total_days-=1
	return total_days

# list of holidays in between start date and end date
# total no of leaves in a applied leave excluding holidays and weekly halfs
# start date is holiday or not
# end date is holiday or not 
def list_of_holidays_total_days(uname,start_date,end_date,list1,leave_type):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	total_days=(edate-sdate).days+1
	if len(list1) == 1 :
		total_days-=0.5
	elif len(list1) == 2:
		total_days-=1	
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':{'$gt':sdate,'$lt':edate}}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	if leave_type == 'CL' or leave_type == 'LWP' or leave_type == 'EL':
		total_days-=len(data['result'])
	data1=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':sdate}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	if len(data1['result']) == 0:
		st_date='No'
	else:
		st_date='Yes'
	data2=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':edate}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	if len(data2['result']) == 0:
		en_date='No'
	else:
		en_date='Yes'
	return {"total_days":total_days,"holidays":data['result'],"st_date":st_date,"en_date":en_date}
# no of cls available to apply now
def business_rule_3(uname,employee_type):
	leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":uname})
	current_month=datetime.datetime.now().month
	if employee_type == 'T':
		if (current_month < 11):
			leaves_available=current_month*0.5+1-leave_quota['leaves_taken']
		else:
			leaves_available=leave_quota['available_cls']-leave_quota['leaves_taken']
	else:
		if (current_month < 11):
			leaves_available=current_month+2-leave_quota['leaves_taken']
		else:
			leaves_available=leave_quota['available_cls']-leave_quota['leaves_taken']
	return leaves_available
# how many times els applied
def business_rule_4(uname):
	current_time=datetime.datetime.now()
	yr_first=datetime.datetime(current_time.year,1,1)
	next_year=datetime.datetime(current_time.year+1,1,1)
	el_applied=find_in_collection('Leaves',{"employee_id":uname,"leave_type":"EL","status":"approved","applied_time":{'$gte':yr_first,'$lt':next_year}})
	return len(el_applied)
#update approve leaves/attendance taken data
def update_approved_leaves_in_leave_quota(uid,leave_type,n_leaves):
	quota_details=find_one_in_collection('LeaveQuota',{"employee_id":uid})
	if leave_type == 'CL':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"leaves_taken":quota_details['leaves_taken']+n_leaves}})
	elif leave_type == 'SL':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"sls_taken":quota_details['sls_taken']+n_leaves}})
	elif leave_type == 'EL':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"els_taken":quota_details['els_taken']+n_leaves}})
	elif leave_type == 'LWP':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"lwps_taken":quota_details['lwps_taken']+n_leaves}})
	elif leave_type == '1':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"tours_taken":quota_details['tours_taken']+n_leaves}})
	elif leave_type == '2':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"onduty_taken":quota_details['onduty_taken']+n_leaves}})
	return 'success'

def no_of_holidays(uname,start_date,end_date):
	work_schedule=find_one_in_collection('WorkSchedule',{"employee_id":uname})
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':{'$gte':start_date,'$lte':end_date}}},
		{'$project':{'_id':0,'date':'$schedule.date'}}])
	return len(data['result'])

def get_leave_chart_data(uid,s_date,e_date,halfdays,leave_type):
	start_date=datetime.datetime.strptime(s_date,"%d/%m/%Y")
	end_date=datetime.datetime.strptime(e_date,"%d/%m/%Y")
	current_year=datetime.datetime.now().year
	if start_date.year == end_date.year and start_date.year==current_year:
		data=find_one_in_collection('ChartData',{"employee_id":uid,"year":start_date.year})
		if start_date.month == end_date.month:
			holidays=no_of_holidays(uid,start_date,end_date)
			tnd=1+end_date.day-start_date.day-0.5*len(halfdays)-holidays+data['chartdata'][start_date.month][leave_type]
			array={}
			update_coll('ChartData',{"employee_id":uid,"year":start_date.year},{'$set': {"chartdata."+str(start_date.month-1)+"."+leave_type:tnd}})			
		else:
			c=calendar.monthrange(start_date.year, start_date.month)[1]
			tnd=1+c-start_date.day+data['chartdata'][start_date.month][leave_type]
			month_end_date=datetime.datetime(start_date.year, start_date.month,c)
			holidays1=no_of_holidays(uid,start_date,month_end_date)
			tnd-+holidays1
			tnd1=end_date.day+data['chartdata'][end_date.month][leave_type]
			month_start_date=datetime.datetime(end_date.year, end_date.month,1)
			holidays2=no_of_holidays(uid,month_start_date,month_end_date)
			tnd1-+holidays2
			if len(halfdays) == 2 :
				tnd-=0.5
				tnd1-=0.5
			elif len(halfdays) == 1 :
				if halfdays[1]=="1":
					tnd-=0.5
				else:
					tnd1-=0.5
			update_coll('ChartData',{"employee_id":uid,"year":start_date.year},{'$set': {"chartdata."+str(start_date.month-1)+"."+leave_type:tnd,"chartdata."+str(end_date.month-1)+"."+leave_type:tnd1}})
	return 'success'
#if a CL is applied there should not be any SL or EL before or after
def business_rule_5(uname,start_date,end_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	new_sdate=sdate-datetime.timedelta(days=1)
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	new_edate=edate+datetime.timedelta(days=1)
	a='yes'
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_sdate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		if data['result'][0]['worked_hours'] == "0.00" :
			a = 'yes'
			new_sdate-=datetime.timedelta(days=1)
		else:
			a = 'no'
	b='yes'
	while(b == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_edate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		if data['result'][0]['worked_hours'] == "0.00" :
			b = 'yes'
			new_edate+=datetime.timedelta(days=1)
		else:
			b = 'no'
	print new_sdate
	print new_edate
	leaves=find_and_filter('Leaves',{"employee_id":uname,'$and':[{'$or':[{"leave_type":"SL"},{"leave_type":"EL"}]},{'$or':[{"status":"approved"},{"status":"applied"}]}]},{"_id":0,"end_date":1,"start_date":1})
	print leaves
	allow_applied_leave = 0
	for l in leaves:
		if new_sdate == datetime.datetime.strptime(l['end_date'], "%d/%m/%Y") :
			allow_applied_leave+=1
		elif new_edate == datetime.datetime.strptime(l['start_date'], "%d/%m/%Y") :
			allow_applied_leave+=1
	return allow_applied_leave 
		
#if EL or SL is applied there should not be any CL before or after
def business_rule_6(uname,start_date,end_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	new_sdate=sdate-datetime.timedelta(days=1)
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	new_edate=edate+datetime.timedelta(days=1)
	a='yes'
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_sdate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		if data['result'][0]['worked_hours'] == "0.00" :
			a = 'yes'
			new_sdate-=datetime.timedelta(days=1)
		else:
			a = 'no'
	b='yes'
	while(b == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_edate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		if data['result'][0]['worked_hours'] == "0.00" :
			b = 'yes'
			new_edate+=datetime.timedelta(days=1)
		else:
			b = 'no'
	leaves=find_and_filter('Leaves',{"employee_id":uname,"leave_type":"CL",'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	allow_applied_leave = 0
	print leaves
	if leaves:
		for l in leaves:
			if new_sdate == datetime.datetime.strptime(l['end_date'], "%d/%m/%Y") :
				allow_applied_leave+=1
			elif new_edate == datetime.datetime.strptime(l['start_date'], "%d/%m/%Y") :
				allow_applied_leave+=1
	print allow_applied_leave
	return allow_applied_leave 
#leave cannot apply in already applied leave dates
def business_rule_7(uname,start_date,end_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	leaves=find_and_filter('Leaves',{"employee_id":uname,'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	previous_leaves = 0
	for l in leaves:
		n_sdate=datetime.datetime.strptime(l['start_date'], "%d/%m/%Y")
		n_edate=datetime.datetime.strptime(l['end_date'], "%d/%m/%Y")
		if n_sdate <= sdate <= n_edate :
			previous_leaves+=1
		elif n_sdate <= edate <= n_edate :
			previous_leaves+=1
		elif sdate <= n_sdate <= edate :
			previous_leaves+=1
		elif sdate <= n_edate <= edate :
			previous_leaves+=1		
	return previous_leaves 

def business_rule_9(uname,p_date):
	leaves=find_and_filter('Leaves',{"employee_id":uname,"leave_type":"CL",'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	previous_leaves = 0
	for l in leaves:
		n_sdate=datetime.datetime.strptime(l['start_date'], "%d/%m/%Y")
		n_edate=datetime.datetime.strptime(l['end_date'], "%d/%m/%Y")
		if n_sdate <= p_date <= n_edate :
			previous_leaves+=1	
	return previous_leaves 

def business_rule_8(uname,start_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	new_sdate=sdate-datetime.timedelta(days=1)
	a='yes'
	b=0
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_sdate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		if data['result'][0]['worked_hours'] != "0.00" :
			p_leaves=business_rule_9(uname,new_sdate)
			if p_leaves > 0 :
				b+=1
				a = 'yes'
				new_sdate-=datetime.timedelta(days=1)
			else:
				a = 'no'
		else:
			a = 'yes'
			new_sdate-=datetime.timedelta(days=1)
	return b 


def business_rule_10(uname,end_date):
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	new_edate=edate+datetime.timedelta(days=1)
	a='yes'
	b=0
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_edate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		if data['result'][0]['worked_hours'] != "0.00" :
			p_leaves=business_rule_9(uname,new_edate)
			if p_leaves > 0 :
				b+=1
				a = 'yes'
				new_edate+=datetime.timedelta(days=1)
			else:
				a = 'no'
		else:
			a = 'yes'
			new_edate+=datetime.timedelta(days=1)
	return b 
