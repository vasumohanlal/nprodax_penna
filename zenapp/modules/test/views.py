from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
import os
import csv
import zipfile
import StringIO
from calendar import monthrange
#from pyrfc import *
from zenapp.modules.leave.lfunctions import *
zen_test = Blueprint('zentest', __name__, url_prefix='/test')


@zen_test.route('/test/')
@login_required
def test():
	p =check_user_role_permissions('55a5e95f0e071e8e5240bb74')
	if p:
		user_details=base()
		uid = current_user.username
		current_year=datetime.datetime.now().year
		lh=find_in_collection('Leaves',{"employee_id":uid,'$or':[{"status":"approved"},{"status":"rejected"}]})
		a=[]
		for e in lh:
			e['reason']=e['leave_reason']
			if e['attribute'] == 'attendance':
				e['leave']=get_attendance_type(e['leave_type'])
			else:
				e['leave']=get_leave_type(e['leave_type'])			
			a=a+[e]
		chartdata=find_and_filter('ChartData',{"employee_id":uid,"year":current_year},{"_id":0,"chartdata":1})
		return render_template('test.html', user_details=user_details,lh=a,chartdata=chartdata[0]['chartdata'])
	else:
		return redirect(url_for('zenuser.index'))

