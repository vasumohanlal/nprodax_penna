from wtforms import Form, TextField,TextAreaField, validators, SelectField, StringField, PasswordField, DateField, FileField, SelectMultipleField,widgets,HiddenField
from zenapp.dbfunctions import *


class SigninForm(Form):
	username = StringField('user name', [validators.required(), validators.length(max=10)])
	password = PasswordField('password') 
	date = StringField('date')
	changepwd = PasswordField('password')
	confirmchangepwd = PasswordField('password')

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label = False)
    option_widget = widgets.CheckboxInput()

class CreateRoleForm(Form):
    role_name = StringField('Role Name', [validators.Required()])     
    permissions = MultiCheckboxField('Select Permissions', choices=[])

class CreatePermissionForm(Form):
    name = StringField('Permission Name', [validators.Required()])
    description = StringField('Permission Description', [validators.Required()])

class ChangePasswordForm(Form):
    old_pass = PasswordField('Old password', [validators.Required()])
    new_pass = PasswordField('New password', [validators.Required(),validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat password')

class AssignRoles(Form):
    designations=HiddenField('Select Designation')
    select_role = MultiCheckboxField('Select Role/Roles to Assign',choices=[])

class MaterialsForm(Form):
    material_type=StringField('Enter Material Code')
    select_date= DateField('Select Date')

class EditRole(Form):
    roles=SelectField('Select Role',choices=[])
    select_permissions = MultiCheckboxField('Select Permissions',choices=[])


class AssignPlant(Form):
    plants=SelectField('Select plants',choices=[])
    select_permissions = MultiCheckboxField('Select Plants',choices=[])

class proj_detail(Form):
    proj_name = StringField('Project Name:', [validators.Required()])
    proj_by = StringField('Created By:', [validators.Required()])
    proj_head = SelectField('Assign Project Head',choices=[])
    proj_desc = TextAreaField('Description', [validators.Required()])