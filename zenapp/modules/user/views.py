from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
from zenapp.sessions import *
import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
#from pyrfc import *
import json
import random
import urllib2
import sys
zen_user = Blueprint('zenuser', __name__, url_prefix='/user')



@zen_user.route('/', methods = ['GET', 'POST'])
@login_required

def index():
	p =check_user_role_permissions('546f2271850d2d1b00023b2a')
	if p:
		user_details=base()
		uid = current_user.username
		lh=find_in_collection('Leaves',{"employee_id":uid, "status":"applied"})
		a=[]

		if lh:
			for e in lh:
				e['reason']=e['leave_reason']
				if e['attribute'] == 'attendance':
					e['leave']=get_attendance_type(e['leave_type'])
				else:
					e['leave']=get_leave_type(e['leave_type'])
				for k in e['approval_levels']:
					if k['a_status'] == 'current':
						e['current_discuss'] = k['a_discuss']
						e['current_al_name'] = get_employee_name(k['a_id'])
				e['hr_name']= get_employee_name(get_hr_id(e['plant_id']))			
				a=a+[e]
		if uid != 'admin':
			ams=find_in_collection_sort('Announcements',{"plant_id":get_plant_id(uid)},[("created_time",-1)])
		if request.method == 'POST':
			form=request.form
			update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"status":"rejected"}})
			revoked_info=find_one_in_collection('Leaves',{'_id':ObjectId(form['leaveid'])})
			if revoked_info['attribute'] == 'attendance':
				leave_type=get_attendance_type(revoked_info['leave_type'])
			else:
				leave_type=get_leave_type(revoked_info['leave_type'])
			message1='You have revoked '+leave_type+' from '+revoked_info['start_date']+' to '+revoked_info['end_date']
			notification(uid,message1)
			get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
			message2=get_employee_name(uid)+' revoked '+leave_type+' from '+revoked_info['start_date']+' to '+revoked_info['end_date']	
			notification(get_hr_id(revoked_info['plant_id']),message2)
			sms2=get_employee_name(uid)+' revoked '+leave_type+' from '+revoked_info['start_date']+' to '+revoked_info['end_date']
			get_employee_email_mobile(get_hr_id(revoked_info['plant_id']),'My Portal Leave Notification',message2,sms2)
			if revoked_info['approval_levels']:
				for i in range(0,len(revoked_info['approval_levels'])):
					notification(revoked_info['approval_levels'][i]['a_id'],message2)
					get_employee_email_mobile(revoked_info['approval_levels'][i]['a_id'],'My Portal Leave Notification',message2,sms2)
			flash(message1,'alert-success')
			return redirect(url_for('zenuser.index'))
		if uid != 'admin':
			if user_details['dob'][:5]==datetime.datetime.now().strftime("%d-%m"):
				if user_details['birthday_flag']==0:
					users = {"username": current_user.username}
					users1= {"$set":{"birthday_flag":1}}
					update_coll('Userinfo',users,users1)
					ran=random.randrange(1,5)
					return render_template('index.html', user_details=user_details,lh=a,ams=ams,birthday=ran)
				else:
					return render_template('index.html', user_details=user_details,lh=a,ams=ams)
			else:
				if user_details['birthday_flag']==1:
					users = {"username": current_user.username}
					users1= {"$set":{"birthday_flag":0}}
					update_coll('Userinfo',users,users1)
					return render_template('index.html', user_details=user_details,lh=a,ams=ams)
		try:
			return render_template('index.html', user_details=user_details,lh=a,ams=ams)
		except:
			return render_template('index.html', user_details=user_details,lh=a)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/signin/', methods = ['GET', 'POST'])
def signin():
	if current_user is not None and current_user.is_authenticated():
        	return redirect(url_for('zenuser.index'))
	form = SigninForm(request.form)
	if request.method == 'POST':
		if request.form['submit'] == 'login':		
			pw_hash = hashlib.sha1(form.password.data).hexdigest()
			user = find_by_username(form.username.data)
			pwd=find_one_in_collection('Userinfo',{'username':form.username.data})			
			if user is not None:
				if pwd['employee_status']=='inactive':
					flash('User is blocked','alert-danger')
					return redirect(url_for('zenuser.signin'))
				if pwd['password'] == "":
					if user.date == form.date.data:
						users = {"dob": form.date.data, "username": form.username.data}
						users1= {"$set":{"password": hashlib.sha1(str(form.changepwd.data)).hexdigest()}}
	 					update_collection('Userinfo',users,users1)
	 					flash('New password is updated','alert-success')
	 					return redirect(url_for('zenuser.signin')) 											
					else:
						flash('Password incorrect','alert-danger')
				elif pwd['password'] == pw_hash:
					if login_user(user):
						forget_pwd=find_one_in_collection('Userinfo',{'chngpwd':'1','username':form.username.data})
						if forget_pwd:
							return redirect(url_for('zenuser.changepassword'))
						else:
							try:
								user=find_one_in_collection('Userinfo',{'username':form.username.data})
								users = {"username": form.username.data}
								module = []
								moduleinfo ={"username":form.username.data,"l_name":user['l_name'],"logintime":datetime.datetime.now(),"lastlogin":'',"module":module}
								users1= {"$set":{"current_login":datetime.datetime.now(),"last_login":user['current_login']}}
			 					update_coll('Userinfo',users,users1)
			 					save_collection('logurl',moduleinfo)
							except:
								user=find_one_in_collection('Userinfo',{'username':form.username.data})
								users = {"username": form.username.data}
								users1= {"$set":{"current_login":datetime.datetime.now(),"last_login":"Welcome to My Portal"}}
			 					update_coll('Userinfo',users,users1)
							return redirect(request.args.get('next') or url_for('zenuser.index'))
					else:
						flash('Error','alert-danger')
				else:
					flash('Password incorrect','alert-danger')
					return redirect(url_for('zenuser.signin'))
			else:
				flash('Password incorrect','alert-danger')	
				return redirect(url_for('zenuser.signin'))
		elif request.form['submit'] == 'getpassword':
			user = find_one_in_collection('Userinfo',{'username':request.form['f_username']})
			if user["official_email"]!='' or user['official_contact']!='' or user['personal_contact1']!='':
				new_pwd=str(uuid.uuid4().get_hex().upper()[0:6])
				print new_pwd
				users = {"username": request.form['f_username']}
				users1= {"$set":{"password": hashlib.sha1(new_pwd).hexdigest(),"chngpwd":"1","status":"1"}}
				update_collection('Userinfo',users,users1)
				smsg= "Your password to login into MyPortal is "+new_pwd
				get_employee_email_mobile(request.form['f_username']," My Portal Profile Notification",smsg,smsg)
				if user["official_email"]!='':
					flash("Your password has been sent to your official email",'alert-success')
				elif user["official_contact"]!='':
					flash("Your password has been sent to your  official contact number",'alert-success')
				elif user["personal_contact1"]!='':
					flash("Your password has been sent to your  personal contact number",'alert-success')
			else:
				flash("Please Contact HR to reset your password",'alert-info')
			return redirect(url_for('zenuser.signin'))
	return render_template('signin.html', form = form)

@zen_user.route('/signout')
@login_required
def signout():
	session=find_one_in_collection('Userinfo',{'username':current_user.username})
	arr = {"username": session['username']}
	log_time = session['current_login']
	end_time = session['end_login']
	time_dif = log_time - end_time
	time3 = str(session['current_login'].date()) + str(time_dif)
	s_time = str(session['current_login'])
	s_time = ''.join(e for e in s_time if e.isalnum())
	e_time = str(session['end_login'])
	e_time = ''.join(e for e in e_time if e.isalnum())
	u_id = (int(s_time) + int(e_time))/ 20

	arr1= {"$set":{"username":session['username'],"uniqueid":str(u_id),"current_login":session['current_login'],"end_login":session['end_login'],"module_login": time3}}
	update_collection('usersession',{"uniqueid":u_id},arr1)
	users2= {"$set":{"end_login":datetime.datetime.now()}}
	update_coll('Userinfo',arr,users2)
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	update_coll('logurl',{'logintime':time},{'$set':{"lastlogin":datetime.datetime.now()}})
	logout_user()
	flash('You have been logged out.','alert-info')
	return redirect(url_for('zenuser.signin'))

# generating dynamic urls for images
@zen_user.route('/static/img/gridfs/<filename>')
@login_required
def gridfs_img(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
    return Response(thing, mimetype='image/jpeg')

@zen_user.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
    return Response(thing, mimetype='application/pdf')



@zen_user.route('/profile/', methods = ['GET', 'POST'])
@login_required
def profile():
	p =check_user_role_permissions('546f22c5850d2d1b00023b30')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	username = session1[0]['username']
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details = base()
		user_details['dob']=current_user.date
		prof_status = find_one_in_collection('Gendata',{"_id":ObjectId("5465de13850d2d61a4a4e25d")})
		dependant=find_one_in_collection('Gendata',{"_id":ObjectId("54a92f9e850d2d5ce738308e")})
		position_details=find_one_in_collection('Positions',{"position_id":user_details['designation_id']})
		
		rms=[]
		try:
			for i in range(0,len(position_details['levels'])):
				if user_details['m'+str(i+1)+'_pid'] != '' and user_details['m'+str(i+1)+'_pid'] in position_details['levels']:
					rms+=[user_details['m'+str(i+1)+'_name']]
		except:
			print 'error'
		return render_template('profile.html', user_details=user_details ,prof_status=prof_status,dependant=dependant,rms=rms)

	else:
		return redirect(url_for('zenuser.index'))


@zen_user.route('/checkuser', methods=['GET', 'POST'])
def checkuser():
	user =  request.form['username']
	users = {"username": user,"password" : ""}
	a=find_one_in_collection('Userinfo',users) 
	if a:
		return '1'
	else:
		users = {"username": user}
		b=find_one_in_collection('Userinfo',users) 
		if b:
			return '2'	
		else:
			return '0'
@zen_user.route('/checkemp', methods=['GET', 'POST'])
def checkemp():
	user =  request.form['employee_id']
	users = {"username": user}
	a=find_one_in_collection('Userinfo',users) 
	if a:
		if a['employee_status']=="active":
			return '1'
		else:
			return '2'
	else:
		return '0'

@zen_user.route('/checkdate', methods=['GET', 'POST'])
def checkdate():
	user =  request.form['username']
	date =  request.form['date']

	users = {"username": user,"dob" : date}
	a=find_one_in_collection('Userinfo',users) 
	if a:
		return '1'
	else:
		return '0'			

def file_save_gridfs(file_data,ctype,ctag):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username
    userpic = find_one_in_collection('fs.files',{"uid" :uid ,"tag" :ctag})
    if userpic:
    	del_doc('fs.chunks',{"files_id":userpic['_id']})
    	del_doc('fs.files',{"_id":userpic['_id']})
    file_ext = file_data.filename.rsplit('.', 1)[1]
    fs.put(file_data.read(), content_type=ctype, filename = str(uuid.uuid4())+'.'+file_ext, uid = uid,
        tag = ctag)

@zen_user.route('/imgupload', methods=['GET', 'POST'])
@login_required
def imgupload():
	input_file = request.files['image']
	file_save_gridfs(input_file,"image/jpeg","profileimg") 
	return '0'


@zen_user.route('/updateofficaluser', methods=['GET', 'POST'])
@login_required
def updateofficaluser():
	user_details = base()
	e=0
	a={}
	if(user_details['official_email']!=request.form['official_email']):
		a["official_email"]=request.form['official_email']
		e=e+1
	if(user_details['official_contact']!=request.form['official_contact']):
		a["official_contact"]=request.form['official_contact']
		e=e+1
	if(e > 0):
		 check= find_one_in_collection('Temp_userinfo',{'employee_id':current_user.username})
		 if check:
		 	users={'employee_id':current_user.username}
		 	users1={"$set":a}
		 	update_collection('Temp_userinfo',users,users1)
		 	return '0'
		 else:	
			a["employee_id"]=current_user.username
			a["plant_id"]=user_details["plant_id"]
			save_collection('Temp_userinfo',a)
			return '0'
	else:
		return '1'

@zen_user.route('/updateuser', methods=['GET', 'POST'])
@login_required
def updateuser():
	user_details = base()
	uid = current_user.get_id()
	usr_details = find_one_in_collection('Userinfo',{'_id': ObjectId(uid)})
	a={}
	e=0
	h=0

	if request.files['preaddress']:
		preaddress=request.files["preaddress"]
		file_save_gridfs(preaddress,"application/pdf","preaddress_proof") 
		b={}
		b["hno_street"]=request.form['prehno_street']
		b["address_line2"]=request.form['preaddress_line2']
		b["district"]=request.form['predistrict']
		b["pincode"]=request.form['prepincode']
		b["city"]=request.form['precity']
		b["state"]=request.form['prestate']
		b["subtype"]="2"
		b["country"]="IN"
		b["url"]=preaddress_pdf()
		a={"present_address":b}
		a["a_status"]="1"
		e=e+1
		

	if request.files['peraddress']:
		peraddress=request.files["peraddress"]
		file_save_gridfs(peraddress,"application/pdf","peraddress_proof")
		c={}
		c["hno_street"]=request.form['perhno_street']
		c["address_line2"]=request.form['peraddress_line2']
		c["district"]=request.form['perdistrict']
		c["pincode"]=request.form['perpincode']
		c["city"]=request.form['percity']
		c["state"]=request.form['perstate']
		c["subtype"]="1"
		c["country"]="IN"
		c["url"]=peraddress_pdf()
		a["permanent_address"]=c
		a["a_status"]="1"
		e=e+1
		



	if(request.form['m_status']!='select'):
		if(user_details['m_status']!=request.form['m_status']):
			a["m_status"]=request.form['m_status']
			a["p_status"]="1"
			e=e+1
	if(usr_details['personal_contact1']!=request.form['contact_no']):
		a["personal_contact1"]=request.form['contact_no']
		a["p_status"]="1"
		e=e+1
	if(usr_details['personal_contact2']!=request.form['contact_no2']):
		a["personal_contact2"]=request.form['contact_no2']
		a["p_status"]="1"
		e=e+1

	de=request.form["dependents"].split(',')


	list1=[]
	if usr_details['dependents']:
		for dependent in usr_details['dependents']:
			list1.append(dependent['relation'])
			list1.append(dependent['fname'])
			list1.append(dependent['lname'])
			list1.append(dependent['dob'])
		
		if list1 != de:
			dependents=[]
			for z in range(0,len(de),4):
				dependents+=[{"relation":de[z],"fname":de[z+1],"lname":de[z+2],"dob":de[z+3]}]
			a["dependents"]=dependents
			a["d_status"]="1"
			e=e+1
	else :
		dependents=[]
		for z in range(0,len(de),4):
			dependents+=[{"relation":de[z],"fname":de[z+1],"lname":de[z+2],"dob":de[z+3]}]
		a["dependents"]=dependents
		a["d_status"]="1"
		e=e+1

	if(user_details['personal_email']!=request.form['personal_email']):
		users = {'_id': ObjectId(uid)}
		users1= {"$set":{"personal_email":request.form['personal_email'],"p_status":"1"}}
 		update_collection('Userinfo',users,users1)
 		h=h+1




	if(e > 0):
		 check= find_one_in_collection('Temp_userinfo',{'employee_id':current_user.username})
		 if check:
		 	users={'employee_id':current_user.username}
		 	users1={"$set":a}
		 	update_collection('Temp_userinfo',users,users1)
		 	return '0'
		 else:	
			a["employee_id"]=current_user.username
			a["plant_id"]=user_details["plant_id"]
			save_collection('Temp_userinfo',a)
			return '0'
	elif(h>0):
		return '0'
	else:
		return '1'

@zen_user.route('/plant/', methods=['GET', 'POST'])
@login_required
def plant():
	p =check_user_role_permissions('557926faebd85951419de8fe')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		var = request.path
		update_collection('usersessions',{'username':current_user.username},{"url":var})
		plants_Details1=find_one_in_collection('Plants',{"present_super_HR": 1})
		plant_id1 = plants_Details1['plant_id']
		user_details = base()
		form = AssignPlant(request.form)
		list_of_roles = find_all_and_filter('Plants',{"plant_id":1,"plant_id1":1})
		form.plants.choices = [(str(x['_id']), x['plant_id']) for x in list_of_roles]
		form.plants.choices.insert(0, ("", "Select Plant"))
		list_of_permissions = find_all_and_filter('plant_id1',{"plant_id" : 1})
		form.select_permissions.choices = [(str(x['_id']), x['plant_id']) for x in list_of_permissions]	
		if request.method == 'POST':
			update_collection('Plants',{"plant_id":plant_id1},{'$set' : {"present_super_HR" : 0}})
			array={"_id":ObjectId(form.plants.data)}
			update_collection('Plants',array,{'$set' : {"present_super_HR" : 1}})
			plants_Details=find_one_in_collection('Plants',{"present_super_HR": 1})
			plant_id = plants_Details['plant_id']
			flash('You are the current HR of the plant'+" "+ plant_id,'alert-success')
		return render_template('plant.html', form=form, user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/getplants', methods=['GET', 'POST'])
def getplants():
	deg = request.form.get('deg')
	p_details=find_one_in_collection('Plants',{"_id":ObjectId(deg)})
	if p_details:
		permissions=p_details['plant_id']
	else:
		permissions=[]
	return jsonify({"plant_id":permissions})
# application/pdf

def test():
	test = []
	with open('/proc/cpuinfo') as f:
	#with open('/proc/meminfo') as f:
		for line in f:
			if line.strip():
				if line.rstrip('\n').startswith('model name'):
					modelname = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('vendor_id'):
					vendor = line.rstrip('\n').split(':')[1]	
				if line.rstrip('\n').startswith('address sizes'):
					size = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('cache size'):
					csize = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('cpu MHz'):
					cpuMhz = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('core id'):
					coreid = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('siblings'):
					siblings = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('apicid'):
					apicid = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('cpuid level'):
					couidlevel = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('clflush size'):
					clflushsize = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('cache_alignment'):
					cache_alignment = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('processor'):
					processor = line.rstrip('\n').split(':')[1]
	test1 = ['Model name','vendor_id','address sizes','cache size','cpu MHz','core id','siblings','apicid','cpuid level','clflush size','cache_alignment','processor']				
	test.append(test1[0]+":"+modelname)
	test.append(test1[1]+":"+vendor)
	test.append(test1[2]+":"+size)
	test.append(test1[3]+":"+csize)
	test.append(test1[4]+":"+cpuMhz)
	test.append(test1[5]+":"+coreid)
	test.append(test1[6]+":"+siblings)
	test.append(test1[7]+":"+apicid)
	test.append(test1[8]+":"+couidlevel)
	test.append(test1[9]+":"+clflushsize)
	test.append(test1[10]+":"+cache_alignment)
	test.append(test1[11]+":"+processor)
	return test
def mem():
	test = []
	with open('/proc/meminfo') as f:
		for line in f:
			if line.strip():
				if line.rstrip('\n').startswith('MemTotal'):
					memTotal = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('MemFree'):
					memFree = line.rstrip('\n').split(':')[1]	
				if line.rstrip('\n').startswith('Buffers'):
					buffers = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('Cached'):
					Cached = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('Active'):
					active = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('Inactive'):
					Inactive = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('Mapped'):
					Mapped = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('SwapTotal'):
					swapTotal = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('CommitLimit'):
					commitLimit = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('DirectMap4k'):
					DirectMap4k = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('DirectMap2M'):
					DirectMap2M = line.rstrip('\n').split(':')[1]
				if line.rstrip('\n').startswith('SwapFree'):
					SwapFree = line.rstrip('\n').split(':')[1]
	test1 = ['Mem Total','Mem Free','Buffers','Cached','Active','Inactive','Mapped','swapTotal','commitLimit','DirectMap4k','DirectMap2M','SwapFree']				
	test.append(test1[0]+":"+memTotal)
	test.append(test1[1]+":"+memFree)
	test.append(test1[2]+":"+buffers)
	test.append(test1[3]+":"+Cached)
	test.append(test1[4]+":"+active)
	test.append(test1[5]+":"+Inactive)
	test.append(test1[6]+":"+Mapped)
	test.append(test1[7]+":"+swapTotal)
	test.append(test1[8]+":"+commitLimit)
	test.append(test1[9]+":"+DirectMap4k)
	test.append(test1[10]+":"+DirectMap2M)
	test.append(test1[11]+":"+SwapFree)
	return test

@zen_user.route('/timesheet/', methods=['GET', 'POST'])
@login_required
def timesheet():
	if current_user.username == "admin" or current_user.username == "00SHR":
		E = "55a8d635a062c5f4374b58b5"
		D = "55acb08743decc7901429803"
		check = ""
		list_details = find_all_in_collection('timesheet')
	else:
		E = ""
		D = ""
		check = "true"
		list_details = find_in_collection('timesheet',{"username":current_user.username})
	user_details = base()
	form = proj_detail(request.form)
	list_of_heads = find_all_and_filter('project_staffs',{"project_head":1})
	form.proj_head.choices = [(str(x['_id']), x['project_head']) for x in list_of_heads]
	form.proj_head.choices.insert(0, ("", "Select Project Head"))
	edit_details = find_in_collection('timesheet',{"edit":1})
	if request.method == 'POST':
		save_collection('timesheet',{"username":current_user.username,"proj_name":form.proj_name.data,"created_by":form.proj_by.data,"proj_head":form.proj_head.data,"proj_desc":form.proj_desc.data,"created_time":datetime.datetime.now()})
		return redirect(url_for('zenuser.timesheet'))
	return render_template('timesheet.html', form=form ,user_details=user_details,list_of_heads = list_of_heads,list_details=list_details,E=E,D=D,edit_details=edit_details,check=check)
	

@zen_user.route('/createpermission/', methods=['GET', 'POST'])
@login_required
def createpermission():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	form = CreatePermissionForm(request.form)
	if request.method == 'POST' and form.validate():
		array={"name":form.name.data, "description":form.description.data, "created_time": datetime.datetime.now()}
		save_collection('Rolepermissions',array)
		flash('permission created successfully','alert-success')
		return redirect(url_for('zenuser.createpermission'))
	return render_template('permission.html', form=form, user_details=user_details)


@zen_user.route('/assignrole/', methods=['GET', 'POST'])
@login_required
def assignrole():
	p =check_user_role_permissions('546f22b9850d2d1b00023b2f')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details = base()
		form = AssignRoles(request.form)
		list_of_positions = find_all_and_filter('Positions',{"_id":0,"position_id":1,"position_name":1})
		list_of_roles = find_all_and_filter('Roles',{"role_name" : 1})
		form.select_role.choices = [(str(x['_id']), x['role_name']) for x in list_of_roles]	
		if request.method == 'POST':
			array={"position_id":form.designations.data}
			update_collection('Positions',array,{'$set' : {"roles" : form.select_role.data}})
			flash('Role assigned successfully','alert-success')
			return redirect(url_for('zenuser.assignrole'))
		return render_template('assign_role.html', form=form, user_details=user_details,list_of_positions=list_of_positions)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/editrole/', methods=['GET', 'POST'])
@login_required
def editrole():
	p =check_user_role_permissions('54b0fe8a507c00c54ba2229a')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details = base()
		form = EditRole(request.form)
		list_of_roles = find_all_and_filter('Roles',{"role_name":1,"permissions":1})
		form.roles.choices = [(str(x['_id']), x['role_name']) for x in list_of_roles]
		form.roles.choices.insert(0, ("", "Select Role"))
		list_of_permissions = find_all_and_filter('Rolepermissions',{"name" : 1})
		form.select_permissions.choices = [(str(x['_id']), x['name']) for x in list_of_permissions]	
		if request.method == 'POST':
			array={"_id":ObjectId(form.roles.data)}
			update_collection('Roles',array,{'$set' : {"permissions" : form.select_permissions.data}})
			flash('Role Updated successfully','alert-success')
			return redirect(url_for('zenuser.editrole'))
		return render_template('edit_role.html', form=form, user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))
		
@zen_user.route('/getpositionroles', methods=['GET', 'POST'])
def getpositionroles():
	deg = request.form.get('deg')
	p_details=find_one_in_collection('Positions',{"position_id":deg})
	if p_details:
		roles=p_details['roles']
	else:
		roles=[]
	return jsonify({"roles":roles})

@zen_user.route('/getrolepermissions', methods=['GET', 'POST'])
def getrolepermissions():
	deg = request.form.get('deg')
	p_details=find_one_in_collection('Roles',{"_id":ObjectId(deg)})
	if p_details:
		permissions=p_details['permissions']
	else:
		permissions=[]
	return jsonify({"permissions":permissions})

@zen_user.route('/changepassword', methods=['GET', 'POST'])
@login_required
def changepassword():
	user_details=base()
	form = ChangePasswordForm(request.form)	
	if request.method == 'POST' and form.validate():
		pw_hash = hashlib.sha1(form.old_pass.data).hexdigest()
		if pw_hash == user_details['password'] :
			pw_hash_new = hashlib.sha1(form.new_pass.data).hexdigest()

			update_coll('Userinfo',{"_id":ObjectId(current_user.get_id())},{'$set':{"password": pw_hash_new,"chngpwd":'0'}})
			flash('password updated successfully','alert-success')
			return redirect(url_for('zenuser.index'))
		else:
			flash('invalid password','alert-danger')
			return redirect(url_for('zenuser.changepassword'))
	return render_template('changepassword.html', user_details=user_details, form=form)

@zen_user.route('/profileapprovals/', methods = ['GET', 'POST'])
@login_required
def profileapprovals():
	p =check_user_role_permissions('546f22de850d2d1b00023b32')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		
		if request.method == 'POST':
			hr1 = find_one_in_collection('Temp_userinfo',{'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']})
			if request.form['submit'] == 'agree':
				del hr1['_id']
				del hr1['employee_id']
				try:
					hr1['permanent_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"peraddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']})
					del hr1['permanent_address']["url"]
				except:
					print "none"


				try:
					hr1['present_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"preaddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']})
					del hr1['present_address']["url"]
				except:
					print "none"

				users={'plant_id':get_plant_id(current_user.username),'username':request.form['id']}
				users1={"$set":hr1}
				update_collection('Userinfo',users,users1)
				use={'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']}
				del_doc('Temp_userinfo',use)
				name=get_employee_name(current_user.username)
				smsg="Your Profile changes has been accepted by "+name
				notification(request.form['id'],smsg)
				get_employee_email_mobile(request.form['id']," My Portal Profile Notification",smsg,smsg)
			elif request.form['submit'] == 'disagree':
				hr1 = find_one_in_collection('Temp_userinfo',{'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']})
				try:
					hr1['permanent_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"peraddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']})
				except:
					print "none"

				try:
					hr1['present_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"preaddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']}) 
				except:
					print "none"

				users={'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']}
				del_doc('Temp_userinfo',users)
				name=get_employee_name(current_user.username)
				smsg="Your Profile changes has been rejected by "+name
				notification(request.form['id'],smsg)
				get_employee_email_mobile(request.form['id']," My Portal Profile Notification",smsg,smsg)


				
		prof_status = find_one_in_collection('Gendata',{"_id":ObjectId("5465de13850d2d61a4a4e25d")})
		dependant=find_one_in_collection('Gendata',{"_id":ObjectId("54a92f9e850d2d5ce738308e")}) 		
		hr = find_in_collection('Temp_userinfo',{'plant_id':get_plant_id(current_user.username)})
		return render_template('profile_approvals.html', user_details=user_details,hr=hr,prof_status=prof_status,dependant=dependant)
	else:
		return redirect(url_for('zenuser.index'))


@zen_user.route('/notificationstamp', methods=['GET', 'POST'])
@login_required
def notificationstamp():
	chk=find_one_in_collection('Notificationtimestamp',{"username":current_user.username})
	if chk:
		update_collection('Notificationtimestamp',{"username":current_user.username},{"$set":{"created_time":datetime.datetime.now()}})
		return '0'
	else:
		save_collection('Notificationtimestamp',{"username":current_user.username,"created_time":datetime.datetime.now()})
		return '0'

@zen_user.route('/newemployee/', methods=['GET', 'POST'])
@login_required
def addnewemployee():
	p =check_user_role_permissions('546f228c850d2d1b00023b2c')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details=base()
		if request.method == 'POST':
			form=request.form
			if form['submit'] == 'Add':
				check_employee=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if check_employee:
					flash('Employee is already exist','alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
				# sap_login=get_sap_user_credentials()
			
				# conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				conn = Connection(user='portal_batch', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
				result = conn.call('Z_LMS_PERSONAL_DATA',PERNR=form['employee_id'])
				if result['IT_0002']:
					result1 = conn.call('Z_LMS_ADDRESS_DATA',PERNR=form['employee_id'])
					e = result['IT_0002'][0]
					dob = datetime.datetime.strptime(str(e['GBDAT']), '%Y-%m-%d')
					dob_new = dob.strftime('%d-%m-%Y')
					if e['EMPDOJ'] is None :
						doj_new='Not Available'
					else:
						doj = datetime.datetime.strptime(str(e['EMPDOJ']), '%Y-%m-%d')
						doj_new = doj.strftime('%d-%m-%Y')
					if result1['IT_0006T'] :
						f = result1['IT_0006T'][0]
						array3={"hno_street":f['STRAS'],"address_line2":f['LOCAT'],"city":f['ORT01'],
					"district":f['ORT02'],"state":f['STATE'],"country":f['LAND1'],"pincode":f['PSTLZ'],
					"subtype":f['SUBTY']}
					else:
						array3={}
					if result1['IT_0006P'] :
						p = result1['IT_0006P'][0]
						array4={"hno_street":p['STRAS'],"address_line2":p['LOCAT'],"city":p['ORT01'],
					"district":p['ORT02'],"state":p['STATE'],"country":p['LAND1'],"pincode":p['PSTLZ'],
					"subtype":p['SUBTY']}
					else:
						array4={}
					result2 = conn.call('Z_LMS_EMP_DEPENDENT_DATA',PERNR=form['employee_id'])
					current_year=datetime.datetime.now().year
					end_date=str(current_year)+'1231'
					start_date=str(current_year)+'0101'
					result3 = conn.call('Z_LMS_WORK_SCHEDULE',BEGDA=start_date,ENDDA=end_date,PERNR=form['employee_id'])
					result4 = conn.call('Z_LMS_LEAVE_QUOTA',P_PERNR=form['employee_id'])
					conn.close()
					# close_sap_user_credentials(sap_login['_id'])
					b=[]
					if result2['IT_0021']:
						for j in range(0,len(result2['IT_0021'])):
							if result2['IT_0021'][j]['DOB'] is not None:
								new_dob=result2['IT_0021'][j]['DOB'].strftime("%d-%m-%Y")
							else:
								new_dob=''
							b+=[{"fname":result2['IT_0021'][j]['FIRSTNAME'],"lname":result2['IT_0021'][j]['LASTNAME'],"relation":result2['IT_0021'][j]['SUBTY'],"dob":new_dob}]
					array2={"username":e['PERNR'][3:],"password":"","dob":dob_new,"f_name":e['VORNA'],"l_name": e['NACHN'],
							"m_status": e['FAMST'],"personal_contact1": e['CONTACT'],"personal_contact2": e['PCONTACT'],"official_contact": e['OCONTACT'],
							"personal_email": e['PEMAIL'],"official_email" :e['OEMAIL'],"department_id":e['ORGEH'],"department_name":e['ORGTX'],
							"gender":e['GESCH'],"designation_id":e['PLANS'],"designation":e['PLSTX'],"country":e['NATIO'],
							"created_time":datetime.datetime.now(),
							"present_address":array3,"permanent_address":array4,"pancard":e['ICNUM'],"calendar_code":e['MOFID'],
							"employee_status":"active","m1_pid":e['MANAGER1'],"m2_pid":e['MANAGER2'],"m3_pid":e['MANAGER3'],
							"m4_pid":e['MANAGER4'],"m5_pid":e['MANAGER5'],"m6_pid":e['MANAGER6'],"m7_pid":e['MANAGER7'],"m1_name":e['MAN_NAME1'],
							"m2_name":e['MAN_NAME2'],"m3_name":e['MAN_NAME3'],"m4_name":e['MAN_NAME4'],"m5_name":e['MAN_NAME5'],
							"m6_name":e['MAN_NAME6'],"m7_name":e['MAN_NAME7'],"plant_id":e['ABKRS'],"dependents":b,"doj":doj_new,"ee_group":e['PERSG'],"birthday_flag":0}
					save_collection('Userinfo',array2)
					save_collection('Notificationtimestamp',{"username":e['PERNR'][3:],"created_time":datetime.datetime.now()})						
					total_count=len(result3['IT_WORK_SCHDL'])
					d=[]	
					for j in range(0,total_count):
						new_date=result3['IT_WORK_SCHDL'][j]['DATE'].strftime("%d/%m/%Y")
						n_date=datetime.datetime.strptime(new_date,"%d/%m/%Y")
						d+=[{"date":n_date,"worked_hours":str(result3['IT_WORK_SCHDL'][j]['WORKED_HOURS'])}]
					save_collection('WorkSchedule',{"employee_id":form['employee_id'],"schedule":d,"year":current_year})
					if result4['IT_P006']:
						if len(result4['IT_P006']) == 3:
							save_collection('LeaveQuota',{"employee_id":form['employee_id'],"available_cls":int(float(str(result4['IT_P006'][1]['ANZHL']))),"leaves_taken":int(float(str(result4['IT_P006'][1]['KVERB']))),"available_sls":int(float(str(result4['IT_P006'][2]['ANZHL']))),"sls_taken":int(float(str(result4['IT_P006'][2]['KVERB']))),"available_els":int(float(str(result4['IT_P006'][0]['ANZHL']))),"els_taken":int(float(str(result4['IT_P006'][0]['KVERB']))),"lwps_taken":0,"tours_taken":0,"onduty_taken":0})
						elif len(result4['IT_P006']) == 2:
							save_collection('LeaveQuota',{"employee_id":form['employee_id'],"available_cls":int(float(str(result4['IT_P006'][1]['ANZHL']))),"leaves_taken":int(float(str(result4['IT_P006'][1]['KVERB']))),"available_sls":0,"sls_taken":0,"available_els":int(float(str(result4['IT_P006'][0]['ANZHL']))),"els_taken":int(float(str(result4['IT_P006'][0]['KVERB']))),"lwps_taken":0,"tours_taken":0,"onduty_taken":0})	
						elif len(result4['IT_P006']) == 1:
							save_collection('LeaveQuota',{"employee_id":form['employee_id'],"available_cls":int(float(str(result4['IT_P006'][0]['ANZHL']))),"leaves_taken":int(float(str(result4['IT_P006'][0]['KVERB']))),"available_sls":0,"sls_taken":0,"available_els":0,"els_taken":0,"lwps_taken":0,"tours_taken":0,"onduty_taken":0})	
					chart=[]
					for i in range(1,13):
						chart+=[{"month":i,"CL":0,"SL":0,"LWP":0,"EL":0}]
					current_year=datetime.datetime.now().year
					save_collection('ChartData',{"employee_id":form['employee_id'],"year":current_year,"chartdata":chart})
					list_of_holidays=find_one_in_collection('Calendar',{"calendar_id":e['MOFID']})		
					for h in list_of_holidays['holidays']:
						d=int(h['date'].strftime('%j'))-1
						arry5={"schedule."+str(d)+".worked_hours":"0.00"}
						update_coll('WorkSchedule',{"employee_id":form['employee_id']},{'$set': arry5})
					check_position=find_one_in_collection('Positions',{"position_id":e['PLANS']})
					if check_position is None:
						levels=[]
						if e['MANAGER1'] != '':
							levels+=[e['MANAGER1']]
							if e['MANAGER2'] != '':
								levels+=[e['MANAGER2']]
								if e['MANAGER3'] != '':
									levels+=[e['MANAGER3']]
									if e['MANAGER4'] != '':
										levels+=[e['MANAGER4']]
										if e['MANAGER5'] != '':
											levels+=[e['MANAGER5']]
											if e['MANAGER6'] != '':
												levels+=[e['MANAGER6']]
												if e['MANAGER7'] != '':
													levels+=[e['MANAGER7']]
						array6 = {"position_id":e['PLANS'], "position_name":e['PLSTX'],"roles":["5487f2a957fbb310be5f9ab1"],"levels":levels}
						save_collection('Positions',array6)
					flash('employee added successfully','alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					conn.close()
					# close_sap_user_credentials(sap_login['_id'])
					flash('Employee is not available in SAP','alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			
			elif form['submit'] == 'Block':
				chk=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if chk:
					update_collection('Userinfo',{"username":form['employee_id']},{"$set":{"employee_status":"inactive"}})
					msg=get_employee_name(form['employee_id'])+' blocked successfully'
					flash(msg,'alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='No user exist with employee ID: '+form['employee_id']
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Unblock':
				chk=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if chk:
					update_collection('Userinfo',{"username":form['employee_id']},{"$set":{"employee_status":"active"}})
					msg=get_employee_name(form['employee_id'])+' Unblocked successfully'
					flash(msg,'alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='No user exist with employee ID: '+form['employee_id']
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Reset Password':
				chk=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if chk:
					new_pwd=str(uuid.uuid4().get_hex().upper()[0:6])
					update_collection('Userinfo',{"username":form['employee_id']},{"$set":{"password":hashlib.sha1(new_pwd).hexdigest(),
						"chngpwd":"1"}})
					msg='Password generated successfully. New password: '+new_pwd
					flash(msg,'alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='No user exist with employee ID: '+form['employee_id']
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
		return render_template('addnewuser.html', user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))


@zen_user.route('/material', methods=['GET', 'POST'])
@login_required
def material():
	user_details=base()
	return render_template('material.html', user_details=user_details)

@zen_user.route('/getmaterialdetails', methods = ['GET', 'POST'])
@login_required
def getmaterialdetails():
	mat = request.form.get('mat')
	db = dbconnect()
	u_mat = db.Materials.aggregate([{'$match' : {"_id": ObjectId('547ef573850d2d44def5bb06')}},
		{ '$unwind': '$materials' },
                {'$match' : {'materials.MATNR' : mat}}, 
                {'$project' : {'materials.MATNR':1,"_id":0}}])
	if u_mat['result'] :
		sap_login=get_sap_user_credentials()
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_MATNR_DISP_STOCK', MATNR=mat)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		count = len(result['IT_FINAL'])
		mat_details=[]
		for i in range(0,count):
			array={"NAME1":str(result['IT_FINAL'][i]['NAME1']),"SPEME":str(result['IT_FINAL'][i]['SPEME']),
			"UMLME":str(result['IT_FINAL'][i]['UMLME']),"EINME":str(result['IT_FINAL'][i]['EINME']),
			"LABST":str(result['IT_FINAL'][i]['LABST']),"WINSM":str(result['IT_FINAL'][i]['WINSM']),
			"WSPEM":str(result['IT_FINAL'][i]['WSPEM']),"WLABS":str(result['IT_FINAL'][i]['WLABS']),
			"WEINM":str(result['IT_FINAL'][i]['WEINM']),"WRETM":str(result['IT_FINAL'][i]['WRETM']),
			"WUMLM":str(result['IT_FINAL'][i]['WUMLM']),"INSME":str(result['IT_FINAL'][i]['INSME']),
			"RETME":str(result['IT_FINAL'][i]['RETME']),"MATNR":str(result['IT_FINAL'][i]['MATNR'])}
			mat_details = mat_details+[array]
		array={"materials":mat_details,"flag":result['FLAG']}	
	else:
		array={"flag":'Y'}
	return jsonify(array)	

@zen_user.route('/getmateriallist', methods = ['GET', 'POST'])
@login_required
def getmateriallist():
	q = request.args.get('term')
	db=dbconnect()
	u_mat = db.Materials.aggregate([{'$match' : {"_id": ObjectId('547ef573850d2d44def5bb06')}},
		{ '$unwind': '$materials' },
                {'$match' : {'materials.MAKTX' : {'$regex': str(q),'$options': 'i' }}}, 
                {'$project' : {'materials.MATNR':1,'materials.MAKTX':1, "_id":0}}])
	if u_mat['result']:
		m=[]
		for mat in u_mat['result']:
			m=m+[mat['materials']]
	return json.dumps({"results":m})		

@zen_user.route('/trade/', methods = ['GET', 'POST'])
@login_required
def trade():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if request.method == 'POST':
		form=request.form
		if form['order'] == 'PTQT':
			im_auart='ZTRD'
		elif form['order'] == 'PDTQ':
			im_auart='ZDTD'
		sap_login=get_sap_user_credentials()
		try:
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
			result = conn.call('Z_BAPI_TRADE_ORDER_CREATE',IM_QUOT_NO=form['quot_no'],IM_AUART=im_auart,IM_BSTKD=form['po_no'],IM_PAYCON=form['payment_terms'])
			conn.close()
			close_sap_user_credentials(sap_login['_id'])
			if result['IT_RETURN'][0]['TYPE'] == 'S':
				array={"order_type":form['order'],"sold_to_id":form['sold_to_id'],"sold_to_add":form['sold_to_add'],
			"ship_to_id":form['ship_to_id'],"ship_to_add":form['ship_to_add'],"po_no":form['po_no'],
			"inco_terms":form['inco_terms'],"material_code":form['material_code'],"quantity":form['quantity'],
			"plant":form['plant'],"route":form['route'],"payment_terms":form['payment_terms'],"sales_rep":form['sales_rep'],
			"destination":form['destination'],"quot_no":form['quot_no'],"zinv":form['zinv'],"zfrt":form['zfrt'],
			"zfr3":form['zfr3'],"jivp":form['jivp'],"jivc":form['jivc'],"jext":form['jext'],"ject":form['ject'],
			"jset":form['jset'],"zbas":form['zbas'],"netwr":form['netwr'],"order_no":result['EX_VBELN']}
				save_collection('TradeData',array)
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-success')
			elif result['IT_RETURN'][0]['TYPE'] == 'E':
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-danger')
		except:
			close_sap_user_credentials(sap_login['_id'])
			flash('Unable to connect Sap user Credential','alert-danger')
		return redirect(url_for('zenuser.trade'))
	return render_template('trade.html', user_details=user_details)

@zen_user.route('/nontrade/', methods = ['GET', 'POST'])
@login_required
def nontrade():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if request.method == 'POST':
		form=request.form
		if form['order'] == 'PNQT':
			im_auart='ZNTD'
		elif form['order'] == 'PDNQ':
			im_auart='ZDNT'
		sap_login=get_sap_user_credentials()
		try:
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
			result = conn.call('Z_BAPI_NONTRADE_ORDER_CREATE',IM_QUOT_NO=form['quot_no'],IM_AUART=im_auart,IM_BSTKD=form['po_no'],IM_PAYCON=form['payment_terms'])
			conn.close()
			close_sap_user_credentials(sap_login['_id'])
			if result['IT_RETURN'][0]['TYPE'] == 'S':
				array={"order_type":form['order'],"sold_to_id":form['sold_to_id'],"sold_to_add":form['sold_to_add'],
			"ship_to_id":form['ship_to_id'],"ship_to_add":form['ship_to_add'],"po_no":form['po_no'],
			"inco_terms":form['inco_terms'],"material_code":form['material_code'],"quantity":form['quantity'],
			"plant":form['plant'],"route":form['route'],"payment_terms":form['payment_terms'],"sales_rep":form['sales_rep'],
			"destination":form['destination'],"quot_no":form['quot_no'],"zmnp":form['zmnp'],"zfrt":form['zfrt'],
			"zfr3":form['zfr3'],"jivp":form['jivp'],"jivc":form['jivc'],"jext":form['jext'],"ject":form['ject'],
			"jset":form['jset'],"zbas":form['zbas'],"zcom":form['zcom'],"ztri":form['ztri'],"zpdn":form['zpdn'],"netwr":form['netwr'],"order_no":result['EX_VBELN']}
				save_collection('NonTradeData',array)
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-success')
			elif result['IT_RETURN'][0]['TYPE'] == 'E':
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-danger')
		except:
			close_sap_user_credentials(sap_login['_id'])
			flash('Unable to connect Sap user Credential','alert-danger')
		return redirect(url_for('zenuser.nontrade'))
	return render_template('nontrade.html', user_details=user_details)
	
@zen_user.route('/getcustomerlist', methods = ['GET', 'POST'])
def getcustomerlist():
	q = request.args.get('term')
	db=dbconnect()
	u_cust = db.Customers.aggregate([
                {'$match' : { '$or':[{'NAME1' : {'$regex': str(q),'$options': 'i' }},{'KUNNR' : {'$regex': str(q),'$options': 'i' }}] }},
                {'$group':{"_id":{"NAME1":'$NAME1',"KUNNR":'$KUNNR'}}} 
                ])
	print u_cust
	return json.dumps({"results":u_cust['result']})

@zen_user.route('/getshiptopartylist', methods = ['GET', 'POST'])
def getshiptopartylist():
	q = str(request.form.get('deg'))
	ship_to_party=find_and_filter('Customers',{"KUNNR":q},{"_id":0,"KUNN2":1,'NAME1_WE':1,"LAND1":1,"LANDX":1,
                "STRAS":1,"TELF1":1,"PSTLZ":1,"PARVW":1,"REGIO":1,"ORT01":1,
                "BEZEI":1,"KUNN3":1})
	return jsonify({"results":ship_to_party})

@zen_user.route('/getshiptopartyadd', methods = ['GET', 'POST'])
def getshiptopartyadd():
	q = str(request.form.get('deg'))
	ship_to_party=find_and_filter('Customers',{"KUNN2":q},{"_id":0,"LAND1_WE":1,"LANDX_WE":1,
                "STRAS_WE":1,"TELF1_WE":1,"PSTLZ_WE":1,"PARVW_WE":1,"REGIO_WE":1,"ORT01_WE":1,
                "BEZEI_WE":1,"DESTINT":1})
	return jsonify(ship_to_party[0])

@zen_user.route('/getprice', methods = ['GET', 'POST'])
def getprice():
	form=request.form
	im_auart=form['order']
	im_kunnr=str(int(form['sold_to_id']))
	im_bstkd=form['po_no']
	im_inco1=form['inco_terms']
	im_matnr=form['material_code']
	im_kwmeng=str(form['quantity'])
	im_werks=form['plant']
	im_route=form['route']
	im_paycon=form['payment_terms']
	im_srep=str(int(form['sales_rep']))
	im_destint=form['destination']
	im_kunnr1=str(int(form['ship_to_id']))	 
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'],sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_BAPI_TRADE_ORDER_PRICING',IM_AUART=im_auart,IM_KUNNR=im_kunnr,IM_KUNNR1=im_kunnr1,IM_SREP=im_srep,IM_BSTKD=im_bstkd,IM_INCO1=im_inco1,IM_MATNR=im_matnr,IM_KWMENG=im_kwmeng,IM_WERKS=im_werks,IM_ROUTE=im_route,IM_PAYCON=im_paycon,IM_DESTINT=im_destint)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		if result['IT_RETURN'][0]['TYPE'] == 'S':
			a={"EX_QUOT_NO":str(result['EX_QUOT_NO']),"EX_ZINV":str(result['EX_ZINV']),"EX_ZFRT":str(result['EX_ZFRT']),
		"EX_ZFR3":str(result['EX_ZFR3']),"EX_JIVP":str(result['EX_JIVP']),"EX_JIVC":str(result['EX_JIVC']),
		"EX_JEXT":str(result['EX_JEXT']),"EX_JECT":str(result['EX_JECT']),"EX_JSET":str(result['EX_JSET']),
		"EX_ZBAS":str(result['EX_ZBAS']),"EX_NETWR":str(result['EX_NETWR']),"error":"no"}
		elif result['IT_RETURN'][0]['TYPE'] == 'E':
			a={"error":"yes","message":result['IT_RETURN'][0]['MESSAGE']}
	except:
		close_sap_user_credentials(sap_login['_id'])
		a={}
		a={"error":"yes","message":'Unable to connect Sap user Credential'}
	return jsonify(a)

@zen_user.route('/nontradegetprice', methods = ['GET', 'POST'])
def nontradegetprice():
	form=request.form
	im_auart=form['order']
	im_kunnr=str(int(form['sold_to_id']))
	im_bstkd=form['po_no']
	im_inco1=form['inco_terms']
	im_matnr=form['material_code']
	im_kwmeng=str(form['quantity'])
	im_werks=form['plant']
	im_route=form['route']
	im_paycon=form['payment_terms']
	im_srep=str(int(form['sales_rep']))
	im_destint=form['destination']
	im_kunnr1=str(int(form['ship_to_id']))
	im_partner_z2=form['comission_agent']
	im_zmnp=form['manual_price'] 
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_BAPI_NONTRADE_ORDER_PRICING',IM_AUART=im_auart,IM_KUNNR=im_kunnr,IM_KUNNR1=im_kunnr1,IM_PARTNER_Z1=im_srep,IM_BSTKD=im_bstkd,IM_INCO1=im_inco1,IM_MATNR=im_matnr,IM_KWMENG=im_kwmeng,IM_WERKS=im_werks,IM_ROUTE=im_route,IM_PAYCON=im_paycon,IM_DESTINT=im_destint,IM_PARTNER_Z2=im_partner_z2,IM_ZMNP=im_zmnp)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		if result['IT_RETURN'][0]['TYPE'] == 'S':
			a={"EX_QUOT_NO":str(result['EX_QUOT_NO']),"EX_ZFRT":str(result['EX_ZFRT']),
		"EX_ZFR3":str(result['EX_ZFR3']),"EX_JIVP":str(result['EX_JIVP']),"EX_JIVC":str(result['EX_JIVC']),
		"EX_JEXT":str(result['EX_JEXT']),"EX_JECT":str(result['EX_JECT']),"EX_JSET":str(result['EX_JSET']),
		"EX_ZBAS":str(result['EX_ZBAS']),"EX_NETWR":str(result['EX_NETWR']),"error":"no","EX_ZMNP":str(result['EX_ZMNP']),
		"EX_ZCOM":str(result['EX_ZCOM']),"EX_ZTRI":str(result['EX_ZTRI']),"EX_ZPDN":str(result['EX_ZPDN'])}
		elif result['IT_RETURN'][0]['TYPE'] == 'E':
			a={"error":"yes","message":result['IT_RETURN'][0]['MESSAGE']}
	except:
		close_sap_user_credentials(sap_login['_id'])
		a={}
		a={"error":"yes","message":'Unable to connect Sap user Credential'}
	return jsonify(a)
	
@zen_user.route('/getsdmaterialslist', methods = ['GET', 'POST'])
def getsdmaterialslist():
	q = request.args.get('term')
	db=dbconnect()
	u_mat = db.Sdmaterials.aggregate([
                {'$match' : { '$or':[{'MATNR' : {'$regex': str(q),'$options': 'i' }},{'MAKTX' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'MAKTX':1,'MATNR':1, "_id":0}}])
	return json.dumps({"results":u_mat['result']})

@zen_user.route('/getsdplantslist', methods = ['GET', 'POST'])
def getsdplantslist():
	q = request.args.get('term')
	db=dbconnect()
	u_plant = db.Sdplants.aggregate([
                {'$match' : { '$or':[{'NAME1' : {'$regex': str(q),'$options': 'i' }},{'WERKS' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'NAME1':1,'WERKS':1, "_id":0}}])
	return json.dumps({"results":u_plant['result']})

@zen_user.route('/announcement/', methods = ['GET', 'POST'])
@login_required
def announcement():
	p =check_user_role_permissions('54ab5b94507c00c54ba22297')
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if p:
		user_details = base()
		if request.method == 'POST':
			form=request.form
			uid = current_user.username
			array={"announcement":form['announcement'],"plant_id":get_plant_id(uid),"created_time": datetime.datetime.now()}
			save_collection('Announcements',array)
			flash('Announcement created successfully','alert-success')
			return redirect(url_for('zenuser.announcement'))
		return render_template('announcements.html', user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/help/', methods = ['GET', 'POST'])
@login_required
def help():
	user_details = base()
	return render_template('help.html', user_details=user_details)
		

@zen_user.route('/preports/', methods = ['GET', 'POST'])
@login_required
def preports():
	user_details = base()
	return render_template('pending_order_reports.html', user_details=user_details)

@zen_user.route('/getpendingreports', methods = ['GET', 'POST'])
def getpendingreports():
	form=request.form
	employees=get_employees_list()
	today=datetime.datetime.now()
	tday=today.strftime('%d.%m.%Y')
	yesterday=today-datetime.timedelta(days=1)
	yday=yesterday.strftime('%d.%m.%Y')
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if form['select_date'] == '1':
			result = conn.call('Z_BAPI_PENDING_ORD_REPORT',IM_YDAY=tday,IM_TDAY=tday,IT_PERNR=employees)
		elif form['select_date'] == '2':
			result = conn.call('Z_BAPI_PENDING_ORD_REPORT',IM_YDAY=yday,IM_TDAY=yday,IT_PERNR=employees)
		else:
			result = conn.call('Z_BAPI_PENDING_ORD_REPORT',IM_YDAY=yday,IM_TDAY=tday,IT_PERNR=employees)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['KBETR']=str(result['IT_FINAL'][i]['KBETR'])
				result['IT_FINAL'][i]['BKWMENG']=str(result['IT_FINAL'][i]['BKWMENG'])
				result['IT_FINAL'][i]['OKWMENG']=str(result['IT_FINAL'][i]['OKWMENG'])
				result['IT_FINAL'][i]['KBETR1']=str(result['IT_FINAL'][i]['KBETR1'])
				result['IT_FINAL'][i]['DKWMENG']=str(result['IT_FINAL'][i]['DKWMENG'])
				result['IT_FINAL'][i]['ERDAT']=result['IT_FINAL'][i]['ERDAT'].strftime('%d-%m-%Y')
			data['reports']=result['IT_FINAL']
	except:
		close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/creports/', methods = ['GET', 'POST'])
@login_required
def creports():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('collecting_reports.html', user_details=user_details)

@zen_user.route('/getcollectingreports', methods = ['GET', 'POST'])
def getcollectingreports():
	form=request.form
	employees=get_employees_list()
	today=datetime.datetime.now()
	tday=today.strftime('%d.%m.%Y')
	yesterday=today-datetime.timedelta(days=1)
	yday=yesterday.strftime('%d.%m.%Y')
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if form['select_date'] == '1':
			result = conn.call('Z_BAPI_COLLECTION_REPORT',IM_YDAY='25.02.2015',IM_TDAY='25.02.2015',IT_PERNR=employees)
		else:
			result = conn.call('Z_BAPI_COLLECTION_REPORT',IM_YDAY=yday,IM_TDAY=yday,IT_PERNR=employees)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['DMBTR']=str(result['IT_FINAL'][i]['DMBTR'])
				result['IT_FINAL'][i]['CPUDT']=result['IT_FINAL'][i]['CPUDT'].strftime('%d-%m-%Y')
				result['IT_FINAL'][i]['INSDATE']=result['IT_FINAL'][i]['INSDATE'].strftime('%d-%m-%Y')
				result['IT_FINAL'][i]['BUDAT']=result['IT_FINAL'][i]['BUDAT'].strftime('%d-%m-%Y')
			data['data']=result['IT_FINAL']
	except:
		close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/sreports/', methods = ['GET', 'POST'])
@login_required
def sreports():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('sales_reports.html', user_details=user_details)

@zen_user.route('/getsalesreports', methods = ['GET', 'POST'])
def getsalesreports():
	form=request.form
	employees=get_employees_list()
	today=datetime.datetime.now()
	tday=today.strftime('%d.%m.%Y')
	yesterday=today-datetime.timedelta(days=1)
	yday=yesterday.strftime('%d.%m.%Y')
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if form['select_date'] == '1':
			result = conn.call('Z_BAPI_DISPATCH_REPORT',IM_YDAY=tday,IM_TDAY=tday,IM_RTYPE=form['select_type'],IT_PERNR=employees)
		elif form['select_date'] == '2':
			result = conn.call('Z_BAPI_DISPATCH_REPORT',IM_YDAY=yday,IM_TDAY=yday,IM_RTYPE=form['select_type'],IT_PERNR=employees)
		else:
			result = conn.call('Z_BAPI_DISPATCH_REPORT',IM_YDAY=yday,IM_TDAY=tday,IM_RTYPE=form['select_type'],IT_PERNR=employees)
		conn.close()
		# close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['ZINV']=str(result['IT_FINAL'][i]['ZINV'])
				result['IT_FINAL'][i]['FKIMG']=str(result['IT_FINAL'][i]['FKIMG'])
				result['IT_FINAL'][i]['ERDAT']=result['IT_FINAL'][i]['ERDAT'].strftime('%d-%m-%Y')
				result['IT_FINAL'][i]['FKDAT']=result['IT_FINAL'][i]['FKDAT'].strftime('%d-%m-%Y')
			data['reports']=result['IT_FINAL']
	except:
		# close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/areports/', methods = ['GET', 'POST'])
@login_required
def areports():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('aging_reports.html', user_details=user_details)

@zen_user.route('/getagingreports', methods = ['GET', 'POST'])
def getagingreports():
	form=request.form
	employees=get_employees_list()
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_BAPI_AGEING_REPORT',IM_BUDAT=form['end_date'],IT_PERNR=employees)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['ZCLOSE']=str(result['IT_FINAL'][i]['ZCLOSE'])
				result['IT_FINAL'][i]['SLABAMT8']=str(result['IT_FINAL'][i]['SLABAMT8'])
				result['IT_FINAL'][i]['SLABAMT9']=str(result['IT_FINAL'][i]['SLABAMT9'])
				result['IT_FINAL'][i]['SLABAMT2']=str(result['IT_FINAL'][i]['SLABAMT2'])
				result['IT_FINAL'][i]['SLABAMT3']=str(result['IT_FINAL'][i]['SLABAMT3'])
				result['IT_FINAL'][i]['SLABAMT1']=str(result['IT_FINAL'][i]['SLABAMT1'])
				result['IT_FINAL'][i]['SLABAMT6']=str(result['IT_FINAL'][i]['SLABAMT6'])
				result['IT_FINAL'][i]['SLABAMT7']=str(result['IT_FINAL'][i]['SLABAMT7'])
				result['IT_FINAL'][i]['SLABAMT4']=str(result['IT_FINAL'][i]['SLABAMT4'])
				result['IT_FINAL'][i]['SLABAMT5']=str(result['IT_FINAL'][i]['SLABAMT5'])
				result['IT_FINAL'][i]['ADDLDEPOSIT']=str(result['IT_FINAL'][i]['ADDLDEPOSIT'])
				result['IT_FINAL'][i]['KLIMK']=str(result['IT_FINAL'][i]['KLIMK'])
				result['IT_FINAL'][i]['DEPOSIT']=str(result['IT_FINAL'][i]['DEPOSIT'])
			data['reports']=result['IT_FINAL']
	except:
		close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/selecthr/', methods = ['GET', 'POST'])
@login_required
def selecthr():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	if request.method == 'POST':
		form=request.form
		update_collection('Plants',{"plant_id":form['plant']},{"$set":{"plant_current_hr":form['hr']}})
		flash('HR updated successfully','alert-success')
		return redirect(url_for('zenuser.selecthr'))
	return render_template('select_hr.html', user_details=user_details)

@zen_user.route('/choosehr', methods = ['GET', 'POST'])
@login_required
def choosehr():
	deg = str(request.form.get('deg'))
	plant=find_one_in_collection('Plants',{"plant_id":deg})
	e_name=get_employee_name_given_pid(plant['plant_current_hr'])
	if deg == 'HY':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,'$or':[{"department_id":"50005157"},{"department_id":"50095158"}]},{"designation_id":1,"_id":0})
	elif deg == 'TC':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095181"},{"designation_id":1,"_id":0})
	elif deg == 'BP':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095192"},{"designation_id":1,"_id":0})
	elif deg == 'GC':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095183"},{"designation_id":1,"_id":0})
	elif deg == 'TA':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095182"},{"designation_id":1,"_id":0})
	elif deg == 'GP':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095184"},{"designation_id":1,"_id":0})
	return jsonify({"current_hr":e_name,"hr_pids":hr_pids})

#dump html files for next project

@zen_user.route('/materiallist/', methods=['GET', 'POST'])
@login_required
def materiallist():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('material_list.html', user_details=user_details)

@zen_user.route('/prstatus/', methods=['GET', 'POST'])
@login_required
def prstatus():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('pr_status.html', user_details=user_details)

@zen_user.route('/consumption/', methods=['GET', 'POST'])
@login_required
def consumption():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('consumption.html', user_details=user_details)

@zen_user.route('/consumption_RM/', methods=['GET', 'POST'])
@login_required
def consumption_RM():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('consumption_RM.html', user_details=user_details)

@zen_user.route('/consumption_emp/', methods=['GET', 'POST'])
@login_required
def consumption_emp():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('consumption_emp.html', user_details=user_details)

@zen_user.route('/comparision/', methods=['GET', 'POST'])
@login_required
def comparision():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('comparision.html', user_details=user_details)

@zen_user.route('/materialissue/', methods=['GET', 'POST'])
@login_required
def materialissue():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('material_issue.html', user_details=user_details)

@zen_user.route('/material_miv_report/', methods=['GET', 'POST'])
@login_required
def material_miv_report():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('material_miv_report.html', user_details=user_details)

@zen_user.route('/material_miv_rpt/', methods=['GET', 'POST'])
@login_required
def material_miv_rpt():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('material_miv_rpt.html', user_details=user_details)

@zen_user.route('/purchasecreation/', methods=['GET', 'POST'])
@login_required
def purchasecreation():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('purchasecreation.html', user_details=user_details)

@zen_user.route('/purchase_pm_order/', methods=['GET', 'POST'])
@login_required
def purchase_pm_order():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('purchase_pm_order.html', user_details=user_details)
						
@zen_user.route('/purchase_cost_order/', methods=['GET', 'POST'])
@login_required
def purchase_cost_order():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('purchase_cost_order.html', user_details=user_details)

@zen_user.route('/purchase_project_network/', methods=['GET', 'POST'])
@login_required
def purchase_project_network():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('purchase_project_network.html', user_details=user_details)
						
@zen_user.route('/purchase_asset/', methods=['GET', 'POST'])
@login_required
def purchase_asset():
	user_details = base()
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	return render_template('purchase_asset.html', user_details=user_details) 