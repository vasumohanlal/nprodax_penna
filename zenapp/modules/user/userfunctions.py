from zenapp.dbfunctions import *
from bson.objectid import ObjectId
from httplib import HTTPException
from flask.ext.login import current_user
import smtplib
import datetime
import urllib,urllib2,urlparse
import email.utils
from email.mime.text import MIMEText


class User():
	def __init__(self, userid=None, username=None ,date=None):
		self.userid = userid
		self.username = username
		self.date = date

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.userid)

	def __repr__(self):
		return '<User %r>' % self.username

def find_by_username(username):
	try:
		data = find_one_in_collection('Userinfo',{'username': username})
		if data is None:
			return None
		else:
			user = User(unicode(data['_id']), data['username'] , data['dob'])
			return user
	except HTTPException:
		return None

def find_by_id(userid):
	try:
		data = find_one_in_collection('Userinfo',{'_id': ObjectId(userid)})
		if data is None:
			return None
		else:
			user = User(unicode(data['_id']), data['username'], data['dob'])
			return user

	except HTTPException:
		return None

def profile_pic():
	uid = current_user.username
	userpic = find_one_in_collection('fs.files',{"uid" :uid,"tag":"profileimg"})
	if userpic:
		return '/user/static/img/gridfs/'+userpic['filename']
	else:
		return '/user/static/img/gridfs/161aa240-ee13-497a-9d21-723eb7689059.png'

def peraddress_pdf():
	uid = current_user.get_id()
	userpic = find_one_in_collection('fs.files',{"uid" :current_user.username,"tag":"peraddress_proof"})
	if userpic:
		return '/user/static/pdf/gridfs/'+userpic['filename']
	else:
		return '/user/static/pdf/gridfs/9e9b3083-b980-499d-8e73-b186a916309d.png'
def preaddress_pdf():
	uid = current_user.get_id()
	userpic = find_one_in_collection('fs.files',{"uid" :current_user.username,"tag":"preaddress_proof"})
	if userpic:
		return '/user/static/pdf/gridfs/'+userpic['filename']
	else:
		return '/user/static/pdf/gridfs/9e9b3083-b980-499d-8e73-b186a916309d.png'

def check_user_role_permissions(permissionid):
	uid = current_user.get_id()
	pid=find_and_filter('Userinfo',{'_id': ObjectId(uid)},{"designation_id":1,"_id":0})
	db = dbconnect()
	u_roles = db.Positions.aggregate([
                {'$match' : {"position_id": pid[0]['designation_id']}}, 
                {'$project' : {"roles":1,"_id":0}}])
	user_roles = u_roles['result'][0]['roles']
	count = len(user_roles)
	Role_Permissions =[]
	for k in range(0,count,1):
		P_in_Roles = db.Roles.aggregate([
                    {'$match' : {'_id' : ObjectId(user_roles[k])}},
                    {'$project' : {'permissions' : 1, '_id' : 0}}])
		Role_Permissions = Role_Permissions + P_in_Roles['result'][0]["permissions"]
	if permissionid in Role_Permissions:
		return True
	else:
		return False 
		
def base():
	uid = current_user.get_id()
	usr_details = find_one_in_collection('Userinfo',{'_id': ObjectId(uid)})
	usr_details['profpic']=profile_pic()
	arr={"e_id":current_user.username}
	notify=find_in_collection('Notifications',arr)
	usr_details['notification']=notify
	p =check_user_role_permissions('546f2097850d2d1b00023b26')
	if p:
		usr_details['la']='ok'
	p =check_user_role_permissions('546f228c850d2d1b00023b2c')
	if p:
		usr_details['ae']='ok'
	p =check_user_role_permissions('546f22de850d2d1b00023b32')
	if p:
		usr_details['pa']='ok'
	p =check_user_role_permissions('546f204b850d2d1b00023b22')
	if p:
		usr_details['al']='ok'
	p =check_user_role_permissions('547c7e0157fbb30ed690336e')
	if p:
		usr_details['hl']='ok'
	p =check_user_role_permissions('5487ed0757fbb30f702709c0')
	if p:
		usr_details['rep']='ok'
	p=check_user_role_permissions('546f22a8850d2d1b00023b2e')
	if p:
		usr_details['cr']='ok'
	p=check_user_role_permissions('546f2245850d2d1b00023b27')
	if p:
		usr_details['ps']='ok'
	p=check_user_role_permissions('546f2257850d2d1b00023b28')
	if p:
		usr_details['f16']='ok'
	p=check_user_role_permissions('546f2038850d2d1b00023b21')
	if p:
		usr_details['ale']='ok'
	p=check_user_role_permissions('546f22b9850d2d1b00023b2f')
	if p:
		usr_details['ar']='ok'
	p=check_user_role_permissions('54ab5b94507c00c54ba22297')
	if p:
		usr_details['ams']='ok'		
	p=check_user_role_permissions('54ae3abd507c00c54ba22299')
	if p:
		usr_details['lt']='ok'
	p=check_user_role_permissions('54b0fe8a507c00c54ba2229a')
	if p:
		usr_details['er']='ok'

	p=check_user_role_permissions('54b378d4507c00c54ba2229b')
	if p:
		usr_details['eh']='ok'
	p=check_user_role_permissions('54ab60d0507c00c54ba22298')
	if p:
		usr_details['sh']='ok'
	p=check_user_role_permissions('54b9001c507c00c54ba2229d')
	if p:
		usr_details['trade']='ok'
	p=check_user_role_permissions('54b9002e507c00c54ba2229e')
	if p:
		usr_details['nontrade']='ok'
	p=check_user_role_permissions('54b90061507c00c54ba2229f')
	if p:
		usr_details['ag']='ok'
	p=check_user_role_permissions('54b900a2507c00c54ba222a0')
	if p:
		usr_details['sales']='ok'
	p=check_user_role_permissions('54b900db507c00c54ba222a1')
	if p:
		usr_details['coll']='ok'
	p=check_user_role_permissions('54b900f7507c00c54ba222a2')
	if p:
		usr_details['po']='ok'
	p=check_user_role_permissions('54b90128507c00c54ba222a3')
	if p:
		usr_details['ob']='ok'
	p=check_user_role_permissions('550fdf44850d2d3861bd7ae4')
	if p:
		usr_details['pmpr']='ok'

	p=check_user_role_permissions('550fde51850d2d3861bd7adf')
	if p:
		usr_details['pmprapproval']='ok'
	
	p=check_user_role_permissions('557926faebd85951419de8fe')
	if p:
		usr_details['pl']='ok'

	p=check_user_role_permissions('557e7579d3491e8c029d66ac')
	if p:
		usr_details['us']='ok'

	p=check_user_role_permissions('559e0d22edce0feffec555a1')
	if p:
		usr_details['ts']='ok'

	p=check_user_role_permissions('55b0c0ccd1c282e4074d2082')
	if p:
		usr_details['bk']='ok'

	p=check_user_role_permissions('55a5e95f0e071e8e5240bb74')
	if p:
		usr_details['test']='ok'

	if current_user.username!="admin":
		if current_user.username !="00SHR":
			if usr_details["plant_id"]!="HY":
				usr_details['pmprcreation']='ok'
		
	tim=find_one_in_collection("Notificationtimestamp",{"username":current_user.username})	
	if tim:
		notify_num=find_in_collection_count("Notifications",{"created_time": {"$gt":tim['created_time']},"e_id":current_user.username })
		usr_details['notify_num']=notify_num
	else:
		usr_details['notify_num']=0

	return usr_details


def email(subject,receivers,message,uname):
	sender = 'myportal@pennacement.com'
	msg = MIMEText('Dear '+uname+',\n\n'+message+'\n\n\n\nThanks,\nTeam-HR.')
	msg['To'] = receivers
	msg['From'] ='myportal@pennacement.com'
	msg['Subject'] = subject
	smtpObj = smtplib.SMTP('202.65.143.58',25)
	smtpObj.set_debuglevel(True) # show communication with the server

	try:
		smtpObj.sendmail(sender, receivers,msg.as_string())         
   		print "Successfully sent email"
	except Exception,R:
            print R
 
def SendSms(message,mobilenumber,uname):
	url = "http://www.smscountry.com/smscwebservice_bulk.aspx"
	values = {'user' : 'pcilhr',
	'passwd' : 'pcil@123',
	'message' : "Dear "+uname+','+message,
	'mobilenumber':mobilenumber,
	'mtype':'N',
	'DR':'Y'
	}

	data = urllib.urlencode(values)
	data = data.encode('utf-8')
	request = urllib2.Request(url,data)
	response = urllib2.urlopen(request)


def notification(employee_id,message):
	save_collection('Notifications',{"e_id":employee_id,"msg":message,"created_time":datetime.datetime.now()})

