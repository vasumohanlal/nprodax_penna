from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.sessions.form import *
from zenapp.dbfunctions import *
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask.ext.login import login_user, current_user
from flask.ext.login import login_required, logout_user
from bson.objectid import ObjectId
import os
import csv
import zipfile
import StringIO
from calendar import monthrange
from datetime import datetime as dt
#from pyrfc import *
from zenapp.modules.leave.lfunctions import *
zen_sessions = Blueprint('zensessions', __name__, url_prefix='/sessions')


@zen_sessions.route('/usersessions/', methods=['GET', 'POST'])
@login_required
def usersessions():
	p =check_user_role_permissions('557e7579d3491e8c029d66ac')
	user_details = base()
	check = ""
	if check == "":
		form = user_sessions(request.values)
		fromd = form.user_date1.data
		tod = form.user_date2.data
	else:
		check = "true"
	if check == "true":
		form = user_sessions1(request.values)
		fromd = form.user1.data
		tod = form.user2.data
	
	if fromd != None and tod != None:
		fromdate = fromd.strip().split('/')
		todate = tod.strip().split('/')
		yy=int(fromdate[0])
		mm=int(fromdate[1])
		dd=int(fromdate[2])
		yy1=int(todate[0])
		mm1=int(todate[1])
		dd1=int(todate[2])
		detail = find_in_collection('logurl',{'$and':[{"logintime":{'$gt':dt(yy,mm,dd)}},{"logintime":{'$lt':dt(yy1,mm1,dd1)}}]})
		log_acount1 = len(detail)
		if log_acount1 > 1:
			log_acount1 = log_acount1-1
		else:
			log_acount1 = 0
	else:
		log1 = find_in_collection('logurl',{"lastlogin":""})
		log_acount1 = len(log1)
	
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	total_user_session = find_all_in_collection('logurl')
	date = datetime.datetime.now().date() 
	week = datetime.datetime.now().isocalendar()[1]
	year = datetime.datetime.now().year
	list_name =[]
	list_test=[]
	
	for i in range(0,len(total_user_session)):
		test = total_user_session[i]['username']
		l_name = find_in_collection('Userinfo',{"username":test})
		list_name.append(l_name[0]['l_name'])
	for i in range(0,len(list_name)):
		list_test.append(list_name[i].encode("utf-8"))
	
	if p:
		if fromd != None and tod != None:
			return render_template('user_sessions1.html',form=form,name=list_test, user_details=user_details,emp_det=detail,log_count=log_acount1,date=date,year=year, week=week)
		else:
			return render_template('user_sessions.html',form=form,name=list_test, user_details=user_details,emp_det=log1,log_count=log_acount1,date=date,year=year, week=week)
	else:
		return redirect(url_for('zenuser.index'))